CREATE VIEW vw_ela_diagnost_student AS
SELECT z.student_guid
FROM (
  SELECT a.student_guid
   FROM (
	SELECT student_guid, count(*) as rec_count
	FROM rpt_ela_decod
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_ela_read_comp
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_ela_read_flu
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_ela_vocab
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_ela_writing
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_reader_motiv
	WHERE rec_status = 'C'
	GROUP BY student_guid
     ) a
 EXCEPT
 SELECT student_guid
 FROM lkp_student_xref
) z