CREATE VIEW vw_math_diagnost_school AS
SELECT z.school_guid
FROM (
   SELECT a.school_guid
   FROM (
	SELECT school_guid, count(*) as rec_count
	FROM rpt_math_comp
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_math_flu
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_math_progress
	WHERE rec_status = 'C'
	GROUP BY school_guid
     ) a
	EXCEPT
	SELECT school_guid
	FROM lkp_school_xref
) z