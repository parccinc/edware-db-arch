CREATE VIEW vw_math_sum_tnt AS
SELECT ela.rec_id
, ela.record_type
, 1 as state_key
, y.year_key
, y.poy_key
, su.subject_key
, d.district_key
, sch.school_key
, t.teacher_key
, g.grade_key
, stu.student_key
, CASE WHEN  UPPER(ela.student_sex) = 'M' THEN 1 
WHEN UPPER(ela.student_sex) = 'F' THEN 0 
ELSE -1 END AS sex_key
, CASE WHEN ethn_hisp_latino = 'Y' THEN 1
 WHEN ethn_indian_alaska = 'Y' THEN 2
 WHEN ethn_asian = 'Y' THEN 3
 WHEN ethn_black = 'Y' THEN 4
 WHEN ethn_hawai = 'Y' THEN 5
 WHEN ethn_white = 'Y' THEN 6
 WHEN ethn_2_more = 'Y' THEN 7
 ELSE -1 
END AS ethnicity_key
, CASE WHEN ela.lep_status = 'Y' THEN 1
WHEN ela.lep_status = 'N' THEN 0 
ELSE -1 END AS lep_key
, ela.date_taken_poy
, ela.state_code
, ela.org_type
, ela.admin_fund_control
, ela.program_type
, ela.country_ansi_code
, ela.econo_research_service
, ela.student_sex
, ela.student_grade
, ela.parcc_growth_percent
, ela.state_growth_percent
, ela.district_growth_percent
, CASE WHEN ethn_hisp_latino = 'Y' THEN 'HISP'
 WHEN ethn_indian_alaska = 'Y' THEN 'IND'
 WHEN ethn_asian = 'Y' THEN 'ASIAN'
 WHEN ethn_black = 'Y' THEN 'BLK'
 WHEN ethn_hawai = 'Y' THEN 'HAW'
 WHEN ethn_white = 'Y' THEN 'WHT'
 WHEN ethn_2_more = 'Y' THEN 'MULT'
 ELSE 'NA' END AS ethnicity
, ela.ethn_hisp_latino
, CASE WHEN  UPPER(ela.ethn_hisp_latino) = 'Y' THEN 1 
WHEN UPPER(ela.student_sex) = 'N' THEN 0 
ELSE -1 END AS is_hisp_latino
, ela.ethn_indian_alaska
, CASE WHEN  UPPER(ela.ethn_indian_alaska) = 'Y' THEN 1 
WHEN UPPER(ela.ethn_indian_alaska) = 'N' THEN 0 
ELSE -1 END AS is_indian_alaska
, ela.ethn_asian
, CASE WHEN  UPPER(ela.ethn_asian) = 'Y' THEN 1 
WHEN UPPER(ela.ethn_asian) = 'N' THEN 0 
ELSE -1 END AS is_asian
, ela.ethn_black
, CASE WHEN  UPPER(ela.ethn_black) = 'Y' THEN 1 
WHEN UPPER(ela.ethn_black) = 'N' THEN 0 
ELSE -1 END AS is_black
, ela.ethn_hawai
, CASE WHEN  UPPER(ela.ethn_hawai) = 'Y' THEN 1 
WHEN UPPER(ela.ethn_hawai) = 'N' THEN 0 
ELSE -1 END AS is_hawai
, ela.ethn_white
, CASE WHEN  UPPER(ela.ethn_white) = 'Y' THEN 1 
WHEN UPPER(ela.ethn_white) = 'N' THEN 0 
ELSE -1 END AS is_white
, ela.ethn_2_more
, CASE WHEN  UPPER(ela.ethn_2_more) = 'Y' THEN 1 
WHEN UPPER(ela.ethn_2_more) = 'N' THEN 0 
ELSE -1 END AS is_2_more
, ela.lep_status
, ela.section_504
, ela.accomod_ell
, ela.accomod_ind_ed
, ela.econo_disadvantage
, ela.migrant_status
, CASE WHEN  UPPER(ela.migrant_status) = 'Y' THEN 1 
WHEN UPPER(ela.migrant_status) = 'N' THEN 0 
ELSE -1 END AS is_migrant
, ela.ell
, CASE WHEN  UPPER(ela.ell) = 'Y' THEN 1 
WHEN UPPER(ela.ell) = 'N' THEN 0 
ELSE -1 END AS is_ell
, ela.gift_talent
, CASE WHEN  UPPER(ela.gift_talent) = 'Y' THEN 1 
WHEN UPPER(ela.gift_talent) = 'N' THEN 0 
ELSE -1 END AS is_gift_talent
, ela.disabil_student
, CASE WHEN  UPPER(ela.disabil_student) = 'Y' THEN 1 
WHEN UPPER(ela.disabil_student) = 'N' THEN 0 
ELSE -1 END AS is_disabil_student
, ela.primary_disabil_type
, CASE WHEN primary_disabil_type = 'AUT' THEN 1
 WHEN primary_disabil_type = 'DB' THEN 2
 WHEN primary_disabil_type = 'DD' THEN 3
 WHEN primary_disabil_type = 'EMN' THEN 4
 WHEN primary_disabil_type = 'HI' THEN 5
 WHEN primary_disabil_type = 'ID' THEN 6
 WHEN primary_disabil_type = 'MD' THEN 7
 WHEN primary_disabil_type = 'OI' THEN 8
 WHEN primary_disabil_type = 'OHI' THEN 9
 WHEN primary_disabil_type = 'SLD' THEN 10
 WHEN primary_disabil_type = 'SLI' THEN 11
 WHEN primary_disabil_type = 'TBI' THEN 12
 WHEN primary_disabil_type = 'VI' THEN 13 
 ELSE -1
END AS primary_disabil_type_key
, CASE WHEN primary_disabil_type = 'AUT' THEN 1 ELSE -1 END AS is_out
, CASE WHEN primary_disabil_type = 'DB' THEN 1 ELSE -1 END AS is_db
, CASE WHEN primary_disabil_type = 'DD' THEN 1 ELSE -1 END AS is_dd
, CASE WHEN primary_disabil_type = 'EMN' THEN 1 ELSE -1 END AS is_emn
, CASE WHEN primary_disabil_type = 'HI' THEN 1 ELSE -1 END AS is_hi
, CASE WHEN primary_disabil_type = 'MD' THEN 1 ELSE -1 END AS is_md
, CASE WHEN primary_disabil_type = 'ID' THEN 1 ELSE -1 END AS is_id
, CASE WHEN primary_disabil_type = 'OI' THEN 1 ELSE -1 END AS is_oi
, CASE WHEN primary_disabil_type = 'OHI' THEN 1 ELSE -1 END AS is_ohi
, CASE WHEN primary_disabil_type = 'SLD' THEN 1 ELSE -1 END AS is_sld
, CASE WHEN primary_disabil_type = 'SLI' THEN 1 ELSE -1 END AS is_sli
, CASE WHEN primary_disabil_type = 'TBI' THEN 1 ELSE -1 END AS is_tbi
, CASE WHEN primary_disabil_type = 'VI' THEN 1 ELSE -1 END AS is_vi
, ela.pba_asmt_guid
, ela.eoy_asmt_guid
, ela.summ_asmt_guid
, ela.asmt_subject
, ela.where_taken_id
, ela.where_taken_name
, ela.asmt_attempt_guid
, ela.form_guid
, ela.date_asmt_session_end
, ela.date_asmt_session_start
, ela.date_taken_year
, ela.admin_code
, ela.test_code
, ela.asmt_score
, ela.csem
, ela.asmt_scaled_score
, ela.asmt_perf_lvl
, ela.claim1_score
, ela.claim2_score
, ela.subclaim1_score
, ela.subclaim2_score
, ela.subclaim3_score
, ela.subclaim4_score
, ela.subclaim5_score
, ela.asmt_format
, ela.retest_flag
, ela.total_items
, ela.total_items_attempt
, ela.total_items_unit1
, ela.unit1_items_attempt
, ela.total_items_unit2
, ela.unit2_items_attempt
, ela.total_items_unit3
, ela.unit3_items_attempt
, ela.total_items_unit4
, ela.unit4_items_attempt
, ela.total_items_unit5
, ela.unit5_items_attempt
, ela.invalidation_code
, ela.invalidation_reason
FROM lkp_student_xref stu  JOIN (SELECT * FROM rpt_math_sum WHERE rec_status = 'C' AND summative_flag = 3) ela 
ON (stu.student_parcc_id = ela.student_parcc_id)
JOIN lkp_district_xref d ON (d.district_guid = ela.district_guid)
JOIN lkp_school_xref sch ON (sch.school_guid = ela.school_guid)  
JOIN ref_poy_year_xref y ON (y.poy = ela.date_taken_poy AND y.year = ela.date_taken_year )
JOIN ref_subject_xref su ON (su.asmt_subject = ela.asmt_subject)
JOIN ref_grade_xref g ON (g.student_grade = ela.student_grade)
JOIN lkp_staff_xref t ON (t.staff_id = ela.staff_id)
;
