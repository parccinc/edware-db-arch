CREATE VIEW vw_ela_diagnost_school AS
SELECT z.school_guid
FROM (
  SELECT a.school_guid
   FROM (
	SELECT school_guid, count(*) as rec_count
	FROM rpt_ela_decod
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_ela_read_comp
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_ela_read_flu
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_ela_vocab
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_ela_writing
	WHERE rec_status = 'C'
	GROUP BY school_guid
	UNION ALL
	SELECT school_guid, count(*) as rec_count
	FROM rpt_reader_motiv
	WHERE rec_status = 'C'
	GROUP BY school_guid
     ) a
 EXCEPT
 SELECT school_guid
 FROM lkp_school_xref
) z