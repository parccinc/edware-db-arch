-- 
-- TABLE: rpt_form_group 
--

CREATE TABLE rpt_form_group(
    rec_id               int8            NOT NULL,
    asmt_guid            varchar(50)     NOT NULL,
    form_guid            varchar(50)     NOT NULL,
    group_guid           varchar(50)     NOT NULL,
    group_code           varchar(50),
    group_name           varchar(60),
    group_desc           varchar(256),
    group_type           varchar(30)     NOT NULL,
    parent_group_guid    varchar(50),
    group_ref_id         varchar(60),
    batch_guid           varchar(50)     NOT NULL,
    rec_status           varchar(1)      DEFAULT 'C' NOT NULL,
    create_date          date            DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date    date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_form_group.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_form_group.form_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_form_group.group_guid IS 'group_guid = claim orsub-claim guid'
;
COMMENT ON COLUMN rpt_form_group.group_name IS 'claim or subclaim name'
;
COMMENT ON COLUMN rpt_form_group.group_type IS 'claim or sub-claim'
;
COMMENT ON COLUMN rpt_form_group.parent_group_guid IS 'for claim it is the same claim (group) guid, claim is a parent for itself, for subclaim it would be a parent claim'
;
COMMENT ON COLUMN rpt_form_group.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_form_group IS 'Added staus_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_form_group_ix 
--

CREATE INDEX rpt_form_group_ix ON rpt_form_group(asmt_guid, form_guid, rec_status)
;
-- 
-- TABLE: rpt_form_group 
--

ALTER TABLE rpt_form_group ADD 
    CONSTRAINT rpt_form_group_pk PRIMARY KEY (rec_id)
;

