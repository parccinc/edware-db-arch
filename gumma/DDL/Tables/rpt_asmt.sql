-- 
-- TABLE: rpt_asmt 
--

CREATE TABLE rpt_asmt(
    rec_id               int8           NOT NULL,
    asmt_guid            varchar(50)    NOT NULL,
    admin_guid           varchar(50)    NOT NULL,
    asmt_title           varchar(60),
    asmt_subject         varchar(15),
    batch_guid           varchar(50)    NOT NULL,
    rec_status           varchar(1)     DEFAULT 'C' NOT NULL,
    create_date          date           DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date    date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_asmt.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_asmt.admin_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_asmt.asmt_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON COLUMN rpt_asmt.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_asmt IS 'removed claim, subclaimn fields. changed to 1-to-many admin - asmt. 10/16/2014. Added staus_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_asmt_ix 
--

CREATE INDEX rpt_asmt_ix ON rpt_asmt(asmt_guid, rec_status)
;
-- 
-- TABLE: rpt_asmt 
--

ALTER TABLE rpt_asmt ADD 
    CONSTRAINT rpt_asmt_pk PRIMARY KEY (rec_id)
;

