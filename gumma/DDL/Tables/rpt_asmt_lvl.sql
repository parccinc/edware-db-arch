-- 
-- TABLE: rpt_asmt_lvl 
--

CREATE TABLE rpt_asmt_lvl(
    rec_id               int8           NOT NULL,
    asmt_guid            varchar(50)    NOT NULL,
    asmt_lvl_design      char(2)        NOT NULL,
    batch_guid           varchar(50)    NOT NULL,
    rec_status           varchar(1)     DEFAULT 'C' NOT NULL,
    create_date          date           DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date    date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_asmt_lvl.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_asmt_lvl.asmt_lvl_design IS 'Amplify name:AssessmentLevelForWhichDesigned. The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed.KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362
'
;
COMMENT ON COLUMN rpt_asmt_lvl.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_asmt_lvl IS 'one asessment guid can be associated with different grade levels. Added staus_change_date 10/24/14.'
;

-- 
-- TABLE: rpt_asmt_lvl 
--

ALTER TABLE rpt_asmt_lvl ADD 
    CONSTRAINT rpt_asmt_lvl_pk PRIMARY KEY (rec_id)
;

