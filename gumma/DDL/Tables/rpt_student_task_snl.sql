-- 
-- TABLE: rpt_student_task_snl 
--

CREATE TABLE rpt_student_task_snl(
    rec_id                 int8             NOT NULL,
    student_parcc_id       varchar(40),
    asmt_attempt_guid      varchar(36)      NOT NULL,
    task_guid              varchar(50),
    task_name              varchar(60),
    total_task_score       numeric(4, 0),
    observation            varchar(250),
    dim_score1             numeric(4, 0),
    dim_score2             numeric(4, 0),
    dim_score3             numeric(4, 0),
    dim_score4             numeric(4, 0),
    dim_score5             numeric(4, 0),
    dr_evidence_observ1    char(1),
    dr_evidence_observ2    char(1),
    dr_evidence_observ3    char(1),
    dr_evidence_observ4    char(1),
    dr_evidence_observ5    char(1),
    batch_guid             varchar(50)      NOT NULL,
    rec_status             char(1)          DEFAULT 'C' NOT NULL,
    create_date            date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_student_task_snl.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON COLUMN rpt_student_task_snl.asmt_attempt_guid IS 'Parcc name: AssessmentRegistrationTestAttemptIdentifier. Assessment Attempt Identifier (this actaully aligns with Amplify AssessmentGuid field). Unique identifier per assessment administration+poy+student+type'
;
COMMENT ON COLUMN rpt_student_task_snl.observation IS 'ObservationsDR For discussion rubric, Observations_PT For performance task, Observations_LL For listening log, Comment # or Text'
;
COMMENT ON COLUMN rpt_student_task_snl.dim_score2 IS 'For Discussion: DimScore1,DimScore2,DimScore3,DimScore4;
For performance: Dim_Score_1_PT,Dim_Score_2_PT,Dim_Score_3_PT,Dim_Score_4_PT,Dim_Score_5_PT;
For Listening: Listening Log Response 1,Listening Log Response 2,Listening Log Response 3,Listening Log Response 4,Listening Log Response 5'
;
COMMENT ON COLUMN rpt_student_task_snl.dim_score3 IS 'For Discussion: DimScore1,DimScore2,DimScore3,DimScore4;
For performance: Dim_Score_1_PT,Dim_Score_2_PT,Dim_Score_3_PT,Dim_Score_4_PT,Dim_Score_5_PT;
For Listening: Listening Log Response 1,Listening Log Response 2,Listening Log Response 3,Listening Log Response 4,Listening Log Response 5'
;
COMMENT ON COLUMN rpt_student_task_snl.dim_score4 IS 'For Discussion: DimScore1,DimScore2,DimScore3,DimScore4;
For performance: Dim_Score_1_PT,Dim_Score_2_PT,Dim_Score_3_PT,Dim_Score_4_PT,Dim_Score_5_PT;
For Listening: Listening Log Response 1,Listening Log Response 2,Listening Log Response 3,Listening Log Response 4,Listening Log Response 5'
;
COMMENT ON COLUMN rpt_student_task_snl.dim_score5 IS 'For Discussion: DimScore1,DimScore2,DimScore3,DimScore4;
For performance: Dim_Score_1_PT,Dim_Score_2_PT,Dim_Score_3_PT,Dim_Score_4_PT,Dim_Score_5_PT;
For Listening: Listening Log Response 1,Listening Log Response 2,Listening Log Response 3,Listening Log Response 4,Listening Log Response 5'
;
COMMENT ON COLUMN rpt_student_task_snl.dr_evidence_observ1 IS 'M, D, or E (where M=Mastered; D=Developing; E=Emerging'
;
COMMENT ON COLUMN rpt_student_task_snl.dr_evidence_observ2 IS 'M, D, or E (where M=Mastered; D=Developing; E=Emerging'
;
COMMENT ON COLUMN rpt_student_task_snl.dr_evidence_observ3 IS 'M, D, or E (where M=Mastered; D=Developing; E=Emerging'
;
COMMENT ON COLUMN rpt_student_task_snl.dr_evidence_observ4 IS 'M, D, or E (where M=Mastered; D=Developing; E=Emerging'
;
COMMENT ON COLUMN rpt_student_task_snl.dr_evidence_observ5 IS 'M, D, or E (where M=Mastered; D=Developing; E=Emerging'
;
COMMENT ON COLUMN rpt_student_task_snl.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_student_task_snl IS 'Student score on particular item. Source XML'
;

-- 
-- TABLE: rpt_student_task_snl 
--

ALTER TABLE rpt_student_task_snl ADD 
    CONSTRAINT rpt_student_task_snl_pk PRIMARY KEY (rec_id)
;

