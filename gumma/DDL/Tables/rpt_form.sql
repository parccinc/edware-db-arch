-- 
-- TABLE: rpt_form 
--

CREATE TABLE rpt_form(
    rec_id                           int8            NOT NULL,
    form_guid                        varchar(50)     NOT NULL,
    asmt_guid                        varchar(50)     NOT NULL,
    form_code                        varchar(100),
    form_name                        varchar(70),
    form_format                      varchar(60),
    form_ref_id                      varchar(60),
    form_version                     varchar(30),
    asmt_score_max                   int2,
    asmt_perf_lvl1_guid              varchar(50),
    asmt_perf_lvl1_cutpoint_label    varchar(256),
    asmt_perf_lvl1_cutpoint_low      int2,
    asmt_perf_lvl1_cutpoint_upp      int2,
    asmt_perf_lvl2_guid              varchar(50),
    asmt_perf_lvl2_cutpoint_label    varchar(256),
    asmt_perf_lvl2_cutpoint_low      int2,
    asmt_perf_lvl2_cutpoint_upp      int2,
    asmt_perf_lvl3_guid              varchar(50),
    asmt_perf_lvl3_cutpoint_label    varchar(256),
    asmt_perf_lvl3_cutpoint_low      int2,
    asmt_perf_lvl3_cutpoint_upp      int2,
    asmt_perf_lvl4_guid              varchar(50),
    asmt_perf_lvl4_cutpoint_label    varchar(256),
    asmt_perf_lvl4_cutpoint_low      int2,
    asmt_perf_lvl4_cutpoint_upp      int2,
    asmt_perf_lvl5_guid              varchar(50),
    asmt_perf_lvl5_cutpoint_label    varchar(256),
    asmt_perf_lvl5_cutpoint_low      int2,
    asmt_perf_lvl5_cutpoint_upp      int2,
    batch_guid                       varchar(50)     NOT NULL,
    rec_status                       varchar(1)      DEFAULT 'C' NOT NULL,
    create_date                      date            DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date                date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_form.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_form.form_name IS 'AssessmentFormName'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl1_cutpoint_label IS 'Readin, Writing, MATH'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl1_cutpoint_low IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl1_cutpoint_upp IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl2_cutpoint_label IS 'Readin, Writing, MATH'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl2_cutpoint_low IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl2_cutpoint_upp IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl3_cutpoint_label IS 'Readin, Writing, MATH'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl3_cutpoint_low IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl3_cutpoint_upp IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl4_cutpoint_label IS 'Readin, Writing, MATH'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl4_cutpoint_low IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl4_cutpoint_upp IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl5_cutpoint_label IS 'Readin, Writing, MATH'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl5_cutpoint_low IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.asmt_perf_lvl5_cutpoint_upp IS 'The cut point of level 1 performance level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21774'
;
COMMENT ON COLUMN rpt_form.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_form IS 'Added staus_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_form_ix 
--

CREATE INDEX rpt_form_ix ON rpt_form(form_guid, rec_status)
;
-- 
-- TABLE: rpt_form 
--

ALTER TABLE rpt_form ADD 
    CONSTRAINT rpt_form_pk PRIMARY KEY (rec_id)
;

