-- 
-- TABLE: rpt_item_ela_vocab 
--

CREATE TABLE rpt_item_ela_vocab(
    rec_id                int8             NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    student_item_score    numeric(4, 0),
    select_key            varchar(250),
    item_seq              int2,
    batch_guid            varchar(50)      NOT NULL,
    create_date           date             DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_item_ela_vocab.asmt_attempt_guid IS 'if not in xml, then would be extracted from asmt results csv'
;
COMMENT ON COLUMN rpt_item_ela_vocab.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON TABLE rpt_item_ela_vocab IS 'Item Math Progression;Item Math Comprehension;Item ELA Decoding;Item ELA Reading Comp,Item ELA Vocab Source'
;

-- 
-- TABLE: rpt_item_ela_vocab 
--

ALTER TABLE rpt_item_ela_vocab ADD 
    CONSTRAINT rpt_item_ela_vocab_pk PRIMARY KEY (rec_id)
;

