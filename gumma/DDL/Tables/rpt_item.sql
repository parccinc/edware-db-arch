-- 
-- TABLE: rpt_item 
--

CREATE TABLE rpt_item(
    rec_id               int8            NOT NULL,
    item_guid            varchar(50)     NOT NULL,
    item_name            varchar(150)    NOT NULL,
    item_descript        varchar(250),
    item_ref_id          varchar(60),
    item_max_points      varchar(30),
    item_version         varchar(30),
    test_item_type       varchar(60),
    is_item_omitted      varchar(30),
    item_status          varchar(30),
    parent_item_guid     varchar(50),
    batch_guid           varchar(50)     NOT NULL,
    rec_status           varchar(1)      DEFAULT 'C' NOT NULL,
    create_date          date            DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date    date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_item.is_item_omitted IS 'non-reportable'
;
COMMENT ON COLUMN rpt_item.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_item IS 'Added staus_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_item_ix 
--

CREATE INDEX rpt_item_ix ON rpt_item(item_guid, rec_status)
;
-- 
-- TABLE: rpt_item 
--

ALTER TABLE rpt_item ADD 
    CONSTRAINT rpt_item_pk PRIMARY KEY (rec_id)
;

