-- 
-- TABLE: rpt_dgnst_asmt_m 
--

CREATE TABLE rpt_dgnst_asmt_m(
    rec_id                        int8           NOT NULL,
    asmt_guid                     varchar(50)    NOT NULL,
    admin_guid                    varchar(50)    NOT NULL,
    asmt_title                    varchar(60),
    asmt_subject                  varchar(15),
    genre                         varchar(40),
    rmm                           float4,
    lexile                        varchar(40),
    category_classification       varchar(10),
    probability_classification    float4,
    report_category               varchar(40),
    concepts_skills               varchar(40),
    cluster_classification        int4,
    domain                        varchar(40),
    cluster                       varchar(40),
    batch_guid                    varchar(50)    NOT NULL,
    rec_status                    varchar(1)     DEFAULT 'C' NOT NULL,
    create_date                   date           DEFAULT CURRENT_DATE NOT NULL,
    status_change_date            date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_dgnst_asmt_m.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_dgnst_asmt_m.asmt_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON COLUMN rpt_dgnst_asmt_m.genre IS 'e.g. Informational'
;
COMMENT ON COLUMN rpt_dgnst_asmt_m.category_classification IS '1 or Ready'
;
COMMENT ON COLUMN rpt_dgnst_asmt_m.report_category IS 'Complex Vowels'
;
COMMENT ON COLUMN rpt_dgnst_asmt_m.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_dgnst_asmt_m IS 'Removed claims, subclaims.changed to 1-to-many admin - asmt. 10/16/2014'
;

-- 
-- TABLE: rpt_dgnst_asmt_m 
--

ALTER TABLE rpt_dgnst_asmt_m ADD 
    CONSTRAINT rpt_dgnst_asmt_m_pk PRIMARY KEY (rec_id)
;

