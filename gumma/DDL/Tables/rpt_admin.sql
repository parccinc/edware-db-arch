-- 
-- TABLE: rpt_admin 
--

CREATE TABLE rpt_admin(
    rec_id               int8            NOT NULL,
    admin_guid           varchar(50)     NOT NULL,
    admin_name           varchar(60),
    admin_code           varchar(50),
    admin_type           varchar(32),
    admin_periodd        varchar(32),
    date_taken_year      char(9),
    callback_url         varchar(256),
    src_file_type        varchar(80),
    batch_guid           varchar(50)     NOT NULL,
    rec_status           varchar(1)      DEFAULT 'C' NOT NULL,
    create_date          date            DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date    date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_admin.admin_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_admin.admin_type IS 'Amplify name:AssessmentIdentifier 
(formerly AssessmentType)
(CEDS - AssessmentIdentifier)), Parcc namea:AdminCode. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21359'
;
COMMENT ON COLUMN rpt_admin.admin_periodd IS 'The time of the year for a particular assessment.'
;
COMMENT ON COLUMN rpt_admin.date_taken_year IS 'Amplify name:AssessmentYear, Parcc name: "SchoolYear
format YYYY-YYYY''. .https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19847. FORMAT YYYY-YYYY, CEDS YYYY'
;
COMMENT ON COLUMN rpt_admin.src_file_type IS 'For phase 1: DDS_DNEXT, WGEN_DNEXT were he values for file type, became a parent_file_type in Universal UDL (phase2). For 3P Assess =  Assessment Name.'
;
COMMENT ON COLUMN rpt_admin.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_admin IS 'changed to 1-to-many admin - asmt. 10/16/2014. added staus_change_date 10/23/2014.Added staus_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_admin_ix 
--

CREATE INDEX rpt_admin_ix ON rpt_admin(admin_guid, rec_status)
;
-- 
-- TABLE: rpt_admin 
--

ALTER TABLE rpt_admin ADD 
    CONSTRAINT rpt_admin_pk PRIMARY KEY (rec_id)
;

