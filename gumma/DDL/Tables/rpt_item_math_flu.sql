-- 
-- TABLE: rpt_item_math_flu 
--

CREATE TABLE rpt_item_math_flu(
    asmt_attempt_guid     varchar(36),
    rec_id                int8             NOT NULL,
    item_guid             varchar(50),
    student_parcc_id      varchar(50),
    rsp_entered           int2             NOT NULL,
    student_item_score    numeric(4, 0),
    item_seq              numeric(6, 0),
    item_time             numeric(4, 0),
    create_date           date             DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_item_math_flu.asmt_attempt_guid IS 'if not in xml, then would be extracted from asmt results csv'
;
COMMENT ON COLUMN rpt_item_math_flu.item_time IS 'Time student spent to answer a single item in seconds. for Item Math Fluency only.'
;
COMMENT ON TABLE rpt_item_math_flu IS 'Item Math Fluency'
;

-- 
-- TABLE: rpt_item_math_flu 
--

ALTER TABLE rpt_item_math_flu ADD 
    CONSTRAINT rpt_item_math_flu_pk PRIMARY KEY (rec_id)
;

