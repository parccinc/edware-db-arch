-- 
-- TABLE: rpt_item_math_mya 
--

CREATE TABLE rpt_item_math_mya(
    rec_id                int8             NOT NULL,
    asmt_attempt_guid     varchar(36)      NOT NULL,
    student_parcc_id      varchar(40),
    student_item_score    numeric(4, 0),
    batch_guid            varchar(50)      NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C' NOT NULL,
    create_date           date             DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_item_math_mya.asmt_attempt_guid IS 'if not in xml, then would be extracted from asmt results csv'
;
COMMENT ON COLUMN rpt_item_math_mya.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON COLUMN rpt_item_math_mya.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_item_math_mya IS 'Student score on particular item. Added staus_change_date 10/24/14.'
;

-- 
-- TABLE: rpt_item_math_mya 
--

ALTER TABLE rpt_item_math_mya ADD 
    CONSTRAINT rpt_item_math_mya_pk PRIMARY KEY (rec_id)
;

