-- 
-- TABLE: rpt_ela_writing 
--

CREATE TABLE rpt_ela_writing(
    rec_id                      int8             NOT NULL,
    state_code                  char(2),
    test_district_guid          varchar(40),
    test_district_name          varchar(60),
    test_school_guid            varchar(40),
    test_school_name            varchar(60),
    district_guid               varchar(40),
    district_name               varchar(60),
    school_guid                 varchar(40),
    school_name                 varchar(60),
    org_structure               char(2),
    school_short_name           varchar(30),
    org_type                    varchar(40),
    admin_fund_control          char(7),
    program_type                char(5),
    country_ansi_code           char(3),
    econo_research_service      numeric(1, 0),
    parcc_org_code              varchar(50),
    student_parcc_id            varchar(40),
    student_state_id            varchar(40),
    student_local_id            varchar(40),
    student_sex                 char(1),
    student_grade               char(2),
    ethn_hisp_latino            char(1),
    ethn_indian_alaska          char(1),
    ethn_asian                  char(1),
    ethn_black                  char(1),
    ethn_hawai                  char(1),
    ethn_white                  char(1),
    ethn_2_more                 char(1),
    lep_status                  char(1),
    section_504                 char(1),
    accomod_ell                 char(1),
    accomod_ind_ed              char(1),
    econo_disadvantage          char(1),
    migrant_status              char(1),
    ell                         char(1),
    gift_talent                 char(1),
    disabil_student             char(1),
    primary_disabil_type        varchar(3),
    asmt_guid                   varchar(36),
    asmt_subject                varchar(15),
    where_taken_id              varchar(40),
    where_taken_name            varchar(60),
    asmt_attempt_guid           varchar(36),
    form_guid                   varchar(30),
    form_name                   varchar(70),
    date_asmt_session_end       varchar(30),
    date_asmt_session_start     varchar(30),
    date_taken                  date,
    date_taken_year             char(9),
    date_taken_poy              varchar(40),
    accomod_sign_video          char(1),
    accomod_sign_human_ela      char(1),
    accomod_sign_human_math     char(1),
    accomod_braille             char(1),
    accomod_close_capt          char(1),
    accomod_text_to_speech      char(1),
    accomod_alt_response        char(1),
    accomod_braille_rsp         char(1),
    accomod_monitor_rsp         char(1),
    accomod_calculator          char(1),
    accomod_mult_table          int2,
    accomod_print_on_demand     int2,
    accomod_read_aloud          varchar(20),
    accomod_oral_response       varchar(20),
    accomod_select_rsp          varchar(20),
    accomod_math_rsp            varchar(20),
    accomod_speech_2_text       int2,
    accomod_streamline_mode     int2,
    accomod_dict_native_lang    char(1),
    accomod_word_predict        char(1),
    accomod_answer_mash         char(1),
    accomod_color_contrast      char(1),
    accomod_large_print         char(1),
    accomod_tactile_graphics    char(1),
    accomod_read_synthetic      char(1),
    accomod_read_question       char(1),
    pnp_spoken                  char(1),
    pnp_asl                     boolean,
    pnp_close_capt              char(1),
    pnp_braille                 char(1),
    pnp_read_aloud              boolean,
    pnp_alt_lang                varchar(3),
    pnp_math_trans              varchar(3),
    pnp_add_break               char(1),
    pnp_alt_location            char(1),
    pnp_small_group             char(1),
    pnp_special_equip           char(1),
    pnp_spec_area               char(1),
    pnp_time_day                char(1),
    pnp_native_lang             char(1),
    pnp_loud_native_lang        char(1),
    pnp_math_rsp                char(1),
    pnp_extend_time             char(1),
    pnp_alt_paper_test          char(1),
    pnp_speech_2_text           boolean,
    pnp_transcription           boolean,
    asmt_attempt_flag           varchar(30),
    asmt_format                 varchar(30),
    retest_flag                 char(1),
    do_not_report               varchar(30),
    do_not_report_reason        varchar(60),
    asmt_score                  int2,
    asmt_scale_score            int2,
    batch_guid                  varchar(50)      NOT NULL,
    rec_status                  varchar(1)       DEFAULT 'C' NOT NULL,
    create_date                 date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_ela_writing.state_code IS 'Parcc name:State Abbreviation. The abbreviation for the state in which the SEA address is located. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19974'
;
COMMENT ON COLUMN rpt_ela_writing.test_district_guid IS 'PARCC name: Responsible District Identifier, amplify ResponsibleDistrictIdentifier. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20905'
;
COMMENT ON COLUMN rpt_ela_writing.test_district_name IS 'PARCC NAME: ResponsbileDistrictName; CEDS name = OrganizationName. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19902'
;
COMMENT ON COLUMN rpt_ela_writing.test_school_guid IS 'PARCC name School Institution Identifier, amplify name ResponsibleSchoolIdentifier'
;
COMMENT ON COLUMN rpt_ela_writing.test_school_name IS 'PARCC name: Organization Name ; amplify: OrganizationName'
;
COMMENT ON COLUMN rpt_ela_writing.district_guid IS 'PARCC name: Responsible District Identifier, amplify ResponsibleDistrictIdentifier. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20905'
;
COMMENT ON COLUMN rpt_ela_writing.district_name IS 'PARCC NAME: ResponsbileDistrictName; CEDS name = OrganizationName. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19902'
;
COMMENT ON COLUMN rpt_ela_writing.school_guid IS 'PARCC name School Institution Identifier, amplify name ResponsibleSchoolIdentifier'
;
COMMENT ON COLUMN rpt_ela_writing.school_name IS 'PARCC name: Organization Name ; amplify: OrganizationName'
;
COMMENT ON COLUMN rpt_ela_writing.org_structure IS 'Pearson-specific field'
;
COMMENT ON COLUMN rpt_ela_writing.school_short_name IS 'amplify name:ShortNameOfInstitution, Parcc name:Short Name of Institution. The name of the institution, which may be the abbreviated form of the full legally accepted name. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=6459'
;
COMMENT ON COLUMN rpt_ela_writing.org_type IS 'Pasrcc name: Organization Type .The type of educational organization or entity.https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=4165'
;
COMMENT ON COLUMN rpt_ela_writing.admin_fund_control IS 'AdministrativeFundingControl	Administrative Funding Control	 The type of education institution as classified by its funding source.https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3012'
;
COMMENT ON COLUMN rpt_ela_writing.program_type IS 'COMMENT ON COLUMN ldr_ela_sum.program_type IS ''Amplify name:ProgramType, Parcc name:"Program Type.The system outlining instructional or non-instructional activities and procedures designed to accomplish a predetermined educational objective or set of objectives or to provide support services to person and/or the community. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3225'
;
COMMENT ON COLUMN rpt_ela_writing.country_ansi_code IS 'Pasrcc name: County ANSI Code. Only applies to school-level. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=4176†'
;
COMMENT ON COLUMN rpt_ela_writing.econo_research_service IS 'Parcc field:Economic Research Service Rural-Urban Continuum Code. Rural-Urban Continuum Codes form a classification scheme that distinguishes metropolitan (metro) counties by the population size of their metro area, and nonmetropolitan (nonmetro) counties by degree of urbanization and adjacency to a metro area or areas. The metro and nonmetro categories have been subdivided into three metro and six nonmetro groupings, resulting in a nine-part county codification. The codes allow researchers working with county data to break such data into finer residential groups beyond a simple metro-nonmetro dichotomy, particularly for the analysis of trends in nonmetro areas that may be related to degree of rurality and metro proximity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3862'
;
COMMENT ON COLUMN rpt_ela_writing.parcc_org_code IS 'PARCC Organization Code'
;
COMMENT ON COLUMN rpt_ela_writing.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775
'
;
COMMENT ON COLUMN rpt_ela_writing.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.'
;
COMMENT ON COLUMN rpt_ela_writing.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN rpt_ela_writing.student_sex IS 'Parccc name: Sex. The concept describing the biological traits that distinguish the males and females of a species. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20811'
;
COMMENT ON COLUMN rpt_ela_writing.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_indian_alaska IS 'Parcc name:American Indian/Alaska Native. A person having origins in any of the original peoples of North and South America (including Central America), and who maintains cultural identification through tribal affiliation or community attachment.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20907'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_asian IS 'Parcc name:Asian.A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian Subcontinent. This area includes, for example, Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand, and Vietnam.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20908'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_black IS 'Parcc name: Black or African American	A person having origins in any of the black racial groups of Africa. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20909'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_hawai IS 'Parcc name: Native Hawaiian or Other Pacific Islander. A person having origins in any of the original peoples of Hawaii, Guam, Samoa, or other Pacific Islands. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20910'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_white IS 'Parcc name:White. A person having origins in any of the original peoples of Europe, Middle East, or North Africa.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20911'
;
COMMENT ON COLUMN rpt_ela_writing.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425'
;
COMMENT ON COLUMN rpt_ela_writing.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783'
;
COMMENT ON COLUMN rpt_ela_writing.section_504 IS 'Amplify name:Section504Status. No parcc. Individuals with disabilities who are being provided with related aids and services under Section 504 of the Rehabilitation Act of 1973, as amended. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20810'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_ell IS 'Assessment Accommodation:  English learner (EL)'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)'
;
COMMENT ON COLUMN rpt_ela_writing.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741'
;
COMMENT ON COLUMN rpt_ela_writing.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789'
;
COMMENT ON COLUMN rpt_ela_writing.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).'
;
COMMENT ON COLUMN rpt_ela_writing.gift_talent IS 'Parcc name:Gifted and Talented. An indication that the student is participating in and served by a Gifted/Talented program. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3122'
;
COMMENT ON COLUMN rpt_ela_writing.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569†'
;
COMMENT ON COLUMN rpt_ela_writing.asmt_guid IS 'Parcc name: AssessmentRegistrationTestAttemptIdentifier. Assessment Attempt Identifier (this actaully aligns with Amplify AssessmentGuid field). Unique identifier per assessment administration+poy+student+type'
;
COMMENT ON COLUMN rpt_ela_writing.asmt_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON COLUMN rpt_ela_writing.where_taken_id IS 'PARCC NAME:AssessmentSessionLocationID. The unique identifier of the place where an assessment is administered.'
;
COMMENT ON COLUMN rpt_ela_writing.where_taken_name IS 'Amplify name:AssessmentSessionLocation, PARCC name: Assessment Session Location	The description of the place where an assessment is administered.'
;
COMMENT ON COLUMN rpt_ela_writing.asmt_attempt_guid IS 'Parcc name: AssessmentRegistrationTestAttemptIdentifier. Assessment Attempt Identifier (this actaully aligns with Amplify AssessmentGuid field). Unique identifier per assessment administration+poy+student+type'
;
COMMENT ON COLUMN rpt_ela_writing.form_guid IS 'Parcc name: AssessmentFormNumber. The version of a assessment form given to a student.https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3365
'
;
COMMENT ON COLUMN rpt_ela_writing.form_name IS 'AssessmentFormName'
;
COMMENT ON COLUMN rpt_ela_writing.date_asmt_session_end IS 'Amplify name: AssessmentSessionActualEndDateTime. Date and time the assessment actually ended. Format YYYY-MM-DDTHH:MM:SS. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=4024
'
;
COMMENT ON COLUMN rpt_ela_writing.date_asmt_session_start IS 'Amplify name: AssessmentSessionActualStartDateTime. Date and time the assessment actually ended. Format YYYY-MM-DDTHH:MM:SS. https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=4023'
;
COMMENT ON COLUMN rpt_ela_writing.date_taken IS 'PARCC name:AssessmentAdministrationFinishDate	.The finish date of the time period designated for the assessment administration.Format: YYYY-MM-DD.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21673
'
;
COMMENT ON COLUMN rpt_ela_writing.date_taken_year IS 'Amplify name:AssessmentYear, Parcc name: SchoolYear format YYYY-YYYY''.  https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19847. FORMAT YYYY-YYYY, CEDS YYYY'
;
COMMENT ON COLUMN rpt_ela_writing.date_taken_poy IS 'The time of the year for a particular assessment.. varchar(32)'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_sign_video IS 'Amplify name: AccommodationAmericanSignLanguage. The specific accommodation necessary for the administration of the assessment using sign language video.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_sign_human_ela IS 'Amplify name: AccommodationSignLanguageHumanIntervention. The specific accommodation necessary for the administration of the assessment using sign language with human interventionhttps://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_sign_human_math IS 'Human Reader or Human Signer for ELA/L; Human Reader or Human Signer for Mathematics'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_braille IS 'csv name: Assessment Accommodation: Braille, parcc name: Assessment Accommodation: Braille, The specific accommodation necessary for the administration of the assessment using braille. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_close_capt IS 'csv field name: Close Captioning. Amplify name: AccommodationClosedCaptioning. The specific accommodation necessary for the administration of the assessment using closed captioning. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_text_to_speech IS 'csv name:  Assessment Accommodation: Speech Recognition System (Speech to Text). Parcc name: AccommodationTextToSpeech. Assessment Accommodation: Student Read Aloud-Synthetic (Text to Speech). The specific accommodation necessary for the administration of the assessment using text to speech. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731. 0-10.'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_alt_response IS 'Amplify name:AccommodationAlternateResponseOptions. The specific accommodation necessary for the administration of the assessment using an alternate response options. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_monitor_rsp IS 'Monitor Test Response'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_calculator IS 'csv name: Assessment Accommodation: Calculator, Amplify name: AccommodationCalculator, Parcc name: Assessment Accommodation: Calculator. The specific accommodation necessary for the administration of the assessment using a calculator. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_mult_table IS 'Amplify field: AccommodationMultiplicationTable	The specific accommodation necessary for the administration of the assessment using mulitiplication table. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_print_on_demand IS 'Amplify name:AccommodationPrintOnDemand	The specific accommodation necessary for the administration of the assessment by printing on demand. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_read_aloud IS 'csv field name: Read Aloud
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_oral_response IS 'Pearson name: Assessment Accommodation: Test Administrator Read Question Aloud. Amplify name: AccommodationReadAloud. The specific accommodation necessary for the administration of the assessment by reading aloud.	0-10Pearson Valid Values: Reader Interpreter. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_select_rsp IS 'ELA/L Selected Response or Technology Enhanced Items SpeechToText;HumanScribe;HumanSigner;ExternalATDevice;blank'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_math_rsp IS 'AccommodationMathematicsResponse SpeechToText;HumanScribe;HumanSigner;ExternalATDevice ;blank'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_dict_native_lang IS 'Amplify name: AccommodationDictionaryInNativeLanguage. Parcc name: Dictionary in Native Language'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_word_predict IS 'scv name: Word Prediction. Amplify name: AccommodationWordPrediction. Parcc name: Word Prediction
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_answer_mash IS 'csv name: Answer Mashing'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_color_contrast IS 'Amplify name: AccommodationColorContrast, Parcc name: Color Contrast. csv name: Color Contrast'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_large_print IS 'Pearson name: Tactile Graphics. AccommodationTactileGraphics
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_tactile_graphics IS 'csv field name: Tactile Graphics'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_read_synthetic IS 'Pearson name: Tactile Graphics. Amplify name: AccommodationTactileGraphics.
'
;
COMMENT ON COLUMN rpt_ela_writing.accomod_read_question IS 'csv name: Assessment Accommodation: Test Administrator Read Question Aloud, Amplify name: AccommodationReadAloud, Pacc name: Assessment Accommodation: Test Administrator Read Question Aloud	The specific accommodation necessary for the administration of the assessment by reading aloud. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_spoken IS 'pearspon name: Spoken: Support Usage'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_asl IS 'Additional American Sign Language content is provided to the user. Unlike other translated content, ASL content is presented in addition (or in parallel) to the default content.'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_close_capt IS 'Amplify name:PnpCloseCaptioning. Closed captioning and subtitling are both processes of displaying text on a television, video screen, or other visual display to provide additional or interpretive information.
'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_braille IS 'csv feidl name: Braille: Usage. parcc field name:Assessment Accommodation: Braille. The specific accommodation necessary for the administration of the assessment using braille. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21731
'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_read_aloud IS 'Amplify name: PnpReadAloud'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_alt_lang IS 'Translation of the Mathematics Assessment in Paper	PnpAlternateLanguage	Language a test needs to be translated to.	N	spa (Spanish) blank'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_math_trans IS 'TranslationoftheMathematicsAssessmentOnline'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_alt_location IS 'Additional Breaks; Separate/Alternate Location'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_small_group IS 'Small Testing Group'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_special_equip IS 'Specialized Equipment or Furniture'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_spec_area IS 'Specified Area or Setting'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_time_day IS 'Time Of Day'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_native_lang IS 'General Administration Directions Clarified in Studentís Native Language (by test administrator)'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Studentís Native Language
(by test administrator)'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_math_rsp IS 'Mathematics Response - EL'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_extend_time IS 'Extended Time'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_alt_paper_test IS 'Alternate Representation - Paper Test'
;
COMMENT ON COLUMN rpt_ela_writing.pnp_speech_2_text IS 'Pearson name: Speech to Text
'
;
COMMENT ON COLUMN rpt_ela_writing.asmt_attempt_flag IS 'csv field name: test attempted flag'
;
COMMENT ON COLUMN rpt_ela_writing.asmt_format IS 'scv field name: Test Format. Amplify name: AssessmentParticipantSessionPlatformType	Paper, Computer
'
;
COMMENT ON COLUMN rpt_ela_writing.retest_flag IS 'csv field: Retest Flag, Amplify name: AssessmentRegistrationRetestIndicator Y, N
'
;
COMMENT ON COLUMN rpt_ela_writing.rec_status IS 'C - currenrt, I - inactive'
;

-- 
-- TABLE: rpt_ela_writing 
--

ALTER TABLE rpt_ela_writing ADD 
    CONSTRAINT rpt_ela_writing_pk PRIMARY KEY (rec_id)
;

