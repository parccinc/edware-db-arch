-- 
-- TABLE: rpt_asmt_skill_m 
--

CREATE TABLE rpt_asmt_skill_m(
    asmt_guid             varchar(50)    NOT NULL,
    skill_guid            varchar(36)    NOT NULL,
    skill_name            varchar(40),
    batch_guid            varchar(50)    NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C' NOT NULL,
    create_date           date           DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_asmt_skill_m.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_asmt_skill_m.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_asmt_skill_m IS 'Math Fluency skills'
;

