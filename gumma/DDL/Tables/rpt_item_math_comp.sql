-- 
-- TABLE: rpt_item_math_comp 
--

CREATE TABLE rpt_item_math_comp(
    rec_id                int8             NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(50),
    student_item_score    numeric(4, 0),
    select_key            varchar(250),
    item_seq              int2,
    batch_guid            varchar(50)      NOT NULL,
    create_date           date             DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_item_math_comp.asmt_attempt_guid IS 'if not in xml, then would be extracted from asmt results csv'
;
COMMENT ON TABLE rpt_item_math_comp IS 'Item Math Progression;Item Math Comprehension;Item ELA Decoding;Item ELA Reading Comp,Item ELA Vocab Source'
;

-- 
-- TABLE: rpt_item_math_comp 
--

ALTER TABLE rpt_item_math_comp ADD 
    CONSTRAINT rpt_item_math_comp_pk PRIMARY KEY (rec_id)
;

