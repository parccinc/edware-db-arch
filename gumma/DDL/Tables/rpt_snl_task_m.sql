-- 
-- TABLE: rpt_snl_task_m 
--

CREATE TABLE rpt_snl_task_m(
    asmt_guid                 varchar(50)     NOT NULL,
    task_guid                 varchar(50)     NOT NULL,
    task_descript             varchar(250),
    task_short_name           char(2),
    pt_task_mode              varchar(10),
    pt_complexity             varchar(40),
    pt_stimulus_type          varchar(30),
    pt_stimulus_complexity    varchar(40),
    batch_guid                varchar(50)     NOT NULL,
    rec_status                varchar(1)      DEFAULT 'C' NOT NULL,
    create_date               date            DEFAULT CURRENT_DATE NOT NULL,
    status_change_date        date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_snl_task_m.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_snl_task_m.task_short_name IS 'Performance, Discussion, Listening tasks; PT, DR, LL'
;
COMMENT ON COLUMN rpt_snl_task_m.pt_task_mode IS 'PerformanceTaskMode'
;
COMMENT ON COLUMN rpt_snl_task_m.pt_complexity IS 'Applicable to Perfromance Task, values;Readily Accessible, Moderately Complex, Very Complex'
;
COMMENT ON COLUMN rpt_snl_task_m.pt_stimulus_type IS 'For Perfromance Task; Print--Literature; Print--Informational; Audio--Literature; Audio--Informational'
;
COMMENT ON COLUMN rpt_snl_task_m.pt_stimulus_complexity IS 'Applicable to Perfromance Task, values; Readily Accessible, Moderately Complex, Very Complex'
;
COMMENT ON COLUMN rpt_snl_task_m.rec_status IS 'C - currenrt, I - inactive'
;

