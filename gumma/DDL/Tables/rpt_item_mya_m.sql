-- 
-- TABLE: rpt_item_mya_m 
--

CREATE TABLE rpt_item_mya_m(
    rec_id                int8            NOT NULL,
    item_guid             varchar(50)     NOT NULL,
    item_name             varchar(150)    NOT NULL,
    item_descript         varchar(250),
    evidence_guid         varchar(50),
    evidence_statement    varchar(250),
    standard_guid         varchar(50),
    standard              varchar(250),
    item_ref_id           varchar(60)     NOT NULL,
    item_max_points       varchar(30),
    item_version          varchar(30),
    test_item_type        varchar(60),
    is_item_omitted       varchar(30),
    item_status           varchar(30),
    parent_item_guid      varchar(50)     NOT NULL,
    batch_guid            varchar(50)     NOT NULL,
    rec_status            varchar(1)      DEFAULT 'C' NOT NULL,
    create_date           date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_item_mya_m.is_item_omitted IS 'non-reportable'
;
COMMENT ON COLUMN rpt_item_mya_m.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_item_mya_m IS 'evidence_statement added '
;

-- 
-- TABLE: rpt_item_mya_m 
--

ALTER TABLE rpt_item_mya_m ADD 
    CONSTRAINT rpt_item_mya_m_pk PRIMARY KEY (rec_id)
;

