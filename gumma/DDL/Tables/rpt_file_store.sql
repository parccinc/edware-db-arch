-- 
-- TABLE: rpt_file_store 
--

CREATE TABLE rpt_file_store(
    rec_id                int8            NOT NULL,
    admin_guid            varchar(50)     NOT NULL,
    asmt_guid             varchar(50)     NOT NULL,
    file_type             varchar(10)     NOT NULL,
    file_name             varchar(150)    NOT NULL,
    file_data             bytea           NOT NULL,
    batch_guid            varchar(50)     NOT NULL,
    rec_status            varchar(1)      DEFAULT 'C' NOT NULL,
    create_date           date            DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_file_store.admin_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_file_store.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_file_store.file_type IS 'XML or JSON'
;
COMMENT ON COLUMN rpt_file_store.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_file_store IS 'Added status_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_file_store_ix 
--

CREATE INDEX rpt_file_store_ix ON rpt_file_store(admin_guid, asmt_guid, file_type, file_name, rec_status)
;
-- 
-- TABLE: rpt_file_store 
--

ALTER TABLE rpt_file_store ADD 
    CONSTRAINT rpt_file_store_pk PRIMARY KEY (rec_id)
;

