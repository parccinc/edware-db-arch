-- 
-- TABLE: rpt_snl_asmt_mode_m 
--

CREATE TABLE rpt_snl_asmt_mode_m(
    asmt_guid      varchar(50)    NOT NULL,
    asmt_mode      int2           NOT NULL,
    batch_guid     varchar(50)    NOT NULL,
    rec_status     varchar(1)     DEFAULT 'C' NOT NULL,
    create_date    date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_snl_asmt_mode_m.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_snl_asmt_mode_m.asmt_mode IS '1 - real Time,2 - advanced Preparation'
;
COMMENT ON COLUMN rpt_snl_asmt_mode_m.rec_status IS 'C - currenrt, I - inactive'
;

