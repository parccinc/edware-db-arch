-- 
-- TABLE: rpt_form_group_item 
--

CREATE TABLE rpt_form_group_item(
    rec_id               int8           NOT NULL,
    asmt_guid            varchar(50)    NOT NULL,
    form_guid            varchar(50)    NOT NULL,
    group_guid           varchar(50)    NOT NULL,
    item_guid            varchar(50)    NOT NULL,
    batch_guid           varchar(50)    NOT NULL,
    rec_status           varchar(1)     DEFAULT 'C' NOT NULL,
    create_date          date           DEFAULT CURRENT_DATE NOT NULL,
    staus_change_date    date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_form_group_item.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_form_group_item.form_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_form_group_item.group_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_form_group_item.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_form_group_item IS 'Added staus_change_date 10/24/14.'
;

-- 
-- INDEX: rpt_form_group_item_ix 
--

CREATE INDEX rpt_form_group_item_ix ON rpt_form_group_item(form_guid, group_guid, item_guid, rec_status)
;
-- 
-- TABLE: rpt_form_group_item 
--

ALTER TABLE rpt_form_group_item ADD 
    CONSTRAINT rpt_form_group_item_pk PRIMARY KEY (rec_id)
;

