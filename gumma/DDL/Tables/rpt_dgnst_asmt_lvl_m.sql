-- 
-- TABLE: rpt_dgnst_asmt_lvl_m 
--

CREATE TABLE rpt_dgnst_asmt_lvl_m(
    rec_id                int8           NOT NULL,
    admin_guid            varchar(50)    NOT NULL,
    asmt_lvl_design       char(2)        NOT NULL,
    batch_guid            varchar(50)    NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C' NOT NULL,
    create_date           date           DEFAULT CURRENT_DATE NOT NULL,
    status_change_date    date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_dgnst_asmt_lvl_m.admin_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_dgnst_asmt_lvl_m.asmt_lvl_design IS 'Amplify name:AssessmentLevelForWhichDesigned. The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed.KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362
'
;
COMMENT ON COLUMN rpt_dgnst_asmt_lvl_m.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_dgnst_asmt_lvl_m IS 'one asessment guid can be associated with different grade levels. v2'
;

-- 
-- TABLE: rpt_dgnst_asmt_lvl_m 
--

ALTER TABLE rpt_dgnst_asmt_lvl_m ADD 
    CONSTRAINT rpt_dgnst_asmt_lvl_m_pk PRIMARY KEY (rec_id)
;

