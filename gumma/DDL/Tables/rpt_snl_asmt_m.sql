-- 
-- TABLE: rpt_snl_asmt_m 
--

CREATE TABLE rpt_snl_asmt_m(
    rec_id                 int8            NOT NULL,
    asmt_guid              varchar(50)     NOT NULL,
    admin_guid             varchar(50)     NOT NULL,
    asmt_title             varchar(60),
    asmt_subject           varchar(15),
    document_id            varchar(30),
    set_id                 varchar(30),
    set_seq_num            int2,
    evidence_stmt1         varchar(250),
    evidence_stmt_guid1    varchar(50),
    evidence_stmt2         varchar(250),
    evidence_stmt_guid2    varchar(50),
    evidence_stmt3         varchar(250),
    evidence_stmt_guid3    varchar(50),
    evidence_stmt4         varchar(250),
    evidence_stmt_guid4    varchar(50),
    evidence_stmt5         varchar(250),
    evidence_stmt_guid5    varchar(50),
    rubric1                varchar(30),
    rubric2                varchar(30),
    batch_guid             varchar(50)     NOT NULL,
    rec_status             varchar(1)      DEFAULT 'C' NOT NULL,
    create_date            date            DEFAULT CURRENT_DATE NOT NULL,
    status_change_date     date            DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN rpt_snl_asmt_m.asmt_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_snl_asmt_m.admin_guid IS 'Paecc_name AssessmentGuid'
;
COMMENT ON COLUMN rpt_snl_asmt_m.asmt_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON COLUMN rpt_snl_asmt_m.document_id IS 'Performance task mode. Alpha numeric'
;
COMMENT ON COLUMN rpt_snl_asmt_m.set_id IS 'ID of the set. Alpha numeric'
;
COMMENT ON COLUMN rpt_snl_asmt_m.set_seq_num IS 'Sequence No. 1-20 '
;
COMMENT ON COLUMN rpt_snl_asmt_m.rubric1 IS 'Presentation for mode1/ Listening for mode2'
;
COMMENT ON COLUMN rpt_snl_asmt_m.rubric2 IS 'Discussion'
;
COMMENT ON COLUMN rpt_snl_asmt_m.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE rpt_snl_asmt_m IS 'Contains evidenceStatements and rubrics'
;

-- 
-- TABLE: rpt_snl_asmt_m 
--

ALTER TABLE rpt_snl_asmt_m ADD 
    CONSTRAINT rpt_snl_asmt_m_pk PRIMARY KEY (rec_id)
;

