CREATE VIEW vw_ela_diagnost_staff AS
SELECT z.staff_guid
FROM (
  SELECT a.staff_guid
   FROM (
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_ela_decod
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_ela_read_comp
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_ela_read_flu
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_ela_vocab
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_ela_writing
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_reader_motiv
	WHERE rec_status = 'C'
	GROUP BY staff_guid
     ) a
 EXCEPT
 SELECT staff_guid
 FROM lkp_staff_xref
) z