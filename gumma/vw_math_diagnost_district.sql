CREATE VIEW vw_math_diagnost_district AS
SELECT z.district_guid
FROM (
   SELECT a.district_guid
   FROM (
	SELECT district_guid, count(*) as rec_count
	FROM rpt_math_comp
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_math_flu
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_math_progress
	WHERE rec_status = 'C'
	GROUP BY district_guid
     ) a
	EXCEPT
	SELECT district_guid
	FROM lkp_district_xref
) z