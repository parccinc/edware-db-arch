CREATE VIEW vw_math_diagnost_student AS
SELECT z.student_guid
FROM (
   SELECT a.student_guid
   FROM (
	SELECT student_guid, count(*) as rec_count
	FROM rpt_math_comp
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_math_flu
	WHERE rec_status = 'C'
	GROUP BY student_guid
	UNION ALL
	SELECT student_guid, count(*) as rec_count
	FROM rpt_math_progress
	WHERE rec_status = 'C'
	GROUP BY student_guid
     ) a
	EXCEPT
	SELECT student_guid
	FROM lkp_student_xref
) z