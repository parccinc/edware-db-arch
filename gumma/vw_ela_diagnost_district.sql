CREATE VIEW vw_ela_diagnost_district AS
SELECT z.district_guid
FROM (
  SELECT a.district_guid
   FROM (
	SELECT district_guid, count(*) as rec_count
	FROM rpt_ela_decod
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_ela_read_comp
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_ela_read_flu
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_ela_vocab
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_ela_writing
	WHERE rec_status = 'C'
	GROUP BY district_guid
	UNION ALL
	SELECT district_guid, count(*) as rec_count
	FROM rpt_reader_motiv
	WHERE rec_status = 'C'
	GROUP BY district_guid
     ) a
 EXCEPT
 SELECT district_guid
 FROM lkp_district_xref
) z
