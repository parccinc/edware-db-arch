CREATE VIEW vw_ela_sum_item_con AS
SELECT si.rec_id AS rec_id
, t.state_key AS state_key
, y.year_key AS year_key
, y.poy_key AS poy_key
, su.subject_key AS subject_key
, g.grade_key AS grade_key
, CASE WHEN  UPPER(ela.student_sex) = 'M' THEN 1 
    WHEN UPPER(ela.student_sex) = 'F' THEN 0 
  ELSE -1 END AS sex_key
, CASE WHEN ethn_hisp_latino = 'Y' THEN 1
 WHEN ethn_indian_alaska = 'Y' THEN 2
 WHEN ethn_asian = 'Y' THEN 3
 WHEN ethn_black = 'Y' THEN 4
 WHEN ethn_hawai = 'Y' THEN 5
 WHEN ethn_white = 'Y' THEN 6
 WHEN ethn_2_more = 'Y' THEN 7
 ELSE -1 
END AS ethnicity_key
, CASE WHEN ela.lep_status = 'Y' THEN 1
   WHEN ela.lep_status = 'N' THEN 0 
  ELSE -1 END AS lep_key
, ela.record_type AS record_type
, ela.date_taken_poy AS date_taken_poy
, ela.state_code AS state_code
, ela.student_sex AS student_sex
, ela.student_grade AS student_grade
, CASE WHEN ethn_hisp_latino = 'Y' THEN 'HISP'
       WHEN ethn_indian_alaska = 'Y' THEN 'IND'
       WHEN ethn_asian = 'Y' THEN 'ASIAN'
       WHEN ethn_black = 'Y' THEN 'BLK'
       WHEN ethn_hawai = 'Y' THEN 'HAW'
       WHEN ethn_white = 'Y' THEN 'WHT'
       WHEN ethn_2_more = 'Y' THEN 'MULT'
  ELSE 'NA' END AS ethnicity
, ela.ethn_hisp_latino AS ethn_hisp_latino
, CASE WHEN  UPPER(ela.ethn_hisp_latino) = 'Y' THEN 1 
    WHEN UPPER(ela.student_sex) = 'N' THEN 0 
  ELSE -1 END AS is_hisp_latino
, ela.ethn_indian_alaska AS ethn_indian_alaska
, CASE WHEN  UPPER(ela.ethn_indian_alaska) = 'Y' THEN 1 
    WHEN UPPER(ela.ethn_indian_alaska) = 'N' THEN 0 
  ELSE -1 END AS is_indian_alaska
, ela.ethn_asian AS ethn_asian
, CASE WHEN  UPPER(ela.ethn_asian) = 'Y' THEN 1 
    WHEN UPPER(ela.ethn_asian) = 'N' THEN 0 
  ELSE -1 END AS is_asian
, ela.ethn_black AS ethn_black
, CASE WHEN  UPPER(ela.ethn_black) = 'Y' THEN 1 
    WHEN UPPER(ela.ethn_black) = 'N' THEN 0 
  ELSE -1 END AS is_black
, ela.ethn_hawai AS ethn_hawai
, CASE WHEN  UPPER(ela.ethn_hawai) = 'Y' THEN 1 
    WHEN UPPER(ela.ethn_hawai) = 'N' THEN 0 
  ELSE -1 END AS is_hawai
, ela.ethn_white AS ethn_white
, CASE WHEN  UPPER(ela.ethn_white) = 'Y' THEN 1 
    WHEN UPPER(ela.ethn_white) = 'N' THEN 0 
  ELSE -1 END AS is_white
, ela.ethn_2_more AS ethn_2_more
, CASE WHEN  UPPER(ela.ethn_2_more) = 'Y' THEN 1 
    WHEN UPPER(ela.ethn_2_more) = 'N' THEN 0 
  ELSE -1 END AS is_2_more
, ela.lep_status AS lep_status
, ela.section_504 AS section_504
, ela.econo_disadvantage AS econo_disadvantage
, CASE WHEN  UPPER(ela.econo_disadvantage) = 'Y' THEN 1 
	WHEN UPPER(ela.econo_disadvantage) = 'N' THEN 0 
	ELSE -1 END AS is_econo_disadvantage
, ela.migrant_status AS migrant_status
, CASE WHEN  UPPER(ela.migrant_status) = 'Y' THEN 1 
WHEN UPPER(ela.migrant_status) = 'N' THEN 0 
ELSE -1 END AS is_migrant
, ela.ell AS ell
, CASE WHEN  UPPER(ela.ell) = 'Y' THEN 1 
WHEN UPPER(ela.ell) = 'N' THEN 0 
ELSE -1 END AS is_ell
, ela.gift_talent AS gift_talent
, CASE WHEN  UPPER(ela.gift_talent) = 'Y' THEN 1 
WHEN UPPER(ela.gift_talent) = 'N' THEN 0 
ELSE -1 END AS is_gift_talent
, ela.disabil_student AS disabil_student
, CASE WHEN  UPPER(ela.disabil_student) = 'Y' THEN 1 
    WHEN UPPER(ela.disabil_student) = 'N' THEN 0 
  ELSE -1 END AS is_disabil_student
, ela.primary_disabil_type AS primary_disabil_type
, CASE WHEN primary_disabil_type = 'AUT' THEN 1
   WHEN primary_disabil_type = 'DB' THEN 2
   WHEN primary_disabil_type = 'DD' THEN 3
   WHEN primary_disabil_type = 'EMN' THEN 4
   WHEN primary_disabil_type = 'HI' THEN 5
   WHEN primary_disabil_type = 'ID' THEN 6
   WHEN primary_disabil_type = 'MD' THEN 7
   WHEN primary_disabil_type = 'OI' THEN 8
   WHEN primary_disabil_type = 'OHI' THEN 9
   WHEN primary_disabil_type = 'SLD' THEN 10
   WHEN primary_disabil_type = 'SLI' THEN 11
   WHEN primary_disabil_type = 'TBI' THEN 12
   WHEN primary_disabil_type = 'VI' THEN 13 
  ELSE -1
  END AS primary_disabil_type_key
, CASE WHEN primary_disabil_type = 'AUT' THEN 1 ELSE -1 END AS is_aut
, CASE WHEN primary_disabil_type = 'DB' THEN 1 ELSE -1 END AS is_db
, CASE WHEN primary_disabil_type = 'DD' THEN 1 ELSE -1 END AS is_dd
, CASE WHEN primary_disabil_type = 'EMN' THEN 1 ELSE -1 END AS is_emn
, CASE WHEN primary_disabil_type = 'HI' THEN 1 ELSE -1 END AS is_hi
, CASE WHEN primary_disabil_type = 'MD' THEN 1 ELSE -1 END AS is_md
, CASE WHEN primary_disabil_type = 'ID' THEN 1 ELSE -1 END AS is_id
, CASE WHEN primary_disabil_type = 'OI' THEN 1 ELSE -1 END AS is_oi
, CASE WHEN primary_disabil_type = 'OHI' THEN 1 ELSE -1 END AS is_ohi
, CASE WHEN primary_disabil_type = 'SLD' THEN 1 ELSE -1 END AS is_sld
, CASE WHEN primary_disabil_type = 'SLI' THEN 1 ELSE -1 END AS is_sli
, CASE WHEN primary_disabil_type = 'TBI' THEN 1 ELSE -1 END AS is_tbi
, CASE WHEN primary_disabil_type = 'VI' THEN 1 ELSE -1 END AS is_vi
, ela.summ_asmt_guid AS summ_asmt_guid
, ela.asmt_subject AS asmt_subject
, ela.asmt_attempt_guid AS asmt_attempt_guid
, ela.date_asmt_session_end AS date_asmt_session_end
, ela.date_asmt_session_start AS date_asmt_session_start
, ela.date_taken_year AS date_taken_year
, ela.invalidation_code AS invalidation_code
, ela.invalidation_reason AS invalidation_reason
, ela.summative_flag AS summative_flag
, ela.not_tested_code AS not_tested_code
, ela.not_tested_reason AS not_tested_reason
, ela.report_suppression_code AS report_suppression_code
, ela.report_suppression_action AS report_suppression_action
, ela.user_agent AS user_agent
, si.student_item_score AS student_item_score
, ri.item_max_points AS item_max_points
, ri.item_guid AS item_guid
--, lix.item_key AS item_key
, ri.item_name AS item_name
, ri.item_descript AS item_descript
, ri.item_ref_id AS item_ref_id
, ri.test_item_type AS test_item_type
, ri.is_item_omitted AS is_item_omitted
, ri.item_status AS item_status
FROM  ref_state_xref t JOIN (SELECT * FROM rpt_ela_sum WHERE rec_status = 'C') ela 
ON (t.state_code = ela.state_code)
JOIN (SELECT * FROM rpt_student_item_score WHERE rec_status = 'C') si ON (ela.asmt_attempt_guid = si.asmt_attempt_guid)
JOIN rpt_item ri ON ( ri.item_guid = si.item_guid)
--JOIN lkp_tnt_item_xref_obsolete lix ON (lix.item_guid = ri.item_guid)
--JOIN lkp_tnt_item_type lit ON (lit.test_item_type = ri.test_item_type)
JOIN ref_poy_year_xref y ON (y.poy = ela.date_taken_poy AND y.year = ela.date_taken_year )
JOIN ref_subject_xref su ON (su.asmt_subject = ela.asmt_subject)
JOIN ref_grade_xref g ON (g.student_grade = ela.student_grade)

