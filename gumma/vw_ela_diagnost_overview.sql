CREATE VIEW vw_ela_diagnost_overview AS
SELECT a.record_type,
  1 AS state_key ,
  y.year_key ,
  y.poy_key ,
  su.subject_key ,
  d.district_key ,
  sch.school_key ,
  tk.teacher_key ,
  g.grade_key ,
  stu.student_key,
  CASE
    WHEN UPPER(a.student_sex) = 'M'
    THEN 1
    WHEN UPPER(a.student_sex) = 'F'
    THEN 0
    ELSE -1
  END AS sex_key,
  CASE
    WHEN ethn_hisp_latino = 'Y'
    THEN 1
    WHEN ethn_indian_alaska = 'Y'
    THEN 2
    WHEN ethn_asian = 'Y'
    THEN 3
    WHEN ethn_black = 'Y'
    THEN 4
    WHEN ethn_hawai = 'Y'
    THEN 5
    WHEN ethn_white = 'Y'
    THEN 6
    WHEN ethn_2_more = 'Y'
    THEN 7
    ELSE -1
  END AS ethnicity_key,
  CASE
    WHEN a.lep_status = 'Y'
    THEN 1
    WHEN a.lep_status = 'N'
    THEN 0
    ELSE -1
  END AS lep_key, 
  a.date_taken_poy,
  a.state_code,
  a.district_guid,
  a.district_name,
  a.school_guid,
  a.school_name,
  a.school_short_name,
  a.student_parcc_id,
  a.student_last_name ||', ' ||a.student_first_name ||
  CASE
    WHEN a.student_middle_name IS NOT NULL THEN ', ' ||a.student_middle_name
    ELSE '' END AS student_full_name,
  a.student_sex,
  a.student_dob,
  a.student_grade,
  CASE
    WHEN ethn_hisp_latino = 'Y' THEN 'HISP'
    WHEN ethn_indian_alaska = 'Y' THEN 'IND'
    WHEN ethn_asian = 'Y' THEN 'ASIAN'
    WHEN ethn_black = 'Y' THEN 'BLK'
    WHEN ethn_hawai = 'Y' THEN 'HAW'
    WHEN ethn_white = 'Y' THEN 'WHT'
    WHEN ethn_2_more = 'Y' THEN 'MULT'
    ELSE 'NA' END AS ethnicity,
  a.ethn_hisp_latino,
  CASE
    WHEN UPPER(a.ethn_hisp_latino) = 'Y' THEN 1
    WHEN UPPER(a.student_sex) = 'N' THEN 0
    ELSE -1 END AS is_hisp_latino,
  a.ethn_indian_alaska,
  CASE
    WHEN UPPER(a.ethn_indian_alaska) = 'Y'
    THEN 1
    WHEN UPPER(a.ethn_indian_alaska) = 'N'
    THEN 0
    ELSE -1
  END AS is_indian_alaska,
  a.ethn_asian,
  CASE
    WHEN UPPER(a.ethn_asian) = 'Y'
    THEN 1
    WHEN UPPER(a.ethn_asian) = 'N'
    THEN 0
    ELSE -1
  END AS is_asian,
  a.ethn_black,
  CASE
    WHEN UPPER(a.ethn_black) = 'Y'
    THEN 1
    WHEN UPPER(a.ethn_black) = 'N'
    THEN 0
    ELSE -1
  END AS is_black,
  a.ethn_hawai,
  CASE
    WHEN UPPER(a.ethn_hawai) = 'Y'
    THEN 1
    WHEN UPPER(a.ethn_hawai) = 'N'
    THEN 0
    ELSE -1
  END AS is_hawai,
  a.ethn_white,
  CASE
    WHEN UPPER(a.ethn_white) = 'Y'
    THEN 1
    WHEN UPPER(a.ethn_white) = 'N'
    THEN 0
    ELSE -1
  END AS is_white,
  a.ethn_2_more,
  CASE
    WHEN UPPER(a.ethn_2_more) = 'Y'
    THEN 1
    WHEN UPPER(a.ethn_2_more) = 'N'
    THEN 0
    ELSE -1
  END AS is_2_more,
  a.lep_status,
  a.econo_disadvantage,
  CASE
    WHEN UPPER(a.econo_disadvantage) = 'Y'
    THEN 1
    WHEN UPPER(a.econo_disadvantage) = 'N'
    THEN 0
    ELSE -1
  END AS is_econo_disadvantage,
  a.migrant_status,
  CASE
    WHEN UPPER(a.migrant_status) = 'Y'
    THEN 1
    WHEN UPPER(a.migrant_status) = 'N'
    THEN 0
    ELSE -1
  END AS is_migrant,
  a.ell,
  CASE
    WHEN UPPER(a.ell) = 'Y'
    THEN 1
    WHEN UPPER(a.ell) = 'N'
    THEN 0
    ELSE -1
  END AS is_ell,
  a.gift_talent,
  CASE
    WHEN UPPER(a.gift_talent) = 'Y'
    THEN 1
    WHEN UPPER(a.gift_talent) = 'N'
    THEN 0
    ELSE -1
  END AS is_gift_talent,
  a.disabil_student,
  CASE
    WHEN UPPER(a.disabil_student) = 'Y'
    THEN 1
    WHEN UPPER(a.disabil_student) = 'N'
    THEN 0
    ELSE -1
  END AS is_disabil_student,
  a.primary_disabil_type,
  CASE
    WHEN primary_disabil_type = 'AUT'
    THEN 1
    WHEN primary_disabil_type = 'DB'
    THEN 2
    WHEN primary_disabil_type = 'DD'
    THEN 3
    WHEN primary_disabil_type = 'EMN'
    THEN 4
    WHEN primary_disabil_type = 'HI'
    THEN 5
    WHEN primary_disabil_type = 'ID'
    THEN 6
    WHEN primary_disabil_type = 'MD'
    THEN 7
    WHEN primary_disabil_type = 'OI'
    THEN 8
    WHEN primary_disabil_type = 'OHI'
    THEN 9
    WHEN primary_disabil_type = 'SLD'
    THEN 10
    WHEN primary_disabil_type = 'SLI'
    THEN 11
    WHEN primary_disabil_type = 'TBI'
    THEN 12
    WHEN primary_disabil_type = 'VI'
    THEN 13
    ELSE -1
  END AS primary_disabil_type_key,
  CASE
    WHEN primary_disabil_type = 'AUT'
    THEN 1
    ELSE -1
  END AS is_out,
  CASE
    WHEN primary_disabil_type = 'DB'
    THEN 1
    ELSE -1
  END AS is_db,
  CASE
    WHEN primary_disabil_type = 'DD'
    THEN 1
    ELSE -1
  END AS is_dd,
  CASE
    WHEN primary_disabil_type = 'EMN'
    THEN 1
    ELSE -1
  END AS is_emn,
  CASE
    WHEN primary_disabil_type = 'HI'
    THEN 1
    ELSE -1
  END AS is_hi,
  CASE
    WHEN primary_disabil_type = 'MD'
    THEN 1
    ELSE -1
  END AS is_md,
  CASE
    WHEN primary_disabil_type = 'ID'
    THEN 1
    ELSE -1
  END AS is_id,
  CASE
    WHEN primary_disabil_type = 'OI'
    THEN 1
    ELSE -1
  END AS is_oi,
  CASE
    WHEN primary_disabil_type = 'OHI'
    THEN 1
    ELSE -1
  END AS is_ohi,
  CASE
    WHEN primary_disabil_type = 'SLD'
    THEN 1
    ELSE -1
  END AS is_sld,
  CASE
    WHEN primary_disabil_type = 'SLI'
    THEN 1
    ELSE -1
  END AS is_sli,
  CASE
    WHEN primary_disabil_type = 'TBI'
    THEN 1
    ELSE -1
  END AS is_tbi,
  CASE
    WHEN primary_disabil_type = 'VI'
    THEN 1
    ELSE -1
  END AS is_vi,
  a.asmt_guid,
  a.asmt_subject,
  a.where_taken_id,
  a.where_taken_name,
  a.staff_id,
  a.classroom_id,
  a.asmt_attempt_guid,
  a.date_asmt_session_end,
  a.date_asmt_session_start,
  a.date_taken_year,
  a.retest_flag,
  a.invalidation_code,
  a.invalidation_reason,
  a.summative_flag,
  a.not_tested_code,
  a.not_tested_reason,
  a.report_suppression_code,
  a.report_suppression_action,
  a.user_agent
FROM lkp_student_xref stu JOIN
(SELECT record_type,
student_sex,
ethn_hisp_latino,
ethn_indian_alaska,
ethn_asian,
ethn_black,
ethn_hawai,
ethn_white,
ethn_2_more,
lep_status,
econo_disadvantage,
ell,
migrant_status,
gift_talent,
disabil_student,
primary_disabil_type,
date_taken_poy,
state_code,
district_guid,
district_name,
school_guid,
school_name,
school_short_name,
student_parcc_id,
student_last_name,
student_first_name,
student_middle_name,
student_sex,
student_dob,
student_grade,
asmt_guid,
asmt_subject,
where_taken_id,
where_taken_name,
staff_id,
classroom_id,
asmt_attempt_guid,
date_asmt_session_end,
date_asmt_session_start,
date_taken_year,
retest_flag,
invalidation_code,
invalidation_reason,
summative_flag,
not_tested_code,
not_tested_reason,
report_suppression_code,
report_suppression_action,
user_agent
FROM rpt_ela_decod
WHERE rec_status = 'C'
UNION ALL
SELECT record_type,
student_sex,
ethn_hisp_latino,
ethn_indian_alaska,
ethn_asian,
ethn_black,
ethn_hawai,
ethn_white,
ethn_2_more,
lep_status,
econo_disadvantage,
ell,
migrant_status,
gift_talent,
disabil_student,
primary_disabil_type,
date_taken_poy,
state_code,
district_guid,
district_name,
school_guid,
school_name,
school_short_name,
student_parcc_id,
student_last_name,
student_first_name,
student_middle_name,
student_sex,
student_dob,
student_grade,
asmt_guid,
asmt_subject,
where_taken_id,
where_taken_name,
staff_id,
classroom_id,
asmt_attempt_guid,
date_asmt_session_end,
date_asmt_session_start,
date_taken_year,
retest_flag,
invalidation_code,
invalidation_reason,
summative_flag,
not_tested_code,
not_tested_reason,
report_suppression_code,
report_suppression_action,
user_agent
FROM rpt_ela_read_comp
WHERE rec_status = 'C'
UNION ALL
SELECT record_type,
student_sex,
ethn_hisp_latino,
ethn_indian_alaska,
ethn_asian,
ethn_black,
ethn_hawai,
ethn_white,
ethn_2_more,
lep_status,
econo_disadvantage,
ell,
migrant_status,
gift_talent,
disabil_student,
primary_disabil_type,
date_taken_poy,
state_code,
district_guid,
district_name,
school_guid,
school_name,
school_short_name,
student_parcc_id,
student_last_name,
student_first_name,
student_middle_name,
student_sex,
student_dob,
student_grade,
asmt_guid,
asmt_subject,
where_taken_id,
where_taken_name,
staff_id,
classroom_id,
asmt_attempt_guid,
date_asmt_session_end,
date_asmt_session_start,
date_taken_year,
retest_flag,
invalidation_code,
invalidation_reason,
summative_flag,
not_tested_code,
not_tested_reason,
report_suppression_code,
report_suppression_action,
user_agent
FROM rpt_ela_read_flu
WHERE rec_status = 'C'
UNION ALL
SELECT record_type,
student_sex,
ethn_hisp_latino,
ethn_indian_alaska,
ethn_asian,
ethn_black,
ethn_hawai,
ethn_white,
ethn_2_more,
lep_status,
econo_disadvantage,
ell,
migrant_status,
gift_talent,
disabil_student,
primary_disabil_type,
date_taken_poy,
state_code,
district_guid,
district_name,
school_guid,
school_name,
school_short_name,
student_parcc_id,
student_last_name,
student_first_name,
student_middle_name,
student_sex,
student_dob,
student_grade,
asmt_guid,
asmt_subject,
where_taken_id,
where_taken_name,
staff_id,
classroom_id,
asmt_attempt_guid,
date_asmt_session_end,
date_asmt_session_start,
date_taken_year,
retest_flag,
invalidation_code,
invalidation_reason,
summative_flag,
not_tested_code,
not_tested_reason,
report_suppression_code,
report_suppression_action,
user_agent
FROM rpt_ela_vocab
WHERE rec_status = 'C'
UNION ALL
SELECT record_type,
student_sex,
ethn_hisp_latino,
ethn_indian_alaska,
ethn_asian,
ethn_black,
ethn_hawai,
ethn_white,
ethn_2_more,
lep_status,
econo_disadvantage,
ell,
migrant_status,
gift_talent,
disabil_student,
primary_disabil_type,
date_taken_poy,
state_code,
district_guid,
district_name,
school_guid,
school_name,
school_short_name,
student_parcc_id,
student_last_name,
student_first_name,
student_middle_name,
student_sex,
student_dob,
student_grade,
asmt_guid,
asmt_subject,
where_taken_id,
where_taken_name,
staff_id,
classroom_id,
asmt_attempt_guid,
date_asmt_session_end,
date_asmt_session_start,
date_taken_year,
retest_flag,
invalidation_code,
invalidation_reason,
summative_flag,
not_tested_code,
not_tested_reason,
report_suppression_code,
report_suppression_action,
user_agent
FROM rpt_ela_writing
WHERE rec_status = 'C'
UNION ALL
SELECT record_type,
student_sex,
ethn_hisp_latino,
ethn_indian_alaska,
ethn_asian,
ethn_black,
ethn_hawai,
ethn_white,
ethn_2_more,
lep_status,
econo_disadvantage,
ell,
migrant_status,
gift_talent,
disabil_student,
primary_disabil_type,
date_taken_poy,
state_code,
district_guid,
district_name,
school_guid,
school_name,
school_short_name,
student_parcc_id,
student_last_name,
student_first_name,
student_middle_name,
student_sex,
student_dob,
student_grade,
asmt_guid,
asmt_subject,
where_taken_id,
where_taken_name,
staff_id,
classroom_id,
asmt_attempt_guid,
date_asmt_session_end,
date_asmt_session_start,
date_taken_year,
retest_flag,
invalidation_code,
invalidation_reason,
summative_flag,
not_tested_code,
not_tested_reason,
report_suppression_code,
report_suppression_action,
user_agent
FROM rpt_reader_motiv
WHERE rec_status = 'C'
) a ON (stu.student_parcc_id = a.student_parcc_id)
JOIN lkp_district_xref d ON (d.district_guid = a.district_guid)
JOIN lkp_staff_xref tk ON ( tk.staff_id = a.staff_id )
JOIN ref_grade_xref g ON (g.student_grade = a.student_grade)
JOIN ref_poy_year_xref y ON (y.poy  = a.date_taken_poy AND y.year = a.date_taken_year ) 
JOIN ref_subject_xref su ON (su.asmt_subject = a.asmt_subject)
JOIN lkp_school_xref sch ON (sch.school_guid = a.school_guid)
