CREATE VIEW vw_math_diagnost_staff AS
SELECT z.staff_guid
FROM (
   SELECT a.staff_guid
   FROM (
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_math_comp
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_math_flu
	WHERE rec_status = 'C'
	GROUP BY staff_guid
	UNION ALL
	SELECT staff_guid, count(*) as rec_count
	FROM rpt_math_progress
	WHERE rec_status = 'C'
	GROUP BY staff_guid
     ) a
	EXCEPT
	SELECT staff_guid
	FROM lkp_staff_xref
) z