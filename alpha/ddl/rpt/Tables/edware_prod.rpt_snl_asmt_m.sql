-- 
-- TABLE: rpt_snl_asmt_m 
--

CREATE TABLE rpt_snl_asmt_m(
    rec_id                 char(10)        NOT NULL,
    batch_guid             varchar(50)     NOT NULL,
    admin_guid             varchar(50)     NOT NULL,
    asmt_guid              varchar(50)     NOT NULL,
    asmt_title             varchar(60),
    asmt_subject           varchar(15),
    document_id            varchar(40),
    set_id                 varchar(35),
    set_seq_num            varchar(10),
    evidence_stmt1         varchar(250),
    evidence_stmt_guid1    varchar(50),
    evidence_stmt2         varchar(250),
    evidence_stmt_guid2    varchar(50),
    evidence_stmt3         varchar(250),
    evidence_stmt_guid3    varchar(50),
    evidence_stmt4         varchar(250),
    evidence_stmt_guid4    varchar(50),
    evidence_stmt5         varchar(250),
    evidence_stmt_guid5    varchar(50),
    rubric1                varchar(30),
    rubric2                varchar(30),
    rec_status             char(1)         DEFAULT 'C') NOT NULL,
    create_date            timestamp       NOT NULL,
    status_change_date     timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_snl_asmt_m 
--

ALTER TABLE rpt_snl_asmt_m ADD 
    CONSTRAINT rpt_snl_asmt_m_pkey PRIMARY KEY (rec_id, asmt_guid)
;

