-- 
-- TABLE: rpt_sum_test_level_m 
--

CREATE TABLE rpt_sum_test_level_m(
    rec_id                char(10)         NOT NULL,
    test_code             varchar(20)      NOT NULL,
    perf_lvl_id           varchar(30)      NOT NULL,
    cutpoint_label        varchar(50)      NOT NULL,
    cutpoint_low          numeric(4, 0)    NOT NULL,
    cutpoint_upper        numeric(4, 0)    NOT NULL,
    batch_guid            varchar(50)      NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL,
    status_change_date    timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_sum_test_level_m 
--

ALTER TABLE rpt_sum_test_level_m ADD 
    CONSTRAINT rpt_sum_test_level_m_pkey PRIMARY KEY (rec_id)
;

