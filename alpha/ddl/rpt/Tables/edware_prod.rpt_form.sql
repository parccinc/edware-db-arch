-- 
-- TABLE: rpt_form 
--

CREATE TABLE rpt_form(
    form_guid                        varchar(50)     NOT NULL,
    asmt_guid                        varchar(50)     NOT NULL,
    form_format                      varchar(50),
    form_name                        varchar(100),
    form_code                        varchar(100),
    form_ref_id                      varchar(60),
    form_version                     varchar(30),
    asmt_score_max                   int2,
    asmt_perf_lvl1_guid              varchar(50),
    asmt_perf_lvl1_cutpoint_label    varchar(30),
    asmt_perf_lvl1_cutpoint_low      int2,
    asmt_perf_lvl1_cutpoint_upp      int2,
    asmt_perf_lvl2_guid              varchar(50),
    asmt_perf_lvl2_cutpoint_label    varchar(30),
    asmt_perf_lvl2_cutpoint_low      int2,
    asmt_perf_lvl2_cutpoint_upp      int2,
    asmt_perf_lvl3_guid              varchar(50),
    asmt_perf_lvl3_cutpoint_label    varchar(30),
    asmt_perf_lvl3_cutpoint_low      int2,
    asmt_perf_lvl3_cutpoint_upp      int2,
    asmt_perf_lvl4_guid              varchar(50),
    asmt_perf_lvl4_cutpoint_label    varchar(30),
    asmt_perf_lvl4_cutpoint_low      int2,
    asmt_perf_lvl4_cutpoint_upp      int2,
    asmt_perf_lvl5_guid              varchar(50),
    asmt_perf_lvl5_cutpoint_label    varchar(30),
    asmt_perf_lvl5_cutpoint_low      int2,
    asmt_perf_lvl5_cutpoint_upp      int2,
    batch_guid                       varchar(50)     NOT NULL,
    rec_id                           char(10)        NOT NULL,
    create_date                      timestamp       NOT NULL,
    rec_status                       varchar(1)      DEFAULT 'C') NOT NULL,
    status_change_date               timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- INDEX: rpt_form_ix_1 
--

CREATE INDEX rpt_form_ix_1 ON rpt_form(form_guid, rec_status)
;
-- 
-- TABLE: rpt_form 
--

ALTER TABLE rpt_form ADD 
    CONSTRAINT rpt_form_pkey PRIMARY KEY (rec_id)
;

