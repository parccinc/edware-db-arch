-- 
-- TABLE: rpt_item_ela_vocab 
--

CREATE TABLE rpt_item_ela_vocab(
    rec_id                char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    student_item_score    numeric(3, 0),
    select_key            varchar(250),
    item_seq              int2,
    batch_guid            varchar(50)      NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL,
    status_change_date    timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_item_ela_vocab 
--

ALTER TABLE rpt_item_ela_vocab ADD 
    CONSTRAINT rpt_item_ela_vocab_pkey PRIMARY KEY (rec_id)
;

