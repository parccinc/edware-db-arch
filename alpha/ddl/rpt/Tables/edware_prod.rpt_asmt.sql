-- 
-- TABLE: rpt_asmt 
--

CREATE TABLE rpt_asmt(
    rec_id                char(10)       NOT NULL,
    asmt_guid             varchar(50)    NOT NULL,
    admin_guid            varchar(50)    NOT NULL,
    asmt_title            varchar(60),
    asmt_subject          varchar(15),
    batch_guid            varchar(50)    NOT NULL,
    create_date           timestamp      NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C') NOT NULL,
    status_change_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- INDEX: rpt_asmt_ix 
--

CREATE INDEX rpt_asmt_ix ON rpt_asmt(asmt_guid, rec_status)
;
-- 
-- TABLE: rpt_asmt 
--

ALTER TABLE rpt_asmt ADD 
    CONSTRAINT rpt_asmt_pkey PRIMARY KEY (rec_id)
;

