-- 
-- TABLE: rpt_item_math_comp 
--

CREATE TABLE rpt_item_math_comp(
    rec_id                char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    student_item_score    numeric(3, 0),
    select_key            varchar(250),
    item_seq              int2,
    batch_guid            varchar(50)      NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL,
    status_change_date    timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_item_math_comp 
--

ALTER TABLE rpt_item_math_comp ADD 
    CONSTRAINT rpt_item_math_comp_pkey PRIMARY KEY (rec_id)
;

