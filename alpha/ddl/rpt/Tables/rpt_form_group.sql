-- 
-- TABLE: rpt_form_group 
--

CREATE TABLE rpt_form_group(
    asmt_guid             varchar(50)     NOT NULL,
    form_guid             varchar(50)     NOT NULL,
    group_guid            varchar(50)     NOT NULL,
    group_code            varchar(50),
    group_name            varchar(60),
    group_desc            varchar(256),
    group_type            varchar(30),
    parent_group_guid     varchar(50),
    group_ref_id          varchar(60),
    batch_guid            varchar(50)     NOT NULL,
    rec_id                char(10)        NOT NULL,
    create_date           timestamp       NOT NULL,
    rec_status            varchar(1)      DEFAULT 'C') NOT NULL,
    status_change_date    timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- INDEX: rpt_form_group_ix_1 
--

CREATE INDEX rpt_form_group_ix_1 ON rpt_form_group(asmt_guid, form_guid, rec_status)
;
-- 
-- TABLE: rpt_form_group 
--

ALTER TABLE rpt_form_group ADD 
    CONSTRAINT rpt_form_group_pkey PRIMARY KEY (rec_id)
;

