-- 
-- TABLE: rpt_math_sum_item_score 
--

CREATE TABLE rpt_math_sum_item_score(
    rec_id                char(10)       NOT NULL,
    year                  varchar(9)     NOT NULL,
    state_code            char(2)        NOT NULL,
    resp_dist_id          varchar(15),
    resp_dist_name        varchar(60),
    resp_school_id        varchar(15),
    resp_school_name      varchar(60),
    test_dist_id          varchar(15),
    test_dist_name        varchar(60),
    test_school_id        varchar(15),
    test_school_name      varchar(60),
    admin_code            varchar(15)    NOT NULL,
    test_code             varchar(20)    NOT NULL,
    asmt_grade            varchar(5),
    asmt_subject          varchar(35)    NOT NULL,
    form_id               varchar(14)    NOT NULL,
    form_format           varchar(5)     NOT NULL,
    student_parcc_id      varchar(40)    NOT NULL,
    student_local_id      varchar(30)    NOT NULL,
    student_grade         varchar(11)    NOT NULL,
    test_uuid             varchar(36)    NOT NULL,
    item_uin              varchar(35)    NOT NULL,
    item_max_score        int2           NOT NULL,
    parent_item_score     int2           NOT NULL,
    batch_guid            varchar(50)    NOT NULL,
    create_date           timestamp      NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C') NOT NULL,
    status_change_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_math_sum_item_score 
--

ALTER TABLE rpt_math_sum_item_score ADD 
    CONSTRAINT rpt_math_sum_item_score_pkey PRIMARY KEY (rec_id)
;

