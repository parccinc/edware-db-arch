-- 
-- TABLE: rpt_file_store 
--

CREATE TABLE rpt_file_store(
    rec_id                char(10)        NOT NULL,
    admin_guid            varchar(50)     NOT NULL,
    asmt_guid             varchar(50)     NOT NULL,
    file_type             varchar(10)     NOT NULL,
    file_name             varchar(150)    NOT NULL,
    page_no               int8            NOT NULL,
    file_data             bytea           NOT NULL,
    batch_guid            varchar(50)     NOT NULL,
    create_date           timestamp       NOT NULL,
    rec_status            varchar(1)      DEFAULT 'C') NOT NULL,
    status_change_date    timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- INDEX: rpt_file_store_ix_1 
--

CREATE INDEX rpt_file_store_ix_1 ON rpt_file_store(admin_guid, file_type, file_name, rec_status)
;
-- 
-- TABLE: rpt_file_store 
--

ALTER TABLE rpt_file_store ADD 
    CONSTRAINT rpt_file_store_pkey PRIMARY KEY (rec_id)
;

