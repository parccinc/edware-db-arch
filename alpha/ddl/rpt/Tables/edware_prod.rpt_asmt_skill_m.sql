-- 
-- TABLE: rpt_asmt_skill_m 
--

CREATE TABLE rpt_asmt_skill_m(
    rec_id                   char(10)       NOT NULL,
    batch_guid               varchar(50)    NOT NULL,
    asmt_guid                varchar(50)    NOT NULL,
    skill_guid               varchar(36)    NOT NULL,
    skill_name               varchar(40),
    max_number_per_skill     int2,
    total_number_of_items    int4,
    create_date              timestamp      NOT NULL,
    rec_status               varchar(1)     DEFAULT 'C') NOT NULL,
    status_change_date       timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_asmt_skill_m 
--

ALTER TABLE rpt_asmt_skill_m ADD 
    CONSTRAINT rpt_asmt_skill_m_pkey PRIMARY KEY (rec_id)
;

