-- 
-- TABLE: ela_sum_rpt 
--

CREATE TABLE ela_sum_rpt(
    rec_id                      int8             NOT NULL,
    test_code                   varchar(20)      NOT NULL,
    test_key                    int2             NOT NULL,
    state_code                  char(2)          NOT NULL,
    state_name                  varchar(50),
    state_key                   int4,
    dist_key                    int4             NOT NULL,
    dist_id                     varchar(15),
    dist_name                   varchar(60),
    school_key                  int4,
    school_id                   varchar(15),
    school_name                 varchar(60),
    student_key                 int4,
    student_parcc_id            varchar(40)      NOT NULL,
    student_state_id            varchar(40),
    student_local_id            varchar(40),
    student_name                varchar(110)     NOT NULL,
    is_male_key                 int2             DEFAULT 0 NOT NULL,
    is_female_key               int2             DEFAULT 0 NOT NULL,
    is_other_sex_key            int2             DEFAULT 0 NOT NULL,
    student_grade               varchar(12)      NOT NULL,
    student_grade_key           int2             NOT NULL,
    is_hisp_latino_key          int2             DEFAULT -1 NOT NULL,
    is_indian_alaska_key        int2             DEFAULT -1 NOT NULL,
    is_asian_key                int2             DEFAULT -1 NOT NULL,
    is_black_key                int2             DEFAULT -1 NOT NULL,
    is_hawai_key                int2             DEFAULT -1 NOT NULL,
    is_white_key                int2             DEFAULT -1 NOT NULL,
    is_ell_key                  int2             DEFAULT -1 NOT NULL,
    is_lep_key                  int2             DEFAULT -1 NOT NULL,
    is_gift_talent_key          int2             DEFAULT -1 NOT NULL,
    is_econo_disadvant_key      int2             DEFAULT -1 NOT NULL,
    is_aut_disabil_key          int2             DEFAULT -1 NOT NULL,
    is_db_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_dd_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_emn_disabil_key          int2             DEFAULT -1 NOT NULL,
    is_hi_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_id_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_md_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_oi_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_ohi_disabil_key          int2             DEFAULT -1 NOT NULL,
    is_sld_disabil_key          int2             DEFAULT -1 NOT NULL,
    is_sli_disabil_key          int2             DEFAULT -1 NOT NULL,
    is_tbi_disabil_key          int2             DEFAULT -1 NOT NULL,
    is_vi_disabil_key           int2             DEFAULT -1 NOT NULL,
    end_year_key                numeric(4, 0)    NOT NULL,
    poy_key                     int2             NOT NULL,
    subject_key                 int2             NOT NULL,
    scale_score                 int4             NOT NULL,
    perf_lvl                    int2,
    read_scale_score            int4,
    write_scaled_score          int4,
    csem                        int4,
    read_csem                   int4,
    write_csem                  int4,
    subclaim1_category          int2,
    subclaim2_category          int2,
    subclaim3_category          int2,
    subclaim4_category          int2,
    subclaim5_category          int2,
    subclaim6_category          int2,
    state_growth_percent        numeric(5, 2),
    district_growth_percent     numeric(5, 2),
    parcc_growth_percent        numeric(5, 2),
    record_type                 char(2)          NOT NULL,
    is_reported_summative       char(1)          DEFAULT 'N',
    is_roster_reported          varchar(1)       DEFAULT 'N',
    report_supression_code      int2,
    report_supression_action    int2             DEFAULT -1 NOT NULL,
    is_school_agg               int2             DEFAULT 1 NOT NULL,
    is_dist_agg                 int2             DEFAULT 1 NOT NULL,
    is_state_agg                int2             DEFAULT 1 NOT NULL,
    is_parcc_agg                int2             DEFAULT 1 NOT NULL,
    is_roster_rpt               int2             DEFAULT 1 NOT NULL,
    is_roster_score_rpt         int2             DEFAULT 1 NOT NULL,
    is_isr_rpt                  int2             DEFAULT 1 NOT NULL,
    is_isr_score_rpt            int2             DEFAULT 1 NOT NULL,
    batch_guid                  varchar(50)      NOT NULL,
    rec_status                  varchar(1)       DEFAULT 'C' NOT NULL,
    create_date                 date             DEFAULT CURRENT_DATE NOT NULL,
    status_change_date          date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN ela_sum_rpt.state_code IS 'Parcc name:State Abbreviation. The abbreviation for the state in which the SEA address is located. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19974'
;
COMMENT ON COLUMN ela_sum_rpt.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN ela_sum_rpt.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN ela_sum_rpt.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN ela_sum_rpt.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN ela_sum_rpt.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON COLUMN ela_sum_rpt.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.'
;
COMMENT ON COLUMN ela_sum_rpt.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN ela_sum_rpt.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN ela_sum_rpt.is_hisp_latino_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_indian_alaska_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_asian_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_black_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_hawai_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_white_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_ell_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_lep_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_gift_talent_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_econo_disadvant_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_aut_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_db_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_dd_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_emn_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_hi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_id_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_md_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_oi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_ohi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_sld_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_sli_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_tbi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.is_vi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN ela_sum_rpt.scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score
'
;
COMMENT ON COLUMN ela_sum_rpt.perf_lvl IS 'Summative Performance Level'
;
COMMENT ON COLUMN ela_sum_rpt.read_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score
'
;
COMMENT ON COLUMN ela_sum_rpt.write_scaled_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score
'
;
COMMENT ON COLUMN ela_sum_rpt.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN ela_sum_rpt.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN ela_sum_rpt.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN ela_sum_rpt.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN ela_sum_rpt.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN ela_sum_rpt.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN ela_sum_rpt.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse'
;
COMMENT ON COLUMN ela_sum_rpt.district_growth_percent IS 'Student Growth Percentile Compared to District'
;
COMMENT ON COLUMN ela_sum_rpt.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC'
;
COMMENT ON COLUMN ela_sum_rpt.record_type IS 'Identifies the type of record: 01 - Test Registration Only - Student Assigned a Test but not attempted/completed, 02 - Test Attempt - Student completed a test assignment and should be used for reporting for a non summative assessment, 03 - Summative - Record used to create reports and aggregation for a summative assessment'
;
COMMENT ON COLUMN ela_sum_rpt.is_reported_summative IS 'only for record type 01.Reported Summative Score Flag indicating the summative record that will be reported. Summative Score Record field that can be updated in the file and re-imported Reported Summative Score Flag default rules Match on: - PARCC Student ID - Test Code - Period Then: - Test Attemptedness flag has Y for either one or both PBA and EOY components - At least one component level Voided PBA/EOY Score Code test attempts is blank - If multiple component level test attempts that met attemptedness rules and is not voided then create multiple unique Summative Record UUIDs. - If the criteria are not met then the Summative Record UUID field will be blank and the No Summative Record Flag field will be Y/true. Import rule: Match on - PARCC Student ID - Test Code -- Period
If all three match and multiple summative uuids, minimum and maximum of 1 record must be Y, else error. If exported defaulted Reported Summative Score Flag is changed to another summative record, do not revert back to Reported Summative Score Flag default rules Note: This may be impossible as the import process does not search for multiple records - it updates one record at a time. Decision: Default to highest score, in the event multiple summative records'
;
COMMENT ON COLUMN ela_sum_rpt.is_roster_reported IS 'Only for ''02 and ''03 records. Default to No. Flag for including students in Rosters'
;
COMMENT ON COLUMN ela_sum_rpt.is_school_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_dist_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_state_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_parcc_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_roster_rpt IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_roster_score_rpt IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_isr_rpt IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.is_isr_score_rpt IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN ela_sum_rpt.rec_status IS 'C - currenrt, I - inactive'
;

-- 
-- TABLE: ela_sum_rpt 
--

ALTER TABLE ela_sum_rpt ADD 
    CONSTRAINT ela_sum_rpt_pkey PRIMARY KEY (rec_id)
;

