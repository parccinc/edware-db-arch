-- 
-- TABLE: rpt_item_math_flu 
--

CREATE TABLE rpt_item_math_flu(
    rec_id                char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    rsp_entered           int2,
    student_item_score    numeric(3, 0),
    item_seq              int2,
    item_time             numeric(4, 0),
    batch_guid            varchar(50)      NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL,
    status_change_date    timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_item_math_flu 
--

ALTER TABLE rpt_item_math_flu ADD 
    CONSTRAINT rpt_item_math_flu_pkey PRIMARY KEY (rec_id)
;

