-- 
-- TABLE: rpt_sum_test_score_m 
--

CREATE TABLE rpt_sum_test_score_m(
    rec_id                char(10)       NOT NULL,
    test_code             varchar(20)    NOT NULL,
    sum_period            varchar(20)    NOT NULL,
    batch_guid            varchar(50)    NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C') NOT NULL,
    create_date           timestamp      NOT NULL,
    status_change_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: rpt_sum_test_score_m 
--

ALTER TABLE rpt_sum_test_score_m ADD 
    CONSTRAINT rpt_sum_test_score_m_pkey PRIMARY KEY (rec_id)
;

