-- 
-- TABLE: rpt_admin 
--

CREATE TABLE rpt_admin(
    rec_id                char(10)       NOT NULL,
    admin_guid            varchar(50)    NOT NULL,
    file_type             varchar(50),
    admin_name            varchar(60),
    admin_code            varchar(50),
    admin_type            varchar(32),
    admin_period          varchar(32),
    date_taken_year       varchar(9)     NOT NULL,
    batch_guid            varchar(50)    NOT NULL,
    create_date           timestamp      NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C') NOT NULL,
    status_change_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- INDEX: rpt_admin_ix 
--

CREATE INDEX rpt_admin_ix ON rpt_admin(admin_guid, rec_status)
;
-- 
-- TABLE: rpt_admin 
--

ALTER TABLE rpt_admin ADD 
    CONSTRAINT rpt_admin_pkey PRIMARY KEY (rec_id)
;

