-- 
-- TABLE: rpt_form_group_item 
--

CREATE TABLE rpt_form_group_item(
    asmt_guid             varchar(50)    NOT NULL,
    form_guid             varchar(50)    NOT NULL,
    group_guid            varchar(50)    NOT NULL,
    item_guid             varchar(50)    NOT NULL,
    batch_guid            varchar(50)    NOT NULL,
    rec_id                char(10)       NOT NULL,
    create_date           timestamp      NOT NULL,
    rec_status            varchar(1)     DEFAULT 'C') NOT NULL,
    status_change_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- INDEX: rpt_form_group_item_ix_1 
--

CREATE INDEX rpt_form_group_item_ix_1 ON rpt_form_group_item(form_guid, group_guid, item_guid, rec_status)
;
-- 
-- TABLE: rpt_form_group_item 
--

ALTER TABLE rpt_form_group_item ADD 
    CONSTRAINT rpt_form_group_item_pkey PRIMARY KEY (rec_id)
;

