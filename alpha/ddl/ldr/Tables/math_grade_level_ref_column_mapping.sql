-- 
-- TABLE: math_grade_level_ref_column_mapping 
--

CREATE TABLE math_grade_level_ref_column_mapping(
    column_map_key              char(10)        NOT NULL,
    phase                       int2,
    source_table                varchar(50)     NOT NULL,
    source_column               varchar(256),
    target_table                varchar(50),
    target_column               varchar(50),
    transformation_rule         varchar(50),
    stored_proc_name            varchar(256),
    stored_proc_created_date    timestamp,
    create_date                 timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: math_grade_level_ref_column_mapping 
--

ALTER TABLE math_grade_level_ref_column_mapping ADD 
    CONSTRAINT math_grade_level_ref_column_mapping_pkey PRIMARY KEY (column_map_key)
;

