-- 
-- TABLE: ldr_item_p_data 
--

CREATE TABLE ldr_item_p_data(
    batch_guid                varchar(50)     NOT NULL,
    record_num                char(10)        NOT NULL,
    p_value                   varchar(10),
    p_distractor1             varchar(10),
    p_distractor2             varchar(10),
    p_distractor3             varchar(10),
    p_distractor4             varchar(10),
    p_distractor5             varchar(10),
    p_distractor6             varchar(10),
    p_distractor7             varchar(10),
    p_distractor8             varchar(10),
    p_distractor9             varchar(10),
    p_distractor10            varchar(10),
    p_score0                  varchar(10),
    p_score1                  varchar(10),
    p_score2                  varchar(10),
    p_score3                  varchar(10),
    p_score4                  varchar(10),
    p_score5                  varchar(10),
    p_score6                  varchar(10),
    p_score7                  varchar(10),
    p_score8                  varchar(10),
    p_score9                  varchar(10),
    p_score10                 varchar(10),
    point_biserial_correct    varchar(12),
    point_biserial_0          varchar(12),
    point_biserial_1          varchar(12),
    point_biserial_2          varchar(12),
    point_biserial_3          varchar(12),
    point_biserial_4          varchar(12),
    point_biserial_5          varchar(12),
    point_biserial_6          varchar(12),
    point_biserial_7          varchar(12),
    point_biserial_8          varchar(12),
    point_biserial_9          varchar(12),
    point_biserial_10         varchar(12),
    dif_any                   varchar(10),
    dif_female                varchar(5),
    dif_aa                    varchar(5),
    dif_hispanic              varchar(5),
    dif_asian                 varchar(5),
    dif_na                    varchar(5),
    dif_swd                   varchar(5),
    dif_ell                   varchar(5),
    dif_ses                   varchar(5),
    diff_swd_acc              varchar(5),
    diff_swd_non              varchar(5),
    diff_ell_acc              varchar(5),
    diff_ell_non              varchar(5),
    p_omit                    varchar(10),
    a_parameter               varchar(12),
    b_parameter               varchar(12),
    c_parameter               varchar(12),
    step_1                    varchar(10),
    step_2                    varchar(10),
    step_3                    varchar(10),
    step_4                    varchar(10),
    step_5                    varchar(10),
    step_6                    varchar(10),
    step_7                    varchar(10),
    step_8                    varchar(10),
    step_9                    varchar(10),
    step_10                   varchar(10),
    n_size                    varchar(10),
    item_guid                 varchar(50),
    item_group_guid           varchar(50),
    paired_item_group_guid    varchar(50),
    item_key_response         varchar(250),
    item_admin_type           varchar(50),
    asmt_subject              varchar(15),
    item_grade_course         varchar(20),
    item_delivery_mode        varchar(50),
    test_item_type            varchar(50),
    date_taken_month          varchar(10),
    date_taken_year           varchar(10),
    is_calc_allowed           varchar(10),
    standard                  varchar(250),
    evidence_statement        varchar(250),
    create_date               timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: ldr_item_p_data 
--

ALTER TABLE ldr_item_p_data ADD 
    CONSTRAINT ldr_item_p_data_pkey PRIMARY KEY (batch_guid, record_num)
;

