-- 
-- TABLE: ldr_item_ela_read_comp 
--

CREATE TABLE ldr_item_ela_read_comp(
    batch_guid            varchar(50)     NOT NULL,
    record_num            char(10)        NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(50),
    student_item_score    varchar(10),
    select_key            varchar(255),
    item_seq              varchar(10),
    create_date           timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: ldr_item_ela_read_comp 
--

ALTER TABLE ldr_item_ela_read_comp ADD 
    CONSTRAINT ldr_item_ela_read_comp_pkey PRIMARY KEY (batch_guid, record_num)
;

