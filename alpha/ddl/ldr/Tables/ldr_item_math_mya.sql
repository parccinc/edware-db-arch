-- 
-- TABLE: ldr_item_math_mya 
--

CREATE TABLE ldr_item_math_mya(
    batch_guid            varchar(50)    NOT NULL,
    record_num            char(10)       NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(50),
    student_item_score    varchar(10),
    create_date           timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: ldr_item_math_mya 
--

ALTER TABLE ldr_item_math_mya ADD 
    CONSTRAINT ldr_item_math_mya_pkey PRIMARY KEY (batch_guid, record_num)
;

