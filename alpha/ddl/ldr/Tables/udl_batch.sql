-- 
-- TABLE: udl_batch 
--

CREATE TABLE udl_batch(
    batch_sid                       char(10)        NOT NULL,
    guid_batch                      varchar(256)    NOT NULL,
    tenant                          varchar(256)    DEFAULT '') NOT NULL,
    input_file                      text            DEFAULT '') NOT NULL,
    load_type                       varchar(50),
    working_schema                  varchar(50),
    udl_phase                       varchar(256),
    udl_phase_step                  varchar(50),
    udl_phase_step_status           varchar(50),
    error_desc                      text,
    stack_trace                     text,
    udl_leaf                        boolean,
    size_records                    int8,
    size_units                      int8,
    start_timestamp                 timestamp       DEFAULT now()),
    end_timestamp                   timestamp       DEFAULT now()),
    duration                        interval,
    time_for_one_million_records    time,
    records_per_hour                int8,
    task_id                         varchar(256),
    task_status_url                 varchar(256),
    user_sid                        int8,
    user_email                      varchar(256),
    create_date                     timestamp       DEFAULT now()),
    mod_date                        timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: udl_batch 
--

ALTER TABLE udl_batch ADD 
    CONSTRAINT udl_batch_pkey PRIMARY KEY (batch_sid)
;

