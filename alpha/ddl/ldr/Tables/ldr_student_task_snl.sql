-- 
-- TABLE: ldr_student_task_snl 
--

CREATE TABLE ldr_student_task_snl(
    batch_guid             varchar(50)     NOT NULL,
    record_num             char(10)        NOT NULL,
    asmt_attempt_guid      varchar(36),
    student_parcc_id       varchar(50),
    task_guid              varchar(50),
    task_name              varchar(60),
    total_task_score       varchar(10),
    observation            varchar(250),
    dim_score1             varchar(30),
    dim_score2             varchar(30),
    dim_score3             varchar(30),
    dim_score4             varchar(30),
    dim_score5             varchar(30),
    dr_evidence_observ1    varchar(10),
    dr_evidence_observ2    varchar(10),
    dr_evidence_observ3    varchar(10),
    dr_evidence_observ4    varchar(10),
    dr_evidence_observ5    varchar(10),
    create_date            timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: ldr_student_task_snl 
--

ALTER TABLE ldr_student_task_snl ADD 
    CONSTRAINT ldr_student_task_snl_pkey PRIMARY KEY (batch_guid, record_num)
;

