-- 
-- TABLE: err_list 
--

CREATE TABLE err_list(
    record_sid         char(10)        NOT NULL,
    guid_batch         varchar(256)    NOT NULL,
    err_code           int8,
    err_source         int8,
    err_code_text      text,
    err_source_text    text,
    create_date        timestamp       DEFAULT now()) NOT NULL,
    err_input          text            DEFAULT '') NOT NULL
)
;




-- 
-- TABLE: err_list 
--

ALTER TABLE err_list ADD 
    CONSTRAINT err_list_pkey PRIMARY KEY (record_sid, guid_batch)
;

