-- 
-- TABLE: ldr_math_sum_item_score 
--

CREATE TABLE ldr_math_sum_item_score(
    batch_guid           varchar(50)    NOT NULL,
    record_num           char(10)       NOT NULL,
    date_taken_year      varchar(14),
    state_code           varchar(5),
    resp_dist_id         varchar(20),
    resp_dist_name       varchar(65),
    resp_school_id       varchar(20),
    resp_school_name     varchar(65),
    test_dist_id         varchar(20),
    test_dist_name       varchar(65),
    test_school_id       varchar(20),
    test_school_name     varchar(65),
    admin_code           varchar(20),
    test_code            varchar(25),
    asmt_grade           varchar(10),
    asmt_subject         varchar(40),
    form_id              varchar(20),
    form_format          varchar(5),
    student_parcc_id     varchar(45),
    student_local_id     varchar(35),
    student_grade        varchar(16),
    test_uuid            varchar(40),
    item_uin             varchar(40),
    item_max_score       varchar(10),
    parent_item_score    varchar(10),
    create_date          timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: ldr_math_sum_item_score 
--

ALTER TABLE ldr_math_sum_item_score ADD 
    CONSTRAINT ldr_math_sum_item_score_pkey PRIMARY KEY (batch_guid, record_num)
;

