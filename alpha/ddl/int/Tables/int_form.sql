-- 
-- TABLE: int_form 
--

CREATE TABLE int_form(
    rec_id                           char(10)        NOT NULL,
    batch_guid                       varchar(50)     NOT NULL,
    form_guid                        varchar(50)     NOT NULL,
    asmt_guid                        varchar(50)     NOT NULL,
    form_format                      varchar(50),
    form_name                        varchar(100),
    form_code                        varchar(100),
    form_ref_id                      varchar(60),
    form_version                     varchar(30),
    asmt_score_max                   int2,
    asmt_perf_lvl1_guid              varchar(50),
    asmt_perf_lvl1_cutpoint_label    varchar(30),
    asmt_perf_lvl1_cutpoint_low      int2,
    asmt_perf_lvl1_cutpoint_upp      int2,
    asmt_perf_lvl2_guid              varchar(50),
    asmt_perf_lvl2_cutpoint_label    varchar(30),
    asmt_perf_lvl2_cutpoint_low      int2,
    asmt_perf_lvl2_cutpoint_upp      int2,
    asmt_perf_lvl3_guid              varchar(50),
    asmt_perf_lvl3_cutpoint_label    varchar(30),
    asmt_perf_lvl3_cutpoint_low      int2,
    asmt_perf_lvl3_cutpoint_upp      int2,
    asmt_perf_lvl4_guid              varchar(50),
    asmt_perf_lvl4_cutpoint_label    varchar(30),
    asmt_perf_lvl4_cutpoint_low      int2,
    asmt_perf_lvl4_cutpoint_upp      int2,
    asmt_perf_lvl5_guid              varchar(50),
    asmt_perf_lvl5_cutpoint_label    varchar(30),
    asmt_perf_lvl5_cutpoint_low      int2,
    asmt_perf_lvl5_cutpoint_upp      int2,
    create_date                      timestamp       NOT NULL,
    rec_status                       varchar(1)      DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_form 
--

ALTER TABLE int_form ADD 
    CONSTRAINT int_form_pkey PRIMARY KEY (rec_id, batch_guid)
;

