-- 
-- TABLE: int_dgnst_math_item_m 
--

CREATE TABLE int_dgnst_math_item_m(
    rec_id                 char(10)        NOT NULL,
    item_guid              varchar(50)     NOT NULL,
    asmt_guid              varchar(50)     NOT NULL,
    subclaim_name          varchar(30),
    item_name              varchar(150),
    item_descript          varchar(250),
    test_item_type         varchar(60),
    passage_type           varchar(40),
    text_complexity        varchar(40),
    content_standard       varchar(40),
    evidence_stmt1         varchar(40),
    evidence_stmt2         varchar(40),
    evidence_stmt3         varchar(40),
    word_count             int2,
    math_correct_answer    varchar(255),
    item_strand            varchar(40),
    test_skill1            varchar(1),
    test_skill2            varchar(1),
    test_skill3            varchar(1),
    test_skill4            varchar(1),
    test_skill5            varchar(1),
    batch_guid             varchar(50)     NOT NULL,
    rec_status             varchar(1)      DEFAULT 'C') NOT NULL,
    create_date            timestamp       NOT NULL
)
;




-- 
-- TABLE: int_dgnst_math_item_m 
--

ALTER TABLE int_dgnst_math_item_m ADD 
    CONSTRAINT int_dgnst_math_item_m_pkey PRIMARY KEY (rec_id, batch_guid)
;

