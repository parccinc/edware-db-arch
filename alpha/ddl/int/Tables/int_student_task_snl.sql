-- 
-- TABLE: int_student_task_snl 
--

CREATE TABLE int_student_task_snl(
    rec_id                 char(10)         NOT NULL,
    batch_guid             varchar(50)      NOT NULL,
    asmt_attempt_guid      varchar(36)      NOT NULL,
    student_parcc_id       varchar(50),
    task_guid              varchar(50),
    task_name              varchar(60),
    total_task_score       numeric(4, 0),
    observation            varchar(250),
    dim_score1             numeric(4, 0),
    dim_score2             numeric(4, 0),
    dim_score3             numeric(4, 0),
    dim_score4             numeric(4, 0),
    dim_score5             numeric(4, 0),
    dr_evidence_observ1    char(1),
    dr_evidence_observ2    char(1),
    dr_evidence_observ3    char(1),
    dr_evidence_observ4    char(1),
    dr_evidence_observ5    char(1),
    rec_status             char(1)          DEFAULT 'C') NOT NULL,
    create_date            timestamp        NOT NULL
)
;




-- 
-- TABLE: int_student_task_snl 
--

ALTER TABLE int_student_task_snl ADD 
    CONSTRAINT int_student_task_snl_pkey PRIMARY KEY (rec_id, batch_guid)
;

