-- 
-- TABLE: int_snl_asmt_mode_m 
--

CREATE TABLE int_snl_asmt_mode_m(
    rec_id         char(10)       NOT NULL,
    batch_guid     varchar(50)    NOT NULL,
    asmt_guid      varchar(50)    NOT NULL,
    asmt_mode      varchar(5)     NOT NULL,
    rec_status     char(1)        DEFAULT 'C') NOT NULL,
    create_date    timestamp      NOT NULL
)
;




-- 
-- TABLE: int_snl_asmt_mode_m 
--

ALTER TABLE int_snl_asmt_mode_m ADD 
    CONSTRAINT int_snl_asmt_mode_m_pkey PRIMARY KEY (rec_id, asmt_guid, asmt_mode)
;

