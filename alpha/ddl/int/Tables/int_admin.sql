-- 
-- TABLE: int_admin 
--

CREATE TABLE int_admin(
    rec_id             char(10)       NOT NULL,
    batch_guid         varchar(50)    NOT NULL,
    admin_guid         varchar(50)    NOT NULL,
    file_type          varchar(50),
    admin_name         varchar(60),
    admin_code         varchar(50),
    admin_type         varchar(32),
    admin_period       varchar(32),
    date_taken_year    varchar(9)     NOT NULL,
    create_date        timestamp      NOT NULL,
    rec_status         varchar(1)     DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_admin 
--

ALTER TABLE int_admin ADD 
    CONSTRAINT int_admin_pkey PRIMARY KEY (rec_id, batch_guid)
;

