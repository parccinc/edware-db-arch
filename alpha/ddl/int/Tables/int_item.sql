-- 
-- TABLE: int_item 
--

CREATE TABLE int_item(
    rec_id              char(10)        NOT NULL,
    batch_guid          varchar(50)     NOT NULL,
    item_guid           varchar(50)     NOT NULL,
    item_name           varchar(150)    NOT NULL,
    item_descript       varchar(250),
    item_ref_id         varchar(60)     NOT NULL,
    item_max_points     varchar(30),
    item_version        varchar(30),
    test_item_type      varchar(60),
    is_item_omitted     varchar(30),
    item_status         varchar(30),
    parent_item_guid    varchar(50)     NOT NULL,
    create_date         timestamp       NOT NULL,
    rec_status          varchar(1)      DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_item 
--

ALTER TABLE int_item ADD 
    CONSTRAINT int_item_pkey PRIMARY KEY (rec_id, batch_guid)
;

