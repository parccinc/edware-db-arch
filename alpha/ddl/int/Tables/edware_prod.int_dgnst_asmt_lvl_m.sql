-- 
-- TABLE: int_dgnst_asmt_lvl_m 
--

CREATE TABLE int_dgnst_asmt_lvl_m(
    rec_id             char(10)       NOT NULL,
    asmt_guid          varchar(50)    NOT NULL,
    asmt_lvl_design    char(2)        NOT NULL,
    batch_guid         varchar(50)    NOT NULL,
    create_date        timestamp      NOT NULL,
    rec_status         varchar(1)     DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_dgnst_asmt_lvl_m 
--

ALTER TABLE int_dgnst_asmt_lvl_m ADD 
    CONSTRAINT int_dgnst_asmt_lvl_m_pkey PRIMARY KEY (rec_id, batch_guid)
;

