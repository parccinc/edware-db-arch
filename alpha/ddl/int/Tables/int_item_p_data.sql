-- 
-- TABLE: int_item_p_data 
--

CREATE TABLE int_item_p_data(
    batch_guid                varchar(50)      NOT NULL,
    record_num                int4             NOT NULL,
    p_value                   numeric(5, 2),
    p_distractor1             numeric(5, 2),
    p_distractor2             numeric(5, 2),
    p_distractor3             numeric(5, 2),
    p_distractor4             numeric(5, 2),
    p_distractor5             numeric(5, 2),
    p_distractor6             numeric(5, 2),
    p_distractor7             numeric(5, 2),
    p_distractor8             numeric(5, 2),
    p_distractor9             numeric(5, 2),
    p_distractor10            numeric(5, 2),
    p_score0                  numeric(5, 2),
    p_score1                  numeric(5, 2),
    p_score2                  numeric(5, 2),
    p_score3                  numeric(5, 2),
    p_score4                  numeric(5, 2),
    p_score5                  numeric(5, 2),
    p_score6                  numeric(5, 2),
    p_score7                  numeric(5, 2),
    p_score8                  numeric(5, 2),
    p_score9                  numeric(5, 2),
    p_score10                 numeric(5, 2),
    point_biserial_correct    numeric(7, 4),
    point_biserial_0          numeric(7, 4),
    point_biserial_1          numeric(7, 4),
    point_biserial_2          numeric(7, 4),
    point_biserial_3          numeric(7, 4),
    point_biserial_4          numeric(7, 4),
    point_biserial_5          numeric(7, 4),
    point_biserial_6          numeric(7, 4),
    point_biserial_7          numeric(7, 4),
    point_biserial_8          numeric(7, 4),
    point_biserial_9          numeric(7, 4),
    point_biserial_10         numeric(7, 4),
    dif_any                   boolean,
    dif_female                char(1),
    dif_aa                    char(1),
    dif_hispanic              char(1),
    dif_asian                 char(1),
    dif_na                    char(1),
    dif_swd                   char(1),
    dif_ell                   char(1),
    dif_ses                   char(1),
    diff_swd_acc              char(1),
    diff_swd_non              char(1),
    diff_ell_acc              char(1),
    diff_ell_non              char(1),
    p_omit                    numeric(5, 2),
    a_parameter               numeric(7, 4),
    b_parameter               numeric(7, 4),
    c_parameter               numeric(7, 4),
    step_1                    numeric(5, 2),
    step_2                    numeric(5, 2),
    step_3                    numeric(5, 2),
    step_4                    numeric(5, 2),
    step_5                    numeric(5, 2),
    step_6                    numeric(5, 2),
    step_7                    numeric(5, 2),
    step_8                    numeric(5, 2),
    step_9                    numeric(5, 2),
    step_10                   numeric(5, 2),
    n_size                    int4,
    item_guid                 varchar(50),
    group_guid                varchar(50),
    paired_group_guid         varchar(50),
    item_key_response         varchar(250),
    item_admin_type           varchar(50),
    asmt_subject              varchar(35),
    item_grade_course         varchar(20),
    item_delivery_mode        varchar(50),
    test_item_type            varchar(50),
    date_taken_month          varchar(10),
    date_taken_year           numeric(4, 0),
    is_calc_allowed           boolean,
    rec_status                varchar(1)       DEFAULT 'C' NOT NULL,
    standard                  varchar(250),
    evidence_statement        varchar(250),
    create_date               date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN int_item_p_data.p_value IS 'percent examinees responding correctly/average item score'
;
COMMENT ON COLUMN int_item_p_data.p_distractor1 IS 'percent examinees selecting distractor 1'
;
COMMENT ON COLUMN int_item_p_data.p_distractor2 IS 'percent examinees selecting distractor 2'
;
COMMENT ON COLUMN int_item_p_data.p_distractor3 IS 'percent examinees selecting distractor 3'
;
COMMENT ON COLUMN int_item_p_data.p_distractor4 IS 'percent examinees selecting distractor 4'
;
COMMENT ON COLUMN int_item_p_data.p_distractor5 IS 'percent examinees selecting distractor 5'
;
COMMENT ON COLUMN int_item_p_data.p_distractor6 IS 'percent examinees selecting distractor 6'
;
COMMENT ON COLUMN int_item_p_data.p_distractor7 IS 'percent examinees selecting distractor 7'
;
COMMENT ON COLUMN int_item_p_data.p_distractor8 IS 'percent examinees selecting distractor 8'
;
COMMENT ON COLUMN int_item_p_data.p_distractor9 IS 'percent examinees selecting distractor 9'
;
COMMENT ON COLUMN int_item_p_data.p_distractor10 IS 'percent examinees selecting distractor 10'
;
COMMENT ON COLUMN int_item_p_data.p_score0 IS 'percent examinees receiving a score of 0
'
;
COMMENT ON COLUMN int_item_p_data.p_score1 IS 'percent examinees receiving a score of 1'
;
COMMENT ON COLUMN int_item_p_data.p_score2 IS 'percent examinees receiving a score of 2'
;
COMMENT ON COLUMN int_item_p_data.p_score3 IS 'percent examinees receiving a score of 3'
;
COMMENT ON COLUMN int_item_p_data.p_score4 IS 'percent examinees receiving a score of 4'
;
COMMENT ON COLUMN int_item_p_data.p_score5 IS 'percent examinees receiving a score of 5'
;
COMMENT ON COLUMN int_item_p_data.p_score6 IS 'percent examinees receiving a score of 6'
;
COMMENT ON COLUMN int_item_p_data.p_score7 IS 'percent examinees receiving a score of 7'
;
COMMENT ON COLUMN int_item_p_data.p_score8 IS 'percent examinees receiving a score of 8'
;
COMMENT ON COLUMN int_item_p_data.p_score9 IS 'percent examinees receiving a score of 9'
;
COMMENT ON COLUMN int_item_p_data.p_score10 IS 'percent examinees receiving a score of 10'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_correct IS 'point-biserial correlation for correct response'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_0 IS 'point-biserial correlation for item score of 0'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_1 IS 'point-biserial correlation for item score of 1'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_2 IS 'point-biserial correlation for item score of 2'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_3 IS 'point-biserial correlation for item score of 3'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_4 IS 'point-biserial correlation for item score of 4'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_5 IS 'point-biserial correlation for item score of 5'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_6 IS 'point-biserial correlation for item score of 6'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_7 IS 'point-biserial correlation for item score of 7'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_8 IS 'point-biserial correlation for item score of 8'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_9 IS 'point-biserial correlation for item score of 9'
;
COMMENT ON COLUMN int_item_p_data.point_biserial_10 IS 'point-biserial correlation for item score of 10'
;
COMMENT ON COLUMN int_item_p_data.dif_any IS 'whether differential item functioning was identified for one or more groups'
;
COMMENT ON COLUMN int_item_p_data.dif_female IS 'whether differential item functions was identified for Females and if so whether it was positive (C+) or negative (C-) or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.dif_aa IS 'whether differential item functions was identified for African Americans and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.dif_hispanic IS 'whether differential item functions was identified for Hispanics and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.dif_asian IS 'whether differential item functions was identified for Asians and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.dif_na IS 'whether differential item functions was identified for Native Americans and if so whether it was positive (C+) or negative (C-) or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.dif_swd IS 'whether differential item functions was identified for students with disabilities and if so whether it waspositive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.dif_ell IS 'whether differential item functions was identified for English language learners and if so whether it was positive (C+) or negative (C-) '
;
COMMENT ON COLUMN int_item_p_data.dif_ses IS 'whether differential item functions was identified for low social economic status and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.diff_swd_acc IS 'whether differential item functions was identified forstudents with disability who received an accommodation and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.diff_swd_non IS 'whether differential item functions was identified for students with disability who did not received an accommodation and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.diff_ell_acc IS 'whether differential item functions was identified for english language learners who received an accommodation and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.diff_ell_non IS 'whether differential item functions was identified for english language learners who did not received an accommodation and if so whether it was positive (C+) or negative (C-)  or Non-uniform (N)'
;
COMMENT ON COLUMN int_item_p_data.p_omit IS 'percent examinees omiting the item'
;
COMMENT ON COLUMN int_item_p_data.a_parameter IS 'irt a parameter value'
;
COMMENT ON COLUMN int_item_p_data.b_parameter IS 'irt b parameter value'
;
COMMENT ON COLUMN int_item_p_data.c_parameter IS 'irt c parameter value'
;
COMMENT ON COLUMN int_item_p_data.n_size IS 'sample size upon which psychometric metadata statistics are based'
;
COMMENT ON COLUMN int_item_p_data.group_guid IS 'identification of the passage set to which the item belongs'
;
COMMENT ON COLUMN int_item_p_data.paired_group_guid IS 'identification of the paired passage set to which the item belongs'
;
COMMENT ON COLUMN int_item_p_data.item_key_response IS 'correct response for selected response item'
;
COMMENT ON COLUMN int_item_p_data.item_admin_type IS 'type of test administration (e;g; field test, operational year 1, summer make up, etc.)'
;
COMMENT ON COLUMN int_item_p_data.asmt_subject IS 'subject area tested by item'
;
COMMENT ON COLUMN int_item_p_data.item_grade_course IS 'grade level or course the item is designed to measure'
;
COMMENT ON COLUMN int_item_p_data.item_delivery_mode IS 'mode of delivery upon which the metadata statistics are based'
;
COMMENT ON COLUMN int_item_p_data.test_item_type IS 'item type - need to create categories (selected response, constructed, TEI, etc.)'
;
COMMENT ON COLUMN int_item_p_data.date_taken_year IS 'year in which item was administered and data used to calculate metadata statsistics'
;
COMMENT ON COLUMN int_item_p_data.is_calc_allowed IS 'whether a calculator could be used for the item. Expected val: Y,N'
;
COMMENT ON COLUMN int_item_p_data.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE int_item_p_data IS 'Psychometric data. Source CSV file.'
;

-- 
-- TABLE: int_item_p_data 
--

ALTER TABLE int_item_p_data ADD 
    CONSTRAINT int_item_p_data_pkey PRIMARY KEY (batch_guid, record_num)
;

