-- 
-- TABLE: int_asmt 
--

CREATE TABLE int_asmt(
    rec_id          char(10)       NOT NULL,
    batch_guid      varchar(50)    NOT NULL,
    asmt_guid       varchar(50)    NOT NULL,
    admin_guid      varchar(50)    NOT NULL,
    asmt_title      varchar(60),
    asmt_subject    varchar(15),
    create_date     timestamp      NOT NULL,
    rec_status      varchar(1)     DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_asmt 
--

ALTER TABLE int_asmt ADD 
    CONSTRAINT int_asmt_pkey PRIMARY KEY (rec_id, batch_guid)
;

