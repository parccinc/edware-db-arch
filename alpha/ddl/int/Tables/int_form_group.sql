-- 
-- TABLE: int_form_group 
--

CREATE TABLE int_form_group(
    rec_id               char(10)        NOT NULL,
    batch_guid           varchar(50)     NOT NULL,
    asmt_guid            varchar(50)     NOT NULL,
    form_guid            varchar(50)     NOT NULL,
    group_guid           varchar(50)     NOT NULL,
    group_code           varchar(50),
    group_name           varchar(60),
    group_desc           varchar(256),
    group_type           varchar(30),
    parent_group_guid    varchar(50),
    group_ref_id         varchar(60),
    create_date          timestamp       NOT NULL,
    rec_status           varchar(1)      DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_form_group 
--

ALTER TABLE int_form_group ADD 
    CONSTRAINT int_form_group_pkey PRIMARY KEY (rec_id, batch_guid)
;

