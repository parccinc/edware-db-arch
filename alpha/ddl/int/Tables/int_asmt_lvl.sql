-- 
-- TABLE: int_asmt_lvl 
--

CREATE TABLE int_asmt_lvl(
    rec_id             char(10)       NOT NULL,
    batch_guid         varchar(50)    NOT NULL,
    asmt_guid          varchar(50)    NOT NULL,
    asmt_lvl_design    char(2)        NOT NULL,
    create_date        timestamp      NOT NULL,
    rec_status         varchar(1)     DEFAULT 'C') NOT NULL
)
;




-- 
-- TABLE: int_asmt_lvl 
--

ALTER TABLE int_asmt_lvl ADD 
    CONSTRAINT int_asmt_lvl_pkey PRIMARY KEY (rec_id, batch_guid)
;

