-- 
-- TABLE: int_ela_sum 
--

CREATE TABLE int_ela_sum(
    batch_guid                    varchar(50)      NOT NULL,
    record_num                    int4             NOT NULL,
    record_type                   char(2)          NOT NULL,
    is_multi_rec                  char(1)          DEFAULT 'N',
    is_reported_summative         char(1)          DEFAULT 'N',
    is_roster_reported            varchar(1)       DEFAULT 'N' NOT NULL,
    report_suppression_code       char(2),
    report_suppression_action     int2,
    pba_category                  char(1),
    eoy_category                  char(1),
    state_code                    char(2)          NOT NULL,
    resp_dist_id                  varchar(15),
    resp_dist_name                varchar(60),
    resp_school_id                varchar(15),
    resp_school_name              varchar(60),
    pba_test_dist_id              varchar(15),
    pba_test_dist_name            varchar(60),
    pba_test_school_id            varchar(15),
    pba_test_school_name          varchar(60),
    eoy_test_dist_id              varchar(15),
    eoy_test_dist_name            varchar(60),
    eoy_test_school_id            varchar(15),
    eoy_test_school_name          varchar(60),
    student_parcc_id              varchar(40)      NOT NULL,
    student_local_id              varchar(40),
    student_state_id              varchar(40),
    student_first_name            varchar(35)      NOT NULL,
    student_middle_name           varchar(35),
    student_last_name             varchar(35)      NOT NULL,
    student_sex                   char(1),
    student_dob                   date             NOT NULL,
    opt_state_data1               varchar(20),
    student_grade                 varchar(12)      NOT NULL,
    ethn_hisp_latino              char(1),
    ethn_indian_alaska            char(1),
    ethn_asian                    char(1),
    ethn_black                    char(1),
    ethn_hawai                    char(1),
    ethn_white                    char(1),
    ethn_filler                   char(1),
    ethnicity                     char(2),
    ell                           char(1),
    lep_status                    char(1),
    gift_talent                   char(1),
    migrant_status                char(1),
    econo_disadvantage            char(1),
    disabil_student               char(1),
    primary_disabil_type          varchar(3),
    accomod_ell                   char(1),
    accomod_504                   char(1),
    accomod_ind_ed                char(1),
    accomod_freq_breaks           char(1),
    accomod_alt_location          char(1),
    accomod_small_group           char(1),
    accomod_special_equip         char(1),
    accomod_spec_area             char(1),
    accomod_time_day              char(1),
    accomod_answer_mask           char(1),
    accomod_color_contrast        varchar(19),
    accomod_text_2_speech_math    char(1),
    accomod_read_math             varchar(14),
    accomod_asl_video             char(1),
    accomod_screen_reader         char(1),
    accomod_close_capt_ela        char(1),
    accomod_read_ela              varchar(14),
    accomod_braille_ela           char(1),
    accomod_tactile_graph         char(1),
    accomod_text_2_speech_ela     char(1),
    accomod_answer_rec            char(1),
    accomod_braille_resp          varchar(16),
    accomod_calculator            char(1),
    accomod_construct_resp_ela    varchar(20),
    accomod_select_resp_ela       varchar(20),
    accomod_math_resp             varchar(20),
    accomod_monitor_test_resp     char(1),
    accomod_word_predict          char(1),
    accomod_native_lang           char(1),
    accomod_loud_native_lang      varchar(40),
    accomod_math_rsp              varchar(20),
    accomod_math_text_2_speech    char(3),
    accomod_math_trans_online     char(3),
    accomod_w_2_w_dict            char(1),
    accomod_extend_time           varchar(6),
    accomod_alt_paper_test        char(1),
    accomod_paper_trans_math      char(3),
    accomod_human_read_sign       varchar(14),
    accomod_large_print           char(1),
    accomod_braille_tactile       char(1),
    opt_state_data2               varchar(20),
    opt_state_data3               varchar(20),
    opt_state_data4               varchar(20),
    opt_state_data5               varchar(20),
    opt_state_data6               varchar(20),
    opt_state_data7               varchar(20),
    opt_state_data8               varchar(20),
    poy                           varchar(20),
    test_code                     varchar(20)      NOT NULL,
    asmt_subject                  varchar(15),
    pba_form_id                   varchar(14),
    eoy_form_id                   varchar(14),
    pba_test_uuid                 varchar(36),
    eoy_test_uuid                 varchar(36),
    sum_score_rec_uuid            varchar(36),
    pba_total_items               numeric(3, 0),
    pba_attempt_flag              char(1),
    eoy_total_items               numeric(3, 0),
    eoy_attempt_flag              char(1),
    pba_total_items_attempt       numeric(3, 0),
    pba_total_items_unit1         numeric(2, 0),
    pba_unit1_items_attempt       numeric(2, 0),
    pba_total_items_unit2         numeric(2, 0),
    pba_unit2_items_attempt       numeric(2, 0),
    pba_total_items_unit3         numeric(2, 0),
    pba_unit3_items_attempt       numeric(2, 0),
    pba_total_items_unit4         numeric(2, 0),
    pba_unit4_items_attempt       numeric(2, 0),
    pba_total_items_unit5         numeric(2, 0),
    pba_unit5_items_attempt       numeric(2, 0),
    eoy_total_items_attempt       numeric(3, 0),
    eoy_total_items_unit1         numeric(2, 0),
    eoy_unit1_items_attempt       numeric(3, 0),
    eoy_total_items_unit2         numeric(2, 0),
    eoy_unit2_items_attempt       numeric(3, 0),
    eoy_total_items_unit3         numeric(2, 0),
    eoy_unit3_items_attempt       numeric(3, 0),
    eoy_total_items_unit4         numeric(2, 0),
    eoy_unit4_items_attempt       numeric(3, 0),
    eoy_total_items_unit5         numeric(2, 0),
    eoy_unit5_items_attempt       numeric(3, 0),
    pba_not_tested_reason         char(2),
    eoy_not_tested_reason         char(2),
    pba_void_reason               char(2),
    eoy_void_reason               char(2),
    pba_raw_score                 int4,
    eoy_raw_score                 int4,
    sum_scale_score               int2,
    sum_csem                      int4,
    sum_perf_lvl                  int2,
    sum_read_scale_score          int2,
    sum_read_csem                 int4,
    sum_write_scale_score         int2,
    sum_write_csem                int4,
    subclaim1_category            int2,
    subclaim2_category            int2,
    subclaim3_category            int2,
    subclaim4_category            int2,
    subclaim5_category            int2,
    subclaim6_category            int2,
    state_growth_percent          numeric(5, 2),
    district_growth_percent       numeric(5, 2),
    parcc_growth_percent          numeric(5, 2),
    year                          char(9)          NOT NULL,
    asmt_grade                    char(11)         NOT NULL,
    include_in_parcc              boolean          NOT NULL,
    include_in_state              boolean          NOT NULL,
    include_in_district           boolean          NOT NULL,
    include_in_school             boolean          NOT NULL,
    include_in_isr                boolean          NOT NULL,
    include_in_roster             char(1)          NOT NULL,
    rec_status                    varchar(1)       DEFAULT 'C' NOT NULL,
    create_date                   date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN int_ela_sum.record_type IS '01 - Summative Score = based on a valid test attempt in PBA and in EOY. If a student has at least one 01 created there will be no 02 or 03 record created
02 - Single Component = based on a test attempt in either PBA and EOY but not in both components
03 -No Component = based on a combination of test assignments only in both PBA and EOY, ALL test attempts were voided in PBA and EOY, or no attempt or assignment in either PBA or EOY and either an assignment or voided test attempt in PBA or EOY.'
;
COMMENT ON COLUMN int_ela_sum.is_multi_rec IS 'Y, Null, This indicates there is multiple 01, 02 or 03 type records for this Student, Test and Period. Multiple Record Flag'
;
COMMENT ON COLUMN int_ela_sum.is_reported_summative IS 'only for record type 01.Reported Summative Score Flag indicating the summative record that will be reported. Summative Score Record field that can be updated in the file and re-imported Reported Summative Score Flag default rules Match on: - PARCC Student ID - Test Code - Period Then: - Test Attemptedness flag has Y for either one or both PBA and EOY components - At least one component level Voided PBA/EOY Score Code test attempts is blank - If multiple component level test attempts that met attemptedness rules and is not voided then create multiple unique Summative Record UUIDs. - If the criteria are not met then the Summative Record UUID field will be blank and the No Summative Record Flag field will be Y/true. Import rule: Match on - PARCC Student ID - Test Code -- Period; If all three match and multiple summative uuids, minimum and maximum of 1 record must be Y, else error. If exported defaulted Reported Summative Score Flag is changed to another summative record, do not revert back to Reported Summative Score Flag default rules Note: This may be impossible as the import process does not search for multiple records - it updates one record at a time. Decision: Default to highest score, in the event multiple summative records'
;
COMMENT ON COLUMN int_ela_sum.is_roster_reported IS 'Reported Roster Flag. Only for ''02 and ''03 records. Default to No. Flag for including students in Rosters'
;
COMMENT ON COLUMN int_ela_sum.report_suppression_code IS 'Only allowed for Record Type 01 Error and reject record if invalid value. Cross Validation: If Report Suppression Action is non-blank then Report Suppression Code must be non-blank, else error.'
;
COMMENT ON COLUMN int_ela_sum.report_suppression_action IS 'Only allowed for Record Type 01 Error and reject record if invalid value. Cross Validation: If Report Suppression Code is non-blank then Report Suppression Action must be non-blank, else error. Report Suppression options for summative record.'
;
COMMENT ON COLUMN int_ela_sum.pba_category IS 'A = Test Attemptedness flag is blank for both PBA and EOY components test attempts B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field C = Test attempts in either PBA and EOY components are Y for Voided PBA/EOY Score Code field and there are no test assignments or test attempts in the other component D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank and there are no test assignments or test attempts in the other component E = Test Assignments exist in both PBA and EOY components F = Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that did not meet attemptedness rules G = Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field H = Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component'
;
COMMENT ON COLUMN int_ela_sum.eoy_category IS 'A = Test Attemptedness flag is blank for both PBA and EOY components test attempts B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field C = Test attempts in either PBA and EOY components are Y for Voided PBA/EOY Score Code field and there are no test assignments or test attempts in the other component D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank and there are no test assignments or test attempts in the other component E = Test Assignments exist in both PBA and EOY components F = Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that did not meet attemptedness rules G = Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field H = Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component'
;
COMMENT ON COLUMN int_ela_sum.state_code IS 'Parcc name:State Abbreviation. The abbreviation for the state in which the SEA address is located. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19974'
;
COMMENT ON COLUMN int_ela_sum.resp_dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.resp_dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.resp_school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.resp_school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.pba_test_dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.pba_test_dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.pba_test_school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.pba_test_school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.eoy_test_dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.eoy_test_dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.eoy_test_school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.eoy_test_school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_ela_sum.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON COLUMN int_ela_sum.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN int_ela_sum.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.'
;
COMMENT ON COLUMN int_ela_sum.student_sex IS 'Parccc name: Sex. The concept describing the biological traits that distinguish the males and females of a species. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20811'
;
COMMENT ON COLUMN int_ela_sum.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995'
;
COMMENT ON COLUMN int_ela_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN int_ela_sum.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN int_ela_sum.ethn_indian_alaska IS 'Parcc name:American Indian/Alaska Native. A person having origins in any of the original peoples of North and South America (including Central America), and who maintains cultural identification through tribal affiliation or community attachment.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20907'
;
COMMENT ON COLUMN int_ela_sum.ethn_asian IS 'Parcc name:Asian.A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian Subcontinent. This area includes, for example, Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand, and Vietnam.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20908'
;
COMMENT ON COLUMN int_ela_sum.ethn_black IS 'Parcc name: Black or African American	A person having origins in any of the black racial groups of Africa. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20909'
;
COMMENT ON COLUMN int_ela_sum.ethn_hawai IS 'Parcc name: Native Hawaiian or Other Pacific Islander. A person having origins in any of the original peoples of Hawaii, Guam, Samoa, or other Pacific Islands. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20910'
;
COMMENT ON COLUMN int_ela_sum.ethn_white IS 'Parcc name:White. A person having origins in any of the original peoples of Europe, Middle East, or North Africa.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20911'
;
COMMENT ON COLUMN int_ela_sum.ethn_filler IS 'Filler Race Field, Y N'
;
COMMENT ON COLUMN int_ela_sum.ethnicity IS 'Federal Race/Ethnicity.01 = American Indian or Alaska Native 02 = Asian 03 = Black or African American 04 = Hispanic or Latino 05 = White 06 = Native Hawaiian or other Pacific Islander 07 = Two or more races blank = could not resolve. Blank. Hispanic trumps all race fields. Example: if Hispanic and White are Yes only report Hispanic (04) in this field If Ethnicity is not Hispanic and more than one race is Yes, report Two or More Races (07) in this field. If Ethnicity is not Hispanic and only one race is Yes, report the race selected (01, 02, 03, 05 or 06) If Ethnicity and Race was not provided then report blank in this field'
;
COMMENT ON COLUMN int_ela_sum.ell IS 'English Learner. Y,N, Blank.Person with native languages other than English. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.'
;
COMMENT ON COLUMN int_ela_sum.lep_status IS 'Title III Limited English Proficient Participation Status. Y, N, Blank.Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record. An indication that a limited English proficient (LEP) student is served by an English language instruction educational program supported with Title III of ESEA funds.'
;
COMMENT ON COLUMN int_ela_sum.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.'
;
COMMENT ON COLUMN int_ela_sum.migrant_status IS 'Migrant Status. Y,N,Blank. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such. Rule: Test Administration Level Student Data: Use data from EOY. If missing EOY data then use PBA data. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741'
;
COMMENT ON COLUMN int_ela_sum.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569†'
;
COMMENT ON COLUMN int_ela_sum.accomod_ell IS 'Assessment Accommodation:  English learner (EL)'
;
COMMENT ON COLUMN int_ela_sum.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)'
;
COMMENT ON COLUMN int_ela_sum.accomod_freq_breaks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location'
;
COMMENT ON COLUMN int_ela_sum.accomod_small_group IS 'Small Testing Group'
;
COMMENT ON COLUMN int_ela_sum.accomod_special_equip IS 'Specialized Equipment or Furniture'
;
COMMENT ON COLUMN int_ela_sum.accomod_spec_area IS 'Specified Area or Setting'
;
COMMENT ON COLUMN int_ela_sum.accomod_time_day IS 'Time Of Day'
;
COMMENT ON COLUMN int_ela_sum.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.'
;
COMMENT ON COLUMN int_ela_sum.accomod_text_2_speech_math IS 'Text-to-Speech for Mathematics y blank. Personal Needs Profile field. Test content will be read aloud to the student via Text-to-Speech. Embedded within TestNav8. Text-to-Speech is an accessibility feature for Math. Text-to-Speech is an accommodation for ELA/L. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_read_math IS 'Human Reader or Human Signer for Mathematics. HumanSigner HumanReadAloud blank. Personal Needs Profile field. Used to assign the Proctor Testing Tickets by assigning the session as a Read Aloud in PearsonAccess Next. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_close_capt_ela IS 'Closed Captioning for ELA/L.  Y blank. Personal Needs Profile field. Closed captioning and subtitling are both processes of displaying text on a television, video screen, or other visual display to provide additional or interpretive information. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_read_ela IS 'General Administration Directions Read Aloud and Repeated as Needed in Studentís Native Language
(by test administrator).OralScriptReadbyTestAdministratorARA
; OralScriptReadbyTestAdministratorCHI; OralScriptReadbyTestAdministratorHAT; 
OralScriptReadbyTestAdministratorMAH
;OralScriptReadbyTestAdministratorNAV
OralScriptReadbyTestAdministratorPOL
;OralScriptReadbyTestAdministratorPOR
OralScriptReadbyTestAdministratorSOM
;OralScriptReadbyTestAdministratorSPA
OralScriptReadbyTestAdministratorVIE; HumanTranslator; blank'
;
COMMENT ON COLUMN int_ela_sum.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_text_2_speech_ela IS 'Text-to-Speech for ELA/L. Y blank. Personal Needs Profile field. Test content will be read aloud to the student via Text-to-Speech. Embedded within TestNav8. Text-to-Speech is an accessibility feature for Math. Text-to-Speech is an accommodation for ELA/L. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.'
;
COMMENT ON COLUMN int_ela_sum.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types.'
;
COMMENT ON COLUMN int_ela_sum.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_native_lang IS 'General Administration Directions Clarified in Studentís Native Language (by test administrator).Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Studentís Native Language
(by test administrator)'
;
COMMENT ON COLUMN int_ela_sum.accomod_math_rsp IS 'AccommodationMathematicsResponse SpeechToText;HumanScribe;HumanSigner;ExternalATDevice ;blank'
;
COMMENT ON COLUMN int_ela_sum.accomod_math_text_2_speech IS 'Translation of the Mathematics Assessment in Text-to-Speech. SPA = Spanish Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN int_ela_sum.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN int_ela_sum.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN int_ela_sum.accomod_extend_time IS 'Extended Time'
;
COMMENT ON COLUMN int_ela_sum.accomod_alt_paper_test IS 'Alternate Representation - Paper Test'
;
COMMENT ON COLUMN int_ela_sum.accomod_paper_trans_math IS 'Translation of the Mathematics Assessment in Paper. SPA = Spanish, Blank. Used to assign the form administered for paper testing based on another language other than English.Paper Only. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Pearson to supply a second test book to be Read Aloud or Signed.'
;
COMMENT ON COLUMN int_ela_sum.accomod_large_print IS 'Large Print. Y = Yes,Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.A Large Print test booklet is provided with text increased 150% to an 18 point font.'
;
COMMENT ON COLUMN int_ela_sum.accomod_braille_tactile IS 'Braille with Tactile Graphics. Y = Yes,Blank. A hard copy Braille test booklet is provided with embedded tactile graphics. Student responds and responses are transcribed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN int_ela_sum.poy IS 'Period. Spring FallBlock Derive Period based on Test Administration Code: fbkeoy14 = FallBlock fbkpba14 = FallBlock sprpba15 = Spring spreoy15 = Spring. Test student took.'
;
COMMENT ON COLUMN int_ela_sum.asmt_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON COLUMN int_ela_sum.pba_form_id IS 'PBA Form ID or  EOY Form ID.  form student tested.'
;
COMMENT ON COLUMN int_ela_sum.eoy_form_id IS 'PBA Form ID or  EOY Form ID.  form student tested.'
;
COMMENT ON COLUMN int_ela_sum.pba_test_uuid IS 'PBA Student Test UUID. System generated unique identifier assigned to the student PBA test.'
;
COMMENT ON COLUMN int_ela_sum.eoy_test_uuid IS 'PBA Student Test UUID. System generated unique identifier assigned to the student PBA test.'
;
COMMENT ON COLUMN int_ela_sum.sum_score_rec_uuid IS 'Summative Score Record UUID System Generated UUID if 01 Summative Score record was created. Unique UUID for every summative score record. System generated unique identifier for Summative record.'
;
COMMENT ON COLUMN int_ela_sum.pba_total_items IS 'Total number of questions/items on the PBA, EOY test for that form'
;
COMMENT ON COLUMN int_ela_sum.pba_attempt_flag IS 'PBA Test Attemptedness Flag. Did the student pass the attempt criteria for PBA?'
;
COMMENT ON COLUMN int_ela_sum.eoy_total_items IS 'Total number of questions/items on the PBA, EOY test for that form'
;
COMMENT ON COLUMN int_ela_sum.eoy_attempt_flag IS 'EOY Test Attemptedness Flag. Did the student pass the attempt criteria for EOY?'
;
COMMENT ON COLUMN int_ela_sum.pba_total_items_attempt IS 'PBA Total Test Items Attempted'
;
COMMENT ON COLUMN int_ela_sum.pba_total_items_unit1 IS 'PBA Unit 1 Total Number of Items'
;
COMMENT ON COLUMN int_ela_sum.pba_total_items_unit2 IS 'PBA Unit 2 Total Number of Items. Total number of items on unit 2 of the test.'
;
COMMENT ON COLUMN int_ela_sum.pba_unit2_items_attempt IS 'PBA Unit 2 Number of Attempted Items. Number of unit 2 items students attempted.'
;
COMMENT ON COLUMN int_ela_sum.pba_total_items_unit3 IS 'PBA Unit 3 Total Number of Items'
;
COMMENT ON COLUMN int_ela_sum.eoy_total_items_attempt IS 'EOY Total Test Items Attempted'
;
COMMENT ON COLUMN int_ela_sum.pba_void_reason IS 'PBA Void PBA/EOY Score Reason'
;
COMMENT ON COLUMN int_ela_sum.pba_raw_score IS 'PBA Raw Score.00000 - 99999'
;
COMMENT ON COLUMN int_ela_sum.eoy_raw_score IS 'EOY Raw Score'
;
COMMENT ON COLUMN int_ela_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN int_ela_sum.sum_csem IS 'Summative CSEM'
;
COMMENT ON COLUMN int_ela_sum.sum_perf_lvl IS 'Summative Performance Level'
;
COMMENT ON COLUMN int_ela_sum.sum_read_scale_score IS 'Summative Reading Scale Score'
;
COMMENT ON COLUMN int_ela_sum.sum_read_csem IS 'Summative Reading CSEM'
;
COMMENT ON COLUMN int_ela_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN int_ela_sum.sum_write_csem IS 'Summative Writing CSEM'
;
COMMENT ON COLUMN int_ela_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN int_ela_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN int_ela_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN int_ela_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN int_ela_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN int_ela_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN int_ela_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse'
;
COMMENT ON COLUMN int_ela_sum.district_growth_percent IS 'Student Growth Percentile Compared to District'
;
COMMENT ON COLUMN int_ela_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC'
;
COMMENT ON COLUMN int_ela_sum.year IS 'Amplify name:AssessmentYear, Parcc name: "SchoolYear
format YYYY-YYYY''. .https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19847. FORMAT YYYY-YYYY, CEDS YYYY'
;
COMMENT ON COLUMN int_ela_sum.asmt_grade IS '"Derived based on test code. Note this could be different then Grade Level When Assessed.
ELA03 - Grade 3
ELA04 - Grade 4
ELA05 - Grade 5
ELA06 - Grade 6
ELA07 - Grade 7
ELA08 - Grade 8
ELA09 - Grade 9
ELA10 - Grade 10
ELA11 - Grade 11
ELA04 - Grade 4 
MAT03  - Grade 3
MAT04  - Grade 4
MAT05  - Grade 5
MAT06  - Grade 6
MAT07  - Grade 7
MAT08  - Grade 8
ALG01 = Blank
ALG02 = Blank
GEO01 = Blank
MAT1I = Blank
MAT2I = Blank
MAT3I = Blank"
'
;
COMMENT ON COLUMN int_ela_sum.rec_status IS 'C - currenrt, I - inactive'
;

-- 
-- TABLE: int_ela_sum 
--

ALTER TABLE int_ela_sum ADD 
    CONSTRAINT int_ela_sum_pkey PRIMARY KEY (batch_guid, record_num)
;

