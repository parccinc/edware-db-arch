-- 
-- TABLE: int_item_math_flu 
--

CREATE TABLE int_item_math_flu(
    rec_id                char(10)         NOT NULL,
    batch_guid            varchar(50)      NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    rsp_entered           int2,
    student_item_score    numeric(3, 0),
    item_seq              int2,
    item_time             numeric(4, 0),
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL
)
;




-- 
-- TABLE: int_item_math_flu 
--

ALTER TABLE int_item_math_flu ADD 
    CONSTRAINT int_item_math_flu_pkey PRIMARY KEY (rec_id, batch_guid)
;

