-- 
-- TABLE: int_item_mya_meta 
--

CREATE TABLE int_item_mya_meta(
    rec_id                  char(10)        NOT NULL,
    batch_guid              varchar(50)     NOT NULL,
    rec_status              varchar(1)      DEFAULT 'C') NOT NULL,
    asmt_form_group_guid    varchar(150),
    item_guid               varchar(50)     NOT NULL,
    item_name               varchar(150)    NOT NULL,
    item_descript           varchar(250),
    evidence_guid           varchar(50),
    evidence_statement      varchar(250),
    standard_guid           varchar(50),
    standard                varchar(250),
    item_ref_id             varchar(60)     NOT NULL,
    item_max_points         varchar(30),
    item_version            varchar(30),
    test_item_type          varchar(60),
    is_item_omitted         varchar(30),
    item_status             varchar(30),
    parent_item_guid        varchar(50)     NOT NULL,
    create_date             timestamp       NOT NULL
)
;




-- 
-- TABLE: int_item_mya_meta 
--

ALTER TABLE int_item_mya_meta ADD 
    CONSTRAINT int_item_mya_meta_pkey PRIMARY KEY (rec_id, batch_guid)
;

