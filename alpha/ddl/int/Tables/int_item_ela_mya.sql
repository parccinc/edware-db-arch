-- 
-- TABLE: int_item_ela_mya 
--

CREATE TABLE int_item_ela_mya(
    rec_id                char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36)      NOT NULL,
    student_parcc_id      varchar(40),
    student_item_score    numeric(4, 0),
    batch_guid            varchar(50)      NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL
)
;




-- 
-- TABLE: int_item_ela_mya 
--

ALTER TABLE int_item_ela_mya ADD 
    CONSTRAINT int_item_ela_mya_pkey PRIMARY KEY (rec_id)
;

