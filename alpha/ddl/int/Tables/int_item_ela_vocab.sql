-- 
-- TABLE: int_item_ela_vocab 
--

CREATE TABLE int_item_ela_vocab(
    rec_id                char(10)         NOT NULL,
    batch_guid            varchar(50)      NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    student_item_score    numeric(3, 0),
    select_key            varchar(250),
    item_seq              int2,
    rec_status            varchar(1)       DEFAULT 'C') NOT NULL,
    create_date           timestamp        NOT NULL
)
;




-- 
-- TABLE: int_item_ela_vocab 
--

ALTER TABLE int_item_ela_vocab ADD 
    CONSTRAINT int_item_ela_vocab_pkey PRIMARY KEY (rec_id, batch_guid)
;

