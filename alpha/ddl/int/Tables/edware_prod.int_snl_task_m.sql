-- 
-- TABLE: int_snl_task_m 
--

CREATE TABLE int_snl_task_m(
    rec_id                    char(10)        NOT NULL,
    asmt_guid                 varchar(50)     NOT NULL,
    task_guid                 varchar(50)     NOT NULL,
    task_descript             varchar(250),
    task_short_name           char(2),
    pt_task_mode              varchar(10),
    pt_complexity             varchar(60),
    pt_stimulus_type          varchar(30),
    pt_stimulus_complexity    varchar(40),
    rec_status                char(1)         DEFAULT 'C') NOT NULL,
    create_date               timestamp       NOT NULL
)
;




-- 
-- TABLE: int_snl_task_m 
--

ALTER TABLE int_snl_task_m ADD 
    CONSTRAINT int_snl_task_m_pkey PRIMARY KEY (rec_id, task_guid)
;

