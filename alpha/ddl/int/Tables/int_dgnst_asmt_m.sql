-- 
-- TABLE: int_dgnst_asmt_m 
--

CREATE TABLE int_dgnst_asmt_m(
    rec_id                        char(10)         NOT NULL,
    asmt_guid                     varchar(50)      NOT NULL,
    admin_guid                    varchar(50)      NOT NULL,
    asmt_title                    varchar(60),
    asmt_subject                  varchar(15),
    genre                         varchar(40),
    rmm                           numeric(4, 2),
    lexile                        varchar(40),
    category_classification       varchar(10),
    probability_classification    numeric(4, 2),
    report_category               varchar(40),
    concepts_skills               varchar(40),
    cluster_classification        int8,
    domain                        varchar(40),
    cluster                       varchar(40),
    batch_guid                    varchar(50)      NOT NULL,
    total_number_of_items         int8,
    max_total_number              int2,
    rec_status                    varchar(1)       DEFAULT 'C') NOT NULL,
    create_date                   timestamp        NOT NULL
)
;




-- 
-- TABLE: int_dgnst_asmt_m 
--

ALTER TABLE int_dgnst_asmt_m ADD 
    CONSTRAINT int_dgnst_asmt_m_pkey PRIMARY KEY (rec_id, batch_guid)
;

