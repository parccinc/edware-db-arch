-- 
-- TABLE: int_math_sum_item_score 
--

CREATE TABLE int_math_sum_item_score(
    batch_guid           varchar(50)    NOT NULL,
    record_num           int4           NOT NULL,
    date_taken_year      char(9)        NOT NULL,
    state_code           char(2)        NOT NULL,
    resp_dist_id         varchar(15),
    resp_dist_name       varchar(60),
    resp_school_id       varchar(15),
    resp_school_name     varchar(60),
    test_dist_id         varchar(15),
    test_dist_name       varchar(60),
    test_school_id       varchar(15),
    test_school_name     varchar(60),
    admin_code           varchar(15)    NOT NULL,
    test_code            varchar(20)    NOT NULL,
    asmt_grade           char(11),
    asmt_subject         varchar(35)    NOT NULL,
    form_id              varchar(14)    NOT NULL,
    form_format          varchar(5)     NOT NULL,
    student_parcc_id     varchar(40)    NOT NULL,
    student_local_id     varchar(30)    NOT NULL,
    student_grade        varchar(11)    NOT NULL,
    test_uuid            varchar(36)    NOT NULL,
    item_uin             varchar(35)    NOT NULL,
    item_max_score       int2           NOT NULL,
    parent_item_score    int2           NOT NULL,
    rec_status           varchar(1)     DEFAULT 'C' NOT NULL,
    create_date          date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN int_math_sum_item_score.date_taken_year IS 'Amplify name:AssessmentYear, Parcc name: "SchoolYear
format YYYY-YYYY''. .https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19847. FORMAT YYYY-YYYY, CEDS YYYY'
;
COMMENT ON COLUMN int_math_sum_item_score.state_code IS 'Parcc name:State Abbreviation. The abbreviation for the state in which the SEA address is located. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19974'
;
COMMENT ON COLUMN int_math_sum_item_score.resp_dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_score.resp_dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_score.resp_school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_score.resp_school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_score.test_dist_id IS 'The Testing District responsible for administering the EOY test for a student. Rule: Report Testing district of component level for 01 records. Report when data is present for 02 and 03 records otherwise blank for missing component level data.'
;
COMMENT ON COLUMN int_math_sum_item_score.test_dist_name IS 'Name of EOY testing district. Rule: Report Testing district of component level for 01 records.Report when data is present for 02 and 03 records otherwise blank for missing component level data.'
;
COMMENT ON COLUMN int_math_sum_item_score.test_school_id IS 'The Testing School responsible for administering the PBA test for a student. Rule: Report Testing school of component level for 01 records.
Report when data is present for 02 and 03 records otherwise blank for missing component level data.'
;
COMMENT ON COLUMN int_math_sum_item_score.test_school_name IS 'Name of PBA testing school. Rule: Report Testing school of component level for 01 records. Report when data is present for 02 and 03 records otherwise blank for missing component level data.'
;
COMMENT ON COLUMN int_math_sum_item_score.asmt_grade IS '"Derived based on test code. Note this could be different then Grade Level When Assessed.
ELA03 - Grade 3
ELA04 - Grade 4
ELA05 - Grade 5
ELA06 - Grade 6
ELA07 - Grade 7
ELA08 - Grade 8
ELA09 - Grade 9
ELA10 - Grade 10
ELA11 - Grade 11
ELA04 - Grade 4 
MAT03  - Grade 3
MAT04  - Grade 4
MAT05  - Grade 5
MAT06  - Grade 6
MAT07  - Grade 7
MAT08  - Grade 8
ALG01 = Blank
ALG02 = Blank
GEO01 = Blank
MAT1I = Blank
MAT2I = Blank
MAT3I = Blank"
'
;
COMMENT ON COLUMN int_math_sum_item_score.asmt_subject IS 'Subject. Subject associated with test. Derived based on test code ELA03 through ELA11 = English Language Arts/Literacy MAT03 through MAT08 = Mathematics ALG01 = Algebra I ALG02 = Algebra II GEO01 = Geometry MAT1I = Mathematics I MAT2I = Mathematics II MAT3I = Mathematics III.'
;
COMMENT ON COLUMN int_math_sum_item_score.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested.'
;
COMMENT ON COLUMN int_math_sum_item_score.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON COLUMN int_math_sum_item_score.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN int_math_sum_item_score.student_grade IS 'Grade Level When Assessed. IT = Infant/toddler PR = Preschool PK = Prekindergarten TK = Transitional Kindergarten KG = Kindergarten 01 = First grade 02 = Second grade 03 = Third grade 04 = Fourth grade 05 = Fifth grade 06 = Sixth grade 07 = Seventh grade 08 = Eighth grade 09 = Ninth grade 10 = Tenth grade 11 = Eleventh grade 12 = Twelfth grade 13 = Grade 13 PS = Postsecondary UG = Ungraded Other = Other OutOfSchool = Out of school'
;
COMMENT ON COLUMN int_math_sum_item_score.test_uuid IS 'PBA Student Test UUID. System generated unique identifier assigned to the student PBA test.'
;
COMMENT ON COLUMN int_math_sum_item_score.rec_status IS 'C - currenrt, I - inactive'
;
COMMENT ON TABLE int_math_sum_item_score IS 'Student score on particular item. Source XML'
;

-- 
-- TABLE: int_math_sum_item_score 
--

ALTER TABLE int_math_sum_item_score ADD 
    CONSTRAINT int_math_sum_item_score_pkey PRIMARY KEY (batch_guid, record_num)
;

