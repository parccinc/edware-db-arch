-- 
-- TABLE: int_math_sum_item_rpt 
--

CREATE TABLE int_math_sum_item_rpt(
    batch_guid                   varchar(50)      NOT NULL,
    record_num                   int4             NOT NULL,
    test_code                    varchar(20)      NOT NULL,
    test_key                     int2             NOT NULL,
    state_code                   char(2)          NOT NULL,
    state_name                   varchar(50),
    state_key                    int4,
    dist_id                      varchar(15),
    dist_name                    varchar(60),
    dist_key                     int4,
    school_id                    varchar(15),
    school_name                  varchar(60),
    school_key                   int4,
    student_parcc_id             varchar(40)      NOT NULL,
    student_state_id             varchar(40),
    student_local_id             varchar(40),
    student_key                  int4,
    student_name                 varchar(110)     NOT NULL,
    is_male_key                  int2             DEFAULT 0 NOT NULL,
    is_female_key                int2             DEFAULT 0 NOT NULL,
    is_other_sex_key             int2             DEFAULT 0 NOT NULL,
    student_grade                varchar(12)      NOT NULL,
    student_grade_key            int2             NOT NULL,
    is_hisp_latino_key           int2             DEFAULT -1 NOT NULL,
    is_indian_alaska_key         int2             DEFAULT -1 NOT NULL,
    is_asian_key                 int2             DEFAULT -1 NOT NULL,
    is_black_key                 int2             DEFAULT -1 NOT NULL,
    is_hawai_key                 int2             DEFAULT -1 NOT NULL,
    is_white_key                 int2             DEFAULT -1 NOT NULL,
    is_ell_key                   int2             DEFAULT -1 NOT NULL,
    is_lep_key                   int2             DEFAULT -1 NOT NULL,
    is_gift_talent_key           int2             DEFAULT -1 NOT NULL,
    is_econo_disadvant_key       int2             DEFAULT -1 NOT NULL,
    is_aut_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_db_disabil_key            int2             DEFAULT -1 NOT NULL,
    is_dd_disabil_key            int2             DEFAULT -1 NOT NULL,
    is_emn_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_hi_disabil_key            int2             DEFAULT -1 NOT NULL,
    is_id_disabil_key            int2             DEFAULT -1 NOT NULL,
    is_md_disabil_key            int2             DEFAULT -1 NOT NULL,
    is_oi_disabil_key            int2             DEFAULT -1 NOT NULL,
    is_ohi_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_sld_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_sli_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_tbi_disabil_key           int2             DEFAULT -1 NOT NULL,
    is_vi_disabil_key            int2             DEFAULT -1 NOT NULL,
    end_year_key                 numeric(4, 0)    NOT NULL,
    poy_key                      int2             NOT NULL,
    subject_key                  int2             NOT NULL,
    item_uin                     varchar(35)      NOT NULL,
    item_max_score               numeric(4, 0)    DEFAULT -1 NOT NULL,
    item_score                   int2,
    item_type                    varchar(60)      NOT NULL,
    standard                     varchar(250),
    evidence_statement           varchar(250),
    is_reported_summative        int2             DEFAULT 1 NOT NULL,
    is_roster_reported           int2             DEFAULT 1,
    report_suppression_code      int2             DEFAULT -1 NOT NULL,
    report_suppression_action    int2             DEFAULT -1 NOT NULL,
    is_school_agg                int2             DEFAULT 1 NOT NULL,
    is_dist_agg                  int2             DEFAULT 1 NOT NULL,
    is_state_agg                 int2             DEFAULT 1 NOT NULL,
    is_parcc_agg                 int2             DEFAULT 1 NOT NULL,
    is_roster_rpt                int2             DEFAULT 1 NOT NULL,
    is_roster_score_rpt          int2             DEFAULT 1 NOT NULL,
    rec_status                   varchar(1)       DEFAULT 'C' NOT NULL,
    create_date                  date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN int_math_sum_item_rpt.state_code IS 'Parcc name:State Abbreviation. The abbreviation for the state in which the SEA address is located. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19974'
;
COMMENT ON COLUMN int_math_sum_item_rpt.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_rpt.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_rpt.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_rpt.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN int_math_sum_item_rpt.student_parcc_id IS 'Amplify name:
PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20775'
;
COMMENT ON COLUMN int_math_sum_item_rpt.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.'
;
COMMENT ON COLUMN int_math_sum_item_rpt.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN int_math_sum_item_rpt.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_hisp_latino_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_indian_alaska_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_asian_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_black_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_hawai_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_white_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_ell_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_lep_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_gift_talent_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_econo_disadvant_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_aut_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_db_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_dd_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_emn_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_hi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_id_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_md_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_oi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_ohi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_sld_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_sli_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_tbi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_vi_disabil_key IS '1 = Y, -1'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_reported_summative IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_roster_reported IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_school_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_dist_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_state_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_parcc_agg IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_roster_rpt IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.is_roster_score_rpt IS '1 for Y, 0 for No'
;
COMMENT ON COLUMN int_math_sum_item_rpt.rec_status IS 'C - currenrt, I - inactive'
;

-- 
-- TABLE: int_math_sum_item_rpt 
--

ALTER TABLE int_math_sum_item_rpt ADD 
    CONSTRAINT int_math_sum_item_rpt_pkey PRIMARY KEY (batch_guid, record_num)
;

