-- 
-- TABLE: stg_asmt_skill_m 
--

CREATE TABLE stg_asmt_skill_m(
    batch_guid               varchar(50)    NOT NULL,
    record_num               char(10)       NOT NULL,
    asmt_guid                varchar(50)    NOT NULL,
    skill_guid               varchar(36),
    skill_name               varchar(40)    NOT NULL,
    max_number_per_skill     int2,
    total_number_of_items    int4,
    create_date              timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_asmt_skill_m 
--

ALTER TABLE stg_asmt_skill_m ADD 
    CONSTRAINT stg_asmt_skill_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

