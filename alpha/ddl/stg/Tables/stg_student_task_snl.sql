-- 
-- TABLE: stg_student_task_snl 
--

CREATE TABLE stg_student_task_snl(
    batch_guid             varchar(50)      NOT NULL,
    record_num             char(10)         NOT NULL,
    asmt_attempt_guid      varchar(36)      NOT NULL,
    student_parcc_id       varchar(50),
    task_guid              varchar(50),
    task_name              varchar(60),
    total_task_score       numeric(4, 0),
    observation            varchar(250),
    dim_score1             numeric(4, 0),
    dim_score2             numeric(4, 0),
    dim_score3             numeric(4, 0),
    dim_score4             numeric(4, 0),
    dim_score5             numeric(4, 0),
    dr_evidence_observ1    char(1),
    dr_evidence_observ2    char(1),
    dr_evidence_observ3    char(1),
    dr_evidence_observ4    char(1),
    dr_evidence_observ5    char(1),
    create_date            timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_student_task_snl 
--

ALTER TABLE stg_student_task_snl ADD 
    CONSTRAINT stg_student_task_snl_pkey PRIMARY KEY (batch_guid, record_num)
;

