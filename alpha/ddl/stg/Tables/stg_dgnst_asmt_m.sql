-- 
-- TABLE: stg_dgnst_asmt_m 
--

CREATE TABLE stg_dgnst_asmt_m(
    batch_guid                    varchar(50)    NOT NULL,
    record_num                    char(10)       NOT NULL,
    admin_guid                    varchar(50)    NOT NULL,
    asmt_guid                     varchar(50),
    asmt_title                    varchar(60),
    asmt_subject                  varchar(15),
    genre                         varchar(50),
    rmm                           varchar(30),
    lexile                        varchar(50),
    category_classification       varchar(15),
    probability_classification    varchar(25),
    report_category               varchar(50),
    concepts_skills               varchar(50),
    cluster_classification        varchar(40),
    domain                        varchar(50),
    cluster                       varchar(50),
    total_number_of_items         int8,
    max_total_number              int2,
    create_date                   timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_dgnst_asmt_m 
--

ALTER TABLE stg_dgnst_asmt_m ADD 
    CONSTRAINT stg_dgnst_asmt_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

