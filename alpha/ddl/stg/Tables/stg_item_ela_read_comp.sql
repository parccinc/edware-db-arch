-- 
-- TABLE: stg_item_ela_read_comp 
--

CREATE TABLE stg_item_ela_read_comp(
    batch_guid            varchar(50)      NOT NULL,
    record_num            char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36)      NOT NULL,
    item_guid             varchar(50)      NOT NULL,
    student_parcc_id      varchar(40),
    student_item_score    numeric(3, 0),
    select_key            varchar(250),
    item_seq              int2,
    create_date           timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_item_ela_read_comp 
--

ALTER TABLE stg_item_ela_read_comp ADD 
    CONSTRAINT stg_item_ela_read_comp_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

