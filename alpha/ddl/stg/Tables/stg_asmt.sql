-- 
-- TABLE: stg_asmt 
--

CREATE TABLE stg_asmt(
    batch_guid      varchar(50)    NOT NULL,
    record_num      char(10)       NOT NULL,
    asmt_guid       varchar(50),
    admin_guid      varchar(50),
    asmt_title      varchar(60),
    asmt_subject    varchar(15),
    create_date     timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_asmt 
--

ALTER TABLE stg_asmt ADD 
    CONSTRAINT stg_asmt_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

