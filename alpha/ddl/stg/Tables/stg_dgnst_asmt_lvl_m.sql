-- 
-- TABLE: stg_dgnst_asmt_lvl_m 
--

CREATE TABLE stg_dgnst_asmt_lvl_m(
    batch_guid         varchar(50)    NOT NULL,
    record_num         char(10)       NOT NULL,
    asmt_guid          varchar(50)    NOT NULL,
    asmt_lvl_design    char(2)        NOT NULL,
    create_date        timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_dgnst_asmt_lvl_m 
--

ALTER TABLE stg_dgnst_asmt_lvl_m ADD 
    CONSTRAINT stg_dgnst_asmt_lvl_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

