-- 
-- TABLE: stg_dgnst_math_item_m 
--

CREATE TABLE stg_dgnst_math_item_m(
    batch_guid             varchar(50)     NOT NULL,
    record_num             char(10)        NOT NULL,
    asmt_guid              varchar(50)     NOT NULL,
    subclaim_name          varchar(30),
    item_guid              varchar(50),
    item_name              varchar(150),
    item_descript          varchar(250),
    test_item_type         varchar(60),
    passage_type           varchar(50),
    text_complexity        varchar(50),
    content_standard       varchar(50),
    evidence_stmt1         varchar(50),
    evidence_stmt2         varchar(50),
    evidence_stmt3         varchar(50),
    word_count             varchar(10),
    math_correct_answer    varchar(255),
    item_strand            varchar(40),
    test_skill1            varchar(10),
    test_skill2            varchar(10),
    test_skill3            varchar(10),
    test_skill4            varchar(10),
    test_skill5            varchar(10),
    create_date            timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_dgnst_math_item_m 
--

ALTER TABLE stg_dgnst_math_item_m ADD 
    CONSTRAINT stg_dgnst_math_item_m_pkey PRIMARY KEY (batch_guid, record_num)
;

