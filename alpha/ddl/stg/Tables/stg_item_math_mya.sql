-- 
-- TABLE: stg_item_math_mya 
--

CREATE TABLE stg_item_math_mya(
    batch_guid            varchar(50)      NOT NULL,
    record_num            char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36),
    item_guid             varchar(50),
    student_parcc_id      varchar(40),
    student_item_score    numeric(4, 0),
    create_date           timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_item_math_mya 
--

ALTER TABLE stg_item_math_mya ADD 
    CONSTRAINT stg_item_math_mya_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

