-- 
-- TABLE: stg_item 
--

CREATE TABLE stg_item(
    batch_guid              varchar(50)     NOT NULL,
    record_num              char(10)        NOT NULL,
    asmt_form_group_guid    varchar(150)    NOT NULL,
    item_guid               varchar(50)     NOT NULL,
    item_name               varchar(150)    NOT NULL,
    item_descript           varchar(250),
    item_ref_id             varchar(60)     NOT NULL,
    item_max_points         varchar(30),
    item_version            varchar(30),
    test_item_type          varchar(60),
    is_item_omitted         varchar(30),
    item_status             varchar(30),
    parent_item_guid        varchar(50)     NOT NULL,
    create_date             timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_item 
--

ALTER TABLE stg_item ADD 
    CONSTRAINT stg_item_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

