-- 
-- TABLE: stg_form_group_item 
--

CREATE TABLE stg_form_group_item(
    rec_id         char(10)       NOT NULL,
    batch_guid     varchar(50)    NOT NULL,
    asmt_guid      varchar(50)    NOT NULL,
    form_guid      varchar(50)    NOT NULL,
    group_guid     varchar(50)    NOT NULL,
    item_guid      varchar(50)    NOT NULL,
    create_date    timestamp      NOT NULL
)
;




-- 
-- TABLE: stg_form_group_item 
--

ALTER TABLE stg_form_group_item ADD 
    CONSTRAINT stg_form_group_ite_pkey PRIMARY KEY (rec_id, batch_guid)
;

