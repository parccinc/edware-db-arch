-- 
-- TABLE: stg_item_math_flu 
--

CREATE TABLE stg_item_math_flu(
    batch_guid            varchar(50)      NOT NULL,
    record_num            char(10)         NOT NULL,
    asmt_attempt_guid     varchar(36)      NOT NULL,
    item_guid             varchar(50)      NOT NULL,
    student_parcc_id      varchar(40),
    rsp_entered           int2,
    student_item_score    numeric(3, 0),
    item_seq              int2,
    item_time             numeric(4, 0),
    create_date           timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_item_math_flu 
--

ALTER TABLE stg_item_math_flu ADD 
    CONSTRAINT stg_item_math_flu_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

