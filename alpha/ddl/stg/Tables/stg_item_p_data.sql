-- 
-- TABLE: stg_item_p_data 
--

CREATE TABLE stg_item_p_data(
    batch_guid                varchar(50)      NOT NULL,
    record_num                char(10)         NOT NULL,
    p_value                   numeric(5, 2),
    p_distractor1             numeric(5, 2),
    p_distractor2             numeric(5, 2),
    p_distractor3             numeric(5, 2),
    p_distractor4             numeric(5, 2),
    p_distractor5             numeric(5, 2),
    p_distractor6             numeric(5, 2),
    p_distractor7             numeric(5, 2),
    p_distractor8             numeric(5, 2),
    p_distractor9             numeric(5, 2),
    p_distractor10            numeric(5, 2),
    p_score0                  numeric(5, 2),
    p_score1                  numeric(5, 2),
    p_score2                  numeric(5, 2),
    p_score3                  numeric(5, 2),
    p_score4                  numeric(5, 2),
    p_score5                  numeric(5, 2),
    p_score6                  numeric(5, 2),
    p_score7                  numeric(5, 2),
    p_score8                  numeric(5, 2),
    p_score9                  numeric(5, 2),
    p_score10                 numeric(5, 2),
    point_biserial_correct    numeric(7, 4),
    point_biserial_0          numeric(7, 4),
    point_biserial_1          numeric(7, 4),
    point_biserial_2          numeric(7, 4),
    point_biserial_3          numeric(7, 4),
    point_biserial_4          numeric(7, 4),
    point_biserial_5          numeric(7, 4),
    point_biserial_6          numeric(7, 4),
    point_biserial_7          numeric(7, 4),
    point_biserial_8          numeric(7, 4),
    point_biserial_9          numeric(7, 4),
    point_biserial_10         numeric(7, 4),
    dif_any                   boolean,
    dif_female                char(1),
    dif_aa                    char(1),
    dif_hispanic              char(1),
    dif_asian                 char(1),
    dif_na                    char(1),
    dif_swd                   char(1),
    dif_ell                   char(1),
    dif_ses                   char(1),
    diff_swd_acc              char(1),
    diff_swd_non              char(1),
    diff_ell_acc              char(1),
    diff_ell_non              char(1),
    p_omit                    numeric(5, 2),
    a_parameter               numeric(7, 4),
    b_parameter               numeric(7, 4),
    c_parameter               numeric(7, 4),
    step_1                    numeric(5, 2),
    step_2                    numeric(5, 2),
    step_3                    numeric(5, 2),
    step_4                    numeric(5, 2),
    step_5                    numeric(5, 2),
    step_6                    numeric(5, 2),
    step_7                    numeric(5, 2),
    step_8                    numeric(5, 2),
    step_9                    numeric(5, 2),
    step_10                   numeric(5, 2),
    n_size                    int4,
    item_guid                 varchar(50),
    item_group_guid           varchar(50),
    paired_item_group_guid    varchar(50),
    item_key_response         varchar(250),
    item_admin_type           varchar(50),
    asmt_subject              varchar(15),
    item_grade_course         varchar(20),
    item_delivery_mode        varchar(50),
    test_item_type            varchar(50),
    date_taken_month          varchar(10),
    date_taken_year           numeric(4, 0),
    is_calc_allowed           boolean,
    standard                  varchar(250),
    evidence_statement        varchar(250),
    create_date               timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_item_p_data 
--

ALTER TABLE stg_item_p_data ADD 
    CONSTRAINT stg_item_p_data_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

