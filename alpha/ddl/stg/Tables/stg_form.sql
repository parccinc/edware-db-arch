-- 
-- TABLE: stg_form 
--

CREATE TABLE stg_form(
    batch_guid                       varchar(50)     NOT NULL,
    record_num                       char(10)        NOT NULL,
    form_guid                        varchar(100),
    asmt_guid                        varchar(50),
    form_format                      varchar(50),
    form_name                        varchar(100),
    form_code                        varchar(100),
    form_ref_id                      varchar(60),
    form_version                     varchar(30),
    asmt_score_max                   int2,
    asmt_perf_lvl1_guid              varchar(50),
    asmt_perf_lvl1_cutpoint_label    varchar(30),
    asmt_perf_lvl1_cutpoint_low      int2,
    asmt_perf_lvl1_cutpoint_upp      int2,
    asmt_perf_lvl2_guid              varchar(50),
    asmt_perf_lvl2_cutpoint_label    varchar(30),
    asmt_perf_lvl2_cutpoint_low      int2,
    asmt_perf_lvl2_cutpoint_upp      int2,
    asmt_perf_lvl3_guid              varchar(50),
    asmt_perf_lvl3_cutpoint_label    varchar(30),
    asmt_perf_lvl3_cutpoint_low      int2,
    asmt_perf_lvl3_cutpoint_upp      int2,
    asmt_perf_lvl4_guid              varchar(50),
    asmt_perf_lvl4_cutpoint_label    varchar(30),
    asmt_perf_lvl4_cutpoint_low      int2,
    asmt_perf_lvl4_cutpoint_upp      int2,
    asmt_perf_lvl5_guid              varchar(50),
    asmt_perf_lvl5_cutpoint_label    varchar(30),
    asmt_perf_lvl5_cutpoint_low      int2,
    asmt_perf_lvl5_cutpoint_upp      int2,
    create_date                      timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_form 
--

ALTER TABLE stg_form ADD 
    CONSTRAINT stg_form_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

