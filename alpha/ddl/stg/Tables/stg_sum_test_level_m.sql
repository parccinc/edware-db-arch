-- 
-- TABLE: stg_sum_test_level_m 
--

CREATE TABLE stg_sum_test_level_m(
    batch_guid        varchar(50)      NOT NULL,
    record_num        char(10)         NOT NULL,
    test_code         varchar(20)      NOT NULL,
    perf_lvl_id       varchar(30),
    cutpoint_label    varchar(50),
    cutpoint_low      numeric(4, 0),
    cutpoint_upper    numeric(4, 0),
    create_date       timestamp        DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_sum_test_level_m 
--

ALTER TABLE stg_sum_test_level_m ADD 
    CONSTRAINT stg_sum_test_level_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

