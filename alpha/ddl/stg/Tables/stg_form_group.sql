-- 
-- TABLE: stg_form_group 
--

CREATE TABLE stg_form_group(
    batch_guid           varchar(50)     NOT NULL,
    record_num           char(10)        NOT NULL,
    asmt_form_guid       varchar(100),
    group_guid           varchar(50),
    group_code           varchar(50),
    group_name           varchar(60),
    group_desc           varchar(256),
    group_type           varchar(30),
    parent_group_guid    varchar(50),
    group_ref_id         varchar(60),
    create_date          timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_form_group 
--

ALTER TABLE stg_form_group ADD 
    CONSTRAINT stg_form_group_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

