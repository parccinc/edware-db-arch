-- 
-- TABLE: stg_snl_asmt_m 
--

CREATE TABLE stg_snl_asmt_m(
    batch_guid             varchar(50)     NOT NULL,
    record_num             char(10)        NOT NULL,
    admin_guid             varchar(50)     NOT NULL,
    asmt_guid              varchar(50)     NOT NULL,
    asmt_title             varchar(60),
    asmt_subject           varchar(15),
    document_id            varchar(40),
    set_id                 varchar(35),
    set_seq_num            varchar(10),
    evidence_stmt1         varchar(250),
    evidence_stmt_guid1    varchar(50),
    evidence_stmt2         varchar(250),
    evidence_stmt_guid2    varchar(50),
    evidence_stmt3         varchar(250),
    evidence_stmt_guid3    varchar(50),
    evidence_stmt4         varchar(250),
    evidence_stmt_guid4    varchar(50),
    evidence_stmt5         varchar(250),
    evidence_stmt_guid5    varchar(50),
    rubric1                varchar(30),
    rubric2                varchar(30),
    create_date            timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_snl_asmt_m 
--

ALTER TABLE stg_snl_asmt_m ADD 
    CONSTRAINT stg_snl_asmt_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

