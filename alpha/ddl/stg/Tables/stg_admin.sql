-- 
-- TABLE: stg_admin 
--

CREATE TABLE stg_admin(
    batch_guid         varchar(50)    NOT NULL,
    record_num         char(10)       NOT NULL,
    file_type          varchar(50),
    admin_guid         varchar(50),
    admin_name         varchar(60),
    admin_code         varchar(50),
    admin_type         varchar(32),
    admin_period       varchar(32),
    date_taken_year    char(9)        NOT NULL,
    create_date        timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_admin 
--

ALTER TABLE stg_admin ADD 
    CONSTRAINT stg_admin_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

