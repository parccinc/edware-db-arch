-- 
-- TABLE: stg_asmt_lvl 
--

CREATE TABLE stg_asmt_lvl(
    batch_guid         varchar(50)    NOT NULL,
    record_num         char(10)       NOT NULL,
    asmt_guid          varchar(50)    NOT NULL,
    asmt_lvl_design    char(2)        NOT NULL,
    create_date        timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_asmt_lvl 
--

ALTER TABLE stg_asmt_lvl ADD 
    CONSTRAINT stg_asmt_lvl_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

