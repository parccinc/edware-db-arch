-- 
-- TABLE: stg_snl_task_m 
--

CREATE TABLE stg_snl_task_m(
    batch_guid                varchar(50)     NOT NULL,
    record_num                char(10)        NOT NULL,
    asmt_guid                 varchar(50)     NOT NULL,
    task_guid                 varchar(50)     NOT NULL,
    task_descript             varchar(250),
    task_short_name           char(2),
    pt_task_mode              varchar(10),
    pt_complexity             varchar(60),
    pt_stimulus_type          varchar(30),
    pt_stimulus_complexity    varchar(40),
    create_date               timestamp       DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_snl_task_m 
--

ALTER TABLE stg_snl_task_m ADD 
    CONSTRAINT stg_snl_task_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

