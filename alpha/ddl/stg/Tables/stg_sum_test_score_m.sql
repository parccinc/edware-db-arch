-- 
-- TABLE: stg_sum_test_score_m 
--

CREATE TABLE stg_sum_test_score_m(
    batch_guid     varchar(50)    NOT NULL,
    record_num     char(10)       NOT NULL,
    test_code      varchar(20)    NOT NULL,
    sum_period     varchar(20)    NOT NULL,
    create_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_sum_test_score_m 
--

ALTER TABLE stg_sum_test_score_m ADD 
    CONSTRAINT stg_sum_test_score_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

