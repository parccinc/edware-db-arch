-- 
-- TABLE: stg_file_store 
--

CREATE TABLE stg_file_store(
    rec_id         char(10)        NOT NULL,
    batch_guid     varchar(50)     NOT NULL,
    admin_guid     varchar(50)     NOT NULL,
    asmt_guid      varchar(50)     NOT NULL,
    file_type      varchar(10)     NOT NULL,
    file_name      varchar(150)    NOT NULL,
    page_no        int8            NOT NULL,
    file_data      bytea           NOT NULL,
    create_date    timestamp       NOT NULL
)
;




-- 
-- TABLE: stg_file_store 
--

ALTER TABLE stg_file_store ADD 
    CONSTRAINT stg_file_store_pkey PRIMARY KEY (rec_id, batch_guid)
;

