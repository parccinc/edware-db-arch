-- 
-- TABLE: stg_snl_asmt_mode_m 
--

CREATE TABLE stg_snl_asmt_mode_m(
    batch_guid     varchar(50)    NOT NULL,
    record_num     char(10)       NOT NULL,
    asmt_guid      varchar(50)    NOT NULL,
    asmt_mode      varchar(5),
    create_date    timestamp      DEFAULT now()) NOT NULL
)
;




-- 
-- TABLE: stg_snl_asmt_mode_m 
--

ALTER TABLE stg_snl_asmt_mode_m ADD 
    CONSTRAINT stg_snl_asmt_mode_m_pkey_1 PRIMARY KEY (batch_guid, record_num)
;

