-- BM 2015-05-20: set default search path for role. 
alter role ${AMPDB_USER} set search_path to public, analytics; 

-- correct DDL 

--________________________________
--MODIFIED: dim_accomod
--________________________________

ALTER TABLE ONLY dim_accomod ALTER COLUMN rec_version SET DEFAULT (0);
ALTER TABLE ONLY dim_accomod ALTER COLUMN valid_from SET DEFAULT '1900/01/01 00:00:00.000';
ALTER TABLE ONLY dim_accomod ALTER COLUMN valid_to SET DEFAULT '2199/12/31 23:59:59.999';


--________________________________
--MODIFIED: dim_poy
--________________________________

ALTER TABLE ONLY dim_poy ALTER COLUMN poy_key TYPE numeric(4,0);
ALTER TABLE ONLY dim_poy ALTER COLUMN poy SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY dim_poy ALTER COLUMN end_year TYPE varchar(9); -- update BM 2015-05-15: new data set has year in the format '2014-2015'
ALTER TABLE ONLY dim_poy ALTER COLUMN end_year SET DEFAULT ('UNKNOWN');
ALTER TABLE ONLY dim_poy ALTER COLUMN school_year SET DEFAULT 'UNKNOWN';
--ALTER TABLE ONLY dim_poy ALTER COLUMN rec_version SET DEFAULT (0);
ALTER TABLE ONLY dim_poy ALTER COLUMN valid_from SET DEFAULT '1900/01/01 00:00:00.000';
ALTER TABLE ONLY dim_poy ALTER COLUMN valid_to SET DEFAULT '2199/12/31 23:59:59.999';




--________________________________
--MODIFIED: dim_student
--________________________________
-- BM 2015-05-27: waiting for DDL change. 
alter table dim_student alter column student_dob type varchar(10);

ALTER TABLE ONLY dim_student ALTER COLUMN student_key TYPE numeric(6,0);
-- 2015-05-19 BM: new ethnicity default from data issues + longer varchar required.  
ALTER TABLE ONLY dim_student ALTER COLUMN ethnicity TYPE varchar(150);
ALTER TABLE ONLY dim_student ALTER COLUMN ethnicity SET DEFAULT 'could not resolve';
ALTER TABLE ONLY dim_student ALTER COLUMN primary_disabil_type TYPE TEXT;
ALTER TABLE ONLY dim_student ALTER COLUMN student_parcc_id SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY dim_student ALTER COLUMN student_dob SET DEFAULT 'UNKNOWN';
--ALTER TABLE ONLY dim_student ALTER COLUMN rec_version SET DEFAULT (0);
alter table dim_student drop column valid_from;
alter table dim_student drop column valid_to;
alter table dim_student add column valid_from timestamp;
alter table dim_student add column valid_to timestamp;
ALTER TABLE ONLY dim_student ALTER COLUMN valid_from SET DEFAULT '1900/01/01 00:00:00.000';
ALTER TABLE ONLY dim_student ALTER COLUMN valid_to SET DEFAULT '2199/12/31 23:59:59.999';


--________________________________
--MODIFIED: dim_test
--________________________________

ALTER TABLE ONLY dim_test ALTER COLUMN test_form_key TYPE numeric(4,0);
ALTER TABLE ONLY dim_test ALTER COLUMN test_code SET DEFAULT 'UNKNOWN';
--ALTER TABLE ONLY dim_test ALTER COLUMN rec_version SET DEFAULT (0);
ALTER TABLE ONLY dim_test ALTER COLUMN asmt_grade set DEFAULT 'UNKNOWN';
ALTER TABLE ONLY dim_test ALTER COLUMN valid_from SET DEFAULT '1900/01/01 00:00:00.000';
ALTER TABLE ONLY dim_test ALTER COLUMN valid_to SET DEFAULT '2199/12/31 23:59:59.999';



--________________________________
--MODIFIED: dim_institution
--________________________________

ALTER TABLE ONLY dim_institution ALTER COLUMN school_key TYPE numeric(4,0);
ALTER TABLE ONLY dim_institution ALTER COLUMN dist_id SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY dim_institution ALTER COLUMN school_name SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY dim_institution ALTER COLUMN school_id SET DEFAULT 'UNKNOWN';
--ALTER TABLE ONLY dim_institution ALTER COLUMN rec_version SET DEFAULT (0);
ALTER TABLE ONLY dim_institution ALTER COLUMN valid_from SET DEFAULT '1900/01/01 00:00:00.000';
ALTER TABLE ONLY dim_institution ALTER COLUMN valid_to SET DEFAULT '2199/12/31 23:59:59.999';
ALTER TABLE ONLY dim_institution ADD COLUMN state_id CHAR(3) default 'N/A'; 


--________________________________
--MODIFIED: fact_sum
--________________________________

ALTER TABLE ONLY fact_sum ALTER COLUMN poy_key TYPE numeric(4,0);
ALTER TABLE ONLY fact_sum ALTER COLUMN attempt_flag TYPE char(2);
ALTER TABLE ONLY fact_sum ALTER COLUMN multirecord_flag TYPE char(2); 
ALTER TABLE ONLY fact_sum ALTER COLUMN test_uuid SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY fact_sum ALTER COLUMN result_type SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY fact_sum ALTER COLUMN reported_score_flag SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY fact_sum ALTER COLUMN record_type SET DEFAULT 'UNKNOWN';
ALTER TABLE ONLY fact_sum ALTER COLUMN include_in_parcc SET DEFAULT 'N';
ALTER TABLE ONLY fact_sum ALTER COLUMN include_in_state SET DEFAULT 'N';
ALTER TABLE ONLY fact_sum ALTER COLUMN include_in_district SET DEFAULT 'N';
ALTER TABLE ONLY fact_sum ALTER COLUMN include_in_school SET DEFAULT 'N';
ALTER TABLE ONLY fact_sum ALTER COLUMN include_in_isr SET DEFAULT 'N';
ALTER TABLE ONLY fact_sum ALTER COLUMN include_in_roster SET DEFAULT 'N';

ALTER TABLE ONLY fact_sum ALTER COLUMN attempt_flag TYPE varchar(8);



create or replace function patch_column() returns void as
$$
begin
    if exists (
        select * from information_schema.columns
            where table_name='fact_sum'
            and column_name='me_flag'
     )
    then
        raise notice 'missing_col already exists';
    else
        alter table fact_sum
            add column me_flag char(1);
    end if;
end;
$$ language plpgsql;

select patch_column();

drop function if exists patch_column();

--________________________________
--MODIFIED: look_up indexes
--________________________________
drop function if exists patch_index(text, text, text);
create or replace function patch_index(TEXT, TEXT, TEXT) returns void as
$$
BEGIN
IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = $1
    ) THEN EXECUTE 'CREATE INDEX '||$1||' ON '||$2||'('||$3||')';
END IF;
END
$$ language plpgsql;

select patch_index('idx_dim_poy_lookup', 'dim_poy', 'poy, school_year');
select patch_index('idx_dim_test_lookup', 'dim_test', 'test_code, test_category');
select patch_index('idx_dim_student_lookup', 'dim_student', 'student_parcc_id');
select patch_index('idx_dim_institution_lookup', 'dim_institution', 'school_id');

drop function if exists patch_index(text, text, text); 



-- BM 2015-05-15: fact_sum_fk12 points incorrectly to dim_accomod  
alter table fact_sum drop constraint if exists fact_sum_fk12; 
alter table fact_sum drop constraint if exists fact_sum_fk7; 


-- BM 2015-05-19: 
-- alter table fact_sum add column form_id varchar(14); 
alter table fact_sum add column form_category varchar(2);
alter table only fact_sum alter column me_flag type varchar(8);




-- BM 2015-05-25: add additional form columns.
alter table dim_test add pba_form_id varchar(50); 
alter table dim_test add eoy_form_id varchar(50); 
alter table dim_test add pba_category char(1); 
alter table dim_test add eoy_category char(1); 

alter table dim_opt_state_data alter column valid_from drop not null; 
alter table dim_opt_state_data alter column valid_to drop not null; 


alter table dim_student alter column student_key set data type numeric(7,0);


-- BM 2015-05-25: truncate all tables for a clean reload. 
truncate table dim_opt_state_data cascade; 
truncate table dim_poy cascade; 
truncate table dim_student cascade; 
truncate table dim_institution cascade; 
truncate table dim_test cascade; 
truncate table dim_accomod cascade; 
truncate table fact_sum cascade; 

-- BM 2015-05-19: add default row for dim_accomod. 
alter table dim_accomod drop column accomod_text_2_speech_math_ela;
alter table dim_accomod add column accomod_text_2_speech_ela char(1);
alter table dim_accomod add column accomod_text_2_speech_math char(1);

alter table dim_accomod drop column accomod_read_math_ela;
alter table dim_accomod add column  accomod_read_ela varchar(50);
alter table dim_accomod add column  accomod_read_math varchar(50);

insert into dim_accomod (rec_key, accomod_ell, accomod_504, accomod_ind_ed, accomod_freq_breacks, accomod_alt_location, accomod_small_group, accomod_special_equip, accomod_spec_area, accomod_time_day, accomod_answer_mask, accomod_color_contrast, accomod_asl_video, accomod_screen_reader, accomod_close_capt_ela, accomod_read_ela, accomod_read_math, accomod_braille_ela, accomod_tactile_graph, accomod_text_2_speech_math, accomod_text_2_speech_ela, accomod_answer_rec, accomod_braille_resp, accomod_construct_resp_ela, accomod_select_resp_ela, accomod_monitor_test_resp, accomod_word_predict, accomod_native_lang, accomod_loud_native_lang, accomod_w_2_w_dict, accomod_extend_time, accomod_alt_paper_test, accomod_human_read_sign, accomod_large_print, accomod_braille_tactile, accomod_math_resp, accomod_calculator, accomod_math_trans_online, accomod_paper_trans_math, valid_from, valid_to, rec_version) 
values (0,'N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N', 'N', 'N','N','N','N','N','N','N','N','N','N','N', 'N', 'N', now(), now(), 1); 


-- BM 2015-05-20: add default row for dim_test.
insert into dim_test values (0, 'UNKNOWN', '-','UNKNOWN','-','UNKNOWN',now(),now(),now(),1);

insert into dim_institution (school_key, dist_name, dist_id, school_name, school_id, rec_version) values (0, '-','-','-','-', 1);
insert into dim_student (student_key, student_parcc_id, rec_version, student_dob) values (0, '-', 1, 'UNKOWN');


alter table fact_sum drop column test_uuid; 
alter table fact_sum add column pba_test_uuid varchar(36); 
alter table fact_sum add column eoy_test_uuid varchar(36); 
alter table fact_sum add column sum_score_rec_uuid varchar(36); 

alter table fact_sum drop column test_school_key; 
alter table fact_sum add column eoy_test_school_key integer;
alter table fact_sum add column pba_test_school_key integer;

alter table fact_sum drop column total_items; 
alter table fact_sum add column pba_total_items integer;
alter table fact_sum add column eoy_total_items integer;

alter table fact_sum drop column attempt_flag; 
alter table fact_sum add column pba_attempt_flag char(1);
alter table fact_sum add column eoy_attempt_flag char(1);

alter table fact_sum drop column total_items_attempt; 
alter table fact_sum add column pba_total_items_attempt integer;
alter table fact_sum add column eoy_total_items_attempt integer;

alter table fact_sum drop column total_items_unit1; 
alter table fact_sum add column pba_total_items_unit1 integer;
alter table fact_sum add column eoy_total_items_unit1 integer;

alter table fact_sum drop column total_items_unit2; 
alter table fact_sum add column pba_total_items_unit2 integer;
alter table fact_sum add column eoy_total_items_unit2 integer;

alter table fact_sum drop column total_items_unit3; 
alter table fact_sum add column pba_total_items_unit3 integer;
alter table fact_sum add column eoy_total_items_unit3 integer;

alter table fact_sum drop column total_items_unit4; 
alter table fact_sum add column pba_total_items_unit4 integer;
alter table fact_sum add column eoy_total_items_unit4 integer;

alter table fact_sum drop column total_items_unit5; 
alter table fact_sum add column pba_total_items_unit5 integer;
alter table fact_sum add column eoy_total_items_unit5 integer;

alter table fact_sum drop column unit1_items_attempt; 
alter table fact_sum add column pba_unit1_items_attempt integer;
alter table fact_sum add column eoy_unit1_items_attempt integer;

alter table fact_sum drop column unit2_items_attempt; 
alter table fact_sum add column pba_unit2_items_attempt integer;
alter table fact_sum add column eoy_unit2_items_attempt integer;

alter table fact_sum drop column unit3_items_attempt; 
alter table fact_sum add column pba_unit3_items_attempt integer;
alter table fact_sum add column eoy_unit3_items_attempt integer;

alter table fact_sum drop column unit4_items_attempt; 
alter table fact_sum add column pba_unit4_items_attempt integer;
alter table fact_sum add column eoy_unit4_items_attempt integer;

alter table fact_sum drop column unit5_items_attempt; 
alter table fact_sum add column pba_unit5_items_attempt integer;
alter table fact_sum add column eoy_unit5_items_attempt integer;

alter table fact_sum drop column not_tested_reason; 
alter table fact_sum add column pba_not_tested_reason varchar(150);
alter table fact_sum add column eoy_not_tested_reason varchar(150);

alter table fact_sum drop column void_reason; 
alter table fact_sum add column pba_void_reason varchar(150);
alter table fact_sum add column eoy_void_reason varchar(150);

alter table fact_sum drop column test_form_key;
alter table fact_sum add column pba_test_key integer;
alter table fact_sum add column eoy_test_key integer;


alter table fact_sum add column summative_population integer; 
alter table fact_sum alter column summative_population set default 1; 

alter table fact_sum add column summative_population integer; 


