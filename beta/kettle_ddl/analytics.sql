--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: _final_median(numeric[]); Type: FUNCTION; Schema: public; Owner: edware
--

CREATE FUNCTION _final_median(numeric[]) RETURNS numeric
    LANGUAGE sql IMMUTABLE
    AS $_$
   SELECT AVG(val)
   FROM (
     SELECT val
     FROM unnest($1) val
     ORDER BY 1
     LIMIT  2 - MOD(array_upper($1, 1), 2)
     OFFSET CEIL(array_upper($1, 1) / 2.0) - 1
   ) sub;
$_$;


ALTER FUNCTION public._final_median(numeric[]) OWNER TO edware;

--
-- Name: median(numeric); Type: AGGREGATE; Schema: public; Owner: edware
--

CREATE AGGREGATE median(numeric) (
    SFUNC = array_append,
    STYPE = numeric[],
    INITCOND = '{}',
    FINALFUNC = _final_median
);


ALTER AGGREGATE public.median(numeric) OWNER TO edware;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dim_accomod; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE dim_accomod (
    rec_key bigint NOT NULL,
    accomod_ell character(1),
    accomod_504 character(1),
    accomod_ind_ed character(1),
    accomod_freq_breacks character(1),
    accomod_alt_location character(1),
    accomod_small_group character(1),
    accomod_special_equip character(1),
    accomod_spec_area character(1),
    accomod_time_day character(1),
    accomod_answer_mask character(1),
    accomod_color_contrast character varying(19),
    accomod_asl_video character(1),
    accomod_screen_reader character(1),
    accomod_close_capt_ela character(1),
    accomod_braille_ela character(1),
    accomod_tactile_graph character(1),
    accomod_answer_rec character(1),
    accomod_braille_resp character varying(16),
    accomod_construct_resp_ela character varying(20),
    accomod_select_resp_ela character varying(20),
    accomod_monitor_test_resp character(1),
    accomod_word_predict character(1),
    accomod_native_lang character(1),
    accomod_loud_native_lang character varying(40),
    accomod_w_2_w_dict character(1),
    accomod_extend_time character varying(6),
    accomod_alt_paper_test character(1),
    accomod_human_read_sign character varying(14),
    accomod_large_print character(1),
    accomod_braille_tactile character(1),
    accomod_math_resp_el character varying(20),
    accomod_calculator character(1),
    accomod_math_trans_online character(3),
    accomod_paper_trans_math character(3),
    accomod_math_resp character varying(20),
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    rec_version smallint DEFAULT 0 NOT NULL,
    accomod_text_2_speech_ela character(1),
    accomod_text_2_speech_math character(1),
    accomod_read_ela character varying(50),
    accomod_read_math character varying(50)
);


ALTER TABLE public.dim_accomod OWNER TO edware;

--
-- Name: COLUMN dim_accomod.accomod_ell; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_ell IS 'Assessment Accommodation:  English learner (EL)';


--
-- Name: COLUMN dim_accomod.accomod_504; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_ind_ed; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)';


--
-- Name: COLUMN dim_accomod.accomod_freq_breacks; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_alt_location; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location';


--
-- Name: COLUMN dim_accomod.accomod_small_group; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_small_group IS 'Small Testing Group';


--
-- Name: COLUMN dim_accomod.accomod_special_equip; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_special_equip IS 'Specialized Equipment or Furniture';


--
-- Name: COLUMN dim_accomod.accomod_spec_area; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_spec_area IS 'Specified Area or Setting';


--
-- Name: COLUMN dim_accomod.accomod_time_day; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_time_day IS 'Time Of Day';


--
-- Name: COLUMN dim_accomod.accomod_answer_mask; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_color_contrast; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.';


--
-- Name: COLUMN dim_accomod.accomod_asl_video; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_screen_reader; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_braille_ela; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_tactile_graph; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_answer_rec; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_braille_resp; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.';


--
-- Name: COLUMN dim_accomod.accomod_construct_resp_ela; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types';


--
-- Name: COLUMN dim_accomod.accomod_select_resp_ela; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_monitor_test_resp; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_word_predict; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_native_lang; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_native_lang IS 'General Administration Directions Clarified in Studentís Native Language (by test administrator)';


--
-- Name: COLUMN dim_accomod.accomod_loud_native_lang; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Studentís Native Language
(by test administrator)';


--
-- Name: COLUMN dim_accomod.accomod_w_2_w_dict; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_extend_time; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_extend_time IS 'Extended Time';


--
-- Name: COLUMN dim_accomod.accomod_alt_paper_test; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_alt_paper_test IS 'Alternate Representation - Paper Test';


--
-- Name: COLUMN dim_accomod.accomod_human_read_sign; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_calculator; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_math_trans_online; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_paper_trans_math; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_math_resp; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: dim_institution; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE dim_institution (
    school_key numeric(4,0) NOT NULL,
    dist_name character varying(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    dist_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_name character varying(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    rec_version smallint NOT NULL,
    state_id character(3) DEFAULT 'N/A'::bpchar
);


ALTER TABLE public.dim_institution OWNER TO edware;

--
-- Name: COLUMN dim_institution.dist_name; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_institution.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_institution.dist_id; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_institution.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_institution.school_name; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_institution.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_institution.school_id; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_institution.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: dim_institution_school_key_seq; Type: SEQUENCE; Schema: public; Owner: edware
--

CREATE SEQUENCE dim_institution_school_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dim_institution_school_key_seq OWNER TO edware;

--
-- Name: dim_institution_school_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edware
--

ALTER SEQUENCE dim_institution_school_key_seq OWNED BY dim_institution.school_key;


--
-- Name: dim_opt_state_data; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE dim_opt_state_data (
    rec_key bigint NOT NULL,
    opt_state_data8 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data7 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data6 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data5 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data4 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data3 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data2 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data1 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date,
    valid_to date,
    rec_version smallint NOT NULL
);


ALTER TABLE public.dim_opt_state_data OWNER TO edware;

--
-- Name: dim_poy; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE dim_poy (
    poy_key numeric(4,0) DEFAULT (-1) NOT NULL,
    poy character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    end_year character varying(9) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_year character(9) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    rec_version smallint NOT NULL
);


ALTER TABLE public.dim_poy OWNER TO edware;

--
-- Name: COLUMN dim_poy.poy_key; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_poy.poy_key IS 'YEAR || 01 OR YEAR || 02';


--
-- Name: COLUMN dim_poy.poy; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring';


--
-- Name: COLUMN dim_poy.end_year; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014';


--
-- Name: dim_student; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE dim_student (
    student_key numeric(7,0) NOT NULL,
    student_parcc_id character varying(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_sex character(1),
    ethnicity character varying(150) DEFAULT 'could not resolve'::character varying NOT NULL,
    ethn_hisp_latino character(1),
    ethn_indian_alaska character(1),
    ethn_asian character(1),
    ethn_black character(1),
    ethn_hawai character(1),
    ethn_white character(1),
    ethn_filler character(1),
    ethn_2_more character(1),
    ell character(1),
    lep_status character(1),
    gift_talent character(1),
    migrant_status character(1),
    econo_disadvantage character(1),
    disabil_student character(1),
    primary_disabil_type text,
    student_state_id character varying(40) DEFAULT 'UNKNOWN'::character varying,
    student_local_id character varying(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_name character varying(105) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_dob_1 character varying(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    rec_version smallint NOT NULL
);


ALTER TABLE public.dim_student OWNER TO edware;

--
-- Name: COLUMN dim_student.student_parcc_id; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.';


--
-- Name: COLUMN dim_student.ethn_hisp_latino; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_indian_alaska; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_asian; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_black; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_hawai; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_white; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_filler; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_2_more; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425';


--
-- Name: COLUMN dim_student.ell; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).';


--
-- Name: COLUMN dim_student.lep_status; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';


--
-- Name: COLUMN dim_student.gift_talent; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.';


--
-- Name: COLUMN dim_student.migrant_status; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789';


--
-- Name: COLUMN dim_student.econo_disadvantage; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';


--
-- Name: COLUMN dim_student.disabil_student; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569†';


--
-- Name: COLUMN dim_student.student_state_id; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.';


--
-- Name: COLUMN dim_student.student_local_id; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.student_local_id IS 'Parcc name: LocalStudentIdentifier. ';


--
-- Name: COLUMN dim_student.student_name; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.student_name IS 'Last name||", "|| First name ||", "||Middle Name';


--
-- Name: COLUMN dim_student.student_dob_1; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_student.student_dob_1 IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995';


--
-- Name: dim_test; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE dim_test (
    test_form_key numeric(4,0) NOT NULL,
    form_id character varying(14) DEFAULT 'NA'::character varying NOT NULL,
    test_category character varying(2) DEFAULT 'NA'::character varying NOT NULL,
    test_subject character varying(35) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    asmt_grade character varying(12) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    test_code character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    rec_version smallint NOT NULL,
    pba_form_id character varying(50),
    eoy_form_id character varying(50),
    pba_category character(1),
    eoy_category character(1)
);


ALTER TABLE public.dim_test OWNER TO edware;

--
-- Name: TABLE dim_test; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON TABLE dim_test IS 'removed course, course_key fields';


--
-- Name: COLUMN dim_test.test_form_key; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_test.test_form_key IS 'NA for summative different for different test_codes';


--
-- Name: COLUMN dim_test.form_id; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative';


--
-- Name: COLUMN dim_test.test_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component ';


--
-- Name: COLUMN dim_test.test_subject; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_test.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356';


--
-- Name: COLUMN dim_test.asmt_grade; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN dim_test.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade';


--
-- Name: fact_sum; Type: TABLE; Schema: public; Owner: edware; Tablespace: 
--

CREATE TABLE fact_sum (
    rec_id bigint NOT NULL,
    rec_key bigint NOT NULL,
    poy_key numeric(4,0) DEFAULT (-1) NOT NULL,
    resp_school_key integer NOT NULL,
    student_key integer NOT NULL,
    student_grade character varying(12) NOT NULL,
    sum_scale_score integer,
    sum_csem integer,
    sum_perf_lvl smallint,
    sum_read_scale_score integer,
    sum_read_csem integer,
    sum_write_scale_score integer,
    sum_write_csem integer,
    subclaim1_category smallint,
    subclaim2_category smallint,
    subclaim3_category smallint,
    subclaim4_category smallint,
    subclaim5_category smallint,
    subclaim6_category smallint,
    state_growth_percent numeric(5,2),
    district_growth_percent numeric(5,2),
    parcc_growth_percent numeric(5,2),
    multirecord_flag character(2) DEFAULT (-1) NOT NULL,
    result_type character varying(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    record_type character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    reported_score_flag character(1) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
    report_suppression_code character(2),
    report_suppression_action smallint,
    reported_roster_flag character varying(2) DEFAULT 'NA'::character varying NOT NULL,
    include_in_parcc boolean DEFAULT false NOT NULL,
    include_in_state boolean DEFAULT false NOT NULL,
    include_in_district boolean DEFAULT false NOT NULL,
    include_in_school boolean DEFAULT false NOT NULL,
    include_in_isr boolean DEFAULT false NOT NULL,
    include_in_roster character(1) DEFAULT 'N'::bpchar NOT NULL,
    create_date date DEFAULT ('now'::text)::date NOT NULL,
    me_flag character varying(8),
    form_category character varying(2),
    pba_test_uuid character varying(36),
    eoy_test_uuid character varying(36),
    sum_score_rec_uuid character varying(36),
    eoy_test_school_key integer,
    pba_test_school_key integer,
    pba_total_items integer,
    eoy_total_items integer,
    pba_attempt_flag character(1),
    eoy_attempt_flag character(1),
    pba_total_items_attempt integer,
    eoy_total_items_attempt integer,
    pba_total_items_unit1 integer,
    eoy_total_items_unit1 integer,
    pba_total_items_unit2 integer,
    eoy_total_items_unit2 integer,
    pba_total_items_unit3 integer,
    eoy_total_items_unit3 integer,
    pba_total_items_unit4 integer,
    eoy_total_items_unit4 integer,
    pba_total_items_unit5 integer,
    eoy_total_items_unit5 integer,
    pba_unit1_items_attempt integer,
    eoy_unit1_items_attempt integer,
    pba_unit2_items_attempt integer,
    eoy_unit2_items_attempt integer,
    pba_unit3_items_attempt integer,
    eoy_unit3_items_attempt integer,
    pba_unit4_items_attempt integer,
    eoy_unit4_items_attempt integer,
    pba_unit5_items_attempt integer,
    eoy_unit5_items_attempt integer,
    pba_not_tested_reason character varying(150),
    eoy_not_tested_reason character varying(150),
    pba_void_reason character varying(150),
    eoy_void_reason character varying(150),
    pba_test_key integer,
    eoy_test_key integer,
    summative_population integer
);


ALTER TABLE public.fact_sum OWNER TO edware;

--
-- Name: COLUMN fact_sum.poy_key; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.poy_key IS 'YEAR || 01 OR YEAR || 02';


--
-- Name: COLUMN fact_sum.student_grade; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';


--
-- Name: COLUMN fact_sum.sum_scale_score; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- Name: COLUMN fact_sum.sum_csem; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_csem IS 'Summative CSEM';


--
-- Name: COLUMN fact_sum.sum_perf_lvl; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level';


--
-- Name: COLUMN fact_sum.sum_read_scale_score; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score';


--
-- Name: COLUMN fact_sum.sum_read_csem; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_csem IS 'Summative Reading CSEM';


--
-- Name: COLUMN fact_sum.sum_write_scale_score; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- Name: COLUMN fact_sum.sum_write_csem; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_csem IS 'Summative Writing CSEM';


--
-- Name: COLUMN fact_sum.subclaim1_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim2_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim3_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim4_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim5_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim6_category; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.state_growth_percent; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse';


--
-- Name: COLUMN fact_sum.district_growth_percent; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District';


--
-- Name: COLUMN fact_sum.parcc_growth_percent; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';


--
-- Name: COLUMN fact_sum.multirecord_flag; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank';


--
-- Name: COLUMN fact_sum.result_type; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.result_type IS 'Summative, PBA, EOY';


--
-- Name: COLUMN fact_sum.record_type; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.record_type IS ' Summative Score, Single Component, No Component';


--
-- Name: COLUMN fact_sum.reported_score_flag; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.reported_score_flag IS 'Y,N,balnk';


--
-- Name: COLUMN fact_sum.reported_roster_flag; Type: COMMENT; Schema: public; Owner: edware
--

COMMENT ON COLUMN fact_sum.reported_roster_flag IS 'Y, N, NA';


--
-- Name: fact_sum_resp_school_key_seq; Type: SEQUENCE; Schema: public; Owner: edware
--

CREATE SEQUENCE fact_sum_resp_school_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fact_sum_resp_school_key_seq OWNER TO edware;

--
-- Name: fact_sum_resp_school_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edware
--

ALTER SEQUENCE fact_sum_resp_school_key_seq OWNED BY fact_sum.resp_school_key;


--
-- Name: school_key; Type: DEFAULT; Schema: public; Owner: edware
--

ALTER TABLE ONLY dim_institution ALTER COLUMN school_key SET DEFAULT nextval('dim_institution_school_key_seq'::regclass);


--
-- Name: resp_school_key; Type: DEFAULT; Schema: public; Owner: edware
--

ALTER TABLE ONLY fact_sum ALTER COLUMN resp_school_key SET DEFAULT nextval('fact_sum_resp_school_key_seq'::regclass);


--
-- Name: dim_ela_accomod_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_accomod
    ADD CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (rec_key);


--
-- Name: dim_institution_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_institution
    ADD CONSTRAINT dim_institution_pk PRIMARY KEY (school_key);


--
-- Name: dim_opt_state_data_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_opt_state_data
    ADD CONSTRAINT dim_opt_state_data_pk PRIMARY KEY (rec_key);


--
-- Name: dim_poy_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_poy
    ADD CONSTRAINT dim_poy_pk PRIMARY KEY (poy_key);


--
-- Name: dim_student_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_student
    ADD CONSTRAINT dim_student_pk PRIMARY KEY (student_key);


--
-- Name: dim_test_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_test
    ADD CONSTRAINT dim_test_pk PRIMARY KEY (test_form_key);


--
-- Name: fact_sum_pk; Type: CONSTRAINT; Schema: public; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_pk PRIMARY KEY (rec_id);


--
-- Name: eoy_attempt_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX eoy_attempt_idx ON fact_sum USING btree (eoy_attempt_flag);


--
-- Name: eoy_test_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX eoy_test_idx ON fact_sum USING btree (eoy_test_key);


--
-- Name: eoy_test_school_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX eoy_test_school_idx ON fact_sum USING btree (eoy_test_school_key);


--
-- Name: fact_sum_ix1; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix1 ON fact_sum USING btree (rec_key);


--
-- Name: fact_sum_ix2; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix2 ON fact_sum USING btree (poy_key);


--
-- Name: fact_sum_ix8; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix8 ON fact_sum USING btree (resp_school_key);


--
-- Name: fact_sum_ix9; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix9 ON fact_sum USING btree (student_key);


--
-- Name: idx_dim_institution_lookup; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_institution_lookup ON dim_institution USING btree (school_id);


--
-- Name: idx_dim_poy_lookup; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_poy_lookup ON dim_poy USING btree (poy, school_year);


--
-- Name: idx_dim_student_lookup; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_student_lookup ON dim_student USING btree (student_parcc_id);


--
-- Name: idx_dim_test_lookup; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_test_lookup ON dim_test USING btree (test_code, test_category);


--
-- Name: me_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX me_idx ON fact_sum USING btree (me_flag);


--
-- Name: multirecord_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX multirecord_idx ON fact_sum USING btree (multirecord_flag);


--
-- Name: pba_attempt_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX pba_attempt_idx ON fact_sum USING btree (pba_attempt_flag);


--
-- Name: pba_test_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX pba_test_idx ON fact_sum USING btree (pba_test_key);


--
-- Name: pba_test_school_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX pba_test_school_idx ON fact_sum USING btree (pba_test_school_key);


--
-- Name: sum_scale_score_idx; Type: INDEX; Schema: public; Owner: edware; Tablespace: 
--

CREATE INDEX sum_scale_score_idx ON fact_sum USING btree (sum_scale_score);


--
-- Name: fact_sum_fk4; Type: FK CONSTRAINT; Schema: public; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk4 FOREIGN KEY (student_key) REFERENCES dim_student(student_key);


--
-- Name: fact_sum_fk6; Type: FK CONSTRAINT; Schema: public; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk6 FOREIGN KEY (poy_key) REFERENCES dim_poy(poy_key);


--
-- Name: fact_sum_fk8; Type: FK CONSTRAINT; Schema: public; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk8 FOREIGN KEY (resp_school_key) REFERENCES dim_institution(school_key);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

