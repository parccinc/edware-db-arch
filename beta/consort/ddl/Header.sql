--
-- ER/Studio Data Architect 10.0 SQL Code Generation
-- Project :      Parcc_OLAP_Consort.dm1
--
-- Date Created : Tuesday, May 05, 2015 13:48:55
-- Target DBMS : PostgreSQL 9.x
--

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk10
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk3
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk4
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk5
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk6
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk9
;

DROP TABLE dim_poy
;
DROP TABLE dim_report_roster
;
DROP TABLE dim_report_suppression
;
DROP TABLE dim_student
;
DROP TABLE dim_suppression_action
;
DROP TABLE dim_test
;
DROP TABLE fact_sum
;
