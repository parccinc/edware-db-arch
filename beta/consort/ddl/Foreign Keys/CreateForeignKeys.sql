-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk10 
    FOREIGN KEY (suppression_action_key)
    REFERENCES dim_suppression_action(suppression_action_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk3 
    FOREIGN KEY (report_suppresssion_key)
    REFERENCES dim_report_suppression(report_suppresssion_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk4 
    FOREIGN KEY (rec_key)
    REFERENCES dim_student(rec_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk5 
    FOREIGN KEY (test_form_key)
    REFERENCES dim_test(test_form_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk6 
    FOREIGN KEY (poy_key)
    REFERENCES dim_poy(poy_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk9 
    FOREIGN KEY (reported_roster_key)
    REFERENCES dim_report_roster(reported_roster_key)
;


