DROP TABLE dim_report_roster
;
-- 
-- TABLE: dim_report_roster 
--

CREATE TABLE dim_report_roster(
    reported_roster_key     int2          DEFAULT -1 NOT NULL,
    reported_roster_flag    varchar(2)    DEFAULT 'NA' NOT NULL,
    create_date             date          DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_report_roster.reported_roster_key IS '1 - y; 0 - N, -1 = null'
;
COMMENT ON COLUMN dim_report_roster.reported_roster_flag IS 'Y, N, NA'
;

-- 
-- TABLE: dim_report_roster 
--

ALTER TABLE dim_report_roster ADD 
    CONSTRAINT dim_report_roster_pk PRIMARY KEY (reported_roster_key)
;

