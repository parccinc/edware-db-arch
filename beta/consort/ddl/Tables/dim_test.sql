DROP TABLE dim_test
;
-- 
-- TABLE: dim_test 
--

CREATE TABLE dim_test(
    test_form_key    int4           NOT NULL,
    form_id          varchar(14)    DEFAULT 'NA' NOT NULL,
    test_category    varchar(2)     DEFAULT 'NA' NOT NULL,
    test_subject     varchar(35)    DEFAULT 'UNKNOWN' NOT NULL,
    test_code        varchar(20)    NOT NULL,
    create_date      date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_test.test_form_key IS 'NA for summative different for different test_codes'
;
COMMENT ON COLUMN dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative'
;
COMMENT ON COLUMN dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component '
;
COMMENT ON COLUMN dim_test.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON TABLE dim_test IS 'removed course, course_key fields'
;

-- 
-- TABLE: dim_test 
--

ALTER TABLE dim_test ADD 
    CONSTRAINT dim_test_pk PRIMARY KEY (test_form_key)
;

