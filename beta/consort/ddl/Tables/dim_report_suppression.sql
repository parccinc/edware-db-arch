DROP TABLE dim_report_suppression
;
-- 
-- TABLE: dim_report_suppression 
--

CREATE TABLE dim_report_suppression(
    report_suppresssion_key    int2       DEFAULT -1 NOT NULL,
    report_suppression_code    char(2),
    create_date                date       DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_report_suppression.report_suppresssion_key IS 'Only allowed for Record Type 01. Error and reject record if invalid value. Cross  Validation: If Report Suppression Action is non-blank then Report Suppression Code  must be non-blank, else error. 1 - 01 = Report Suppression Code reason 1; 2 - 02 = Report Suppression Code reason 2; 3 - 03 = Report Suppression Code reason 3; 4 - 04 = Report Suppression Code reason 4; 5 - 05 = Report Suppression Code reason 5; 6 - 06 = Report Suppression Code reason 6; 7 - 07 = Report Suppression Code reason 7; 8 - 08 = Report Suppression Code reason 8; 9 - 09 = Report Suppression Code reason 9; 10 - 10 = Report Suppression Code reason 10; -1 - Blank for all other cases'
;

-- 
-- TABLE: dim_report_suppression 
--

ALTER TABLE dim_report_suppression ADD 
    CONSTRAINT dim_report_suppression_pk PRIMARY KEY (report_suppresssion_key)
;

