DROP TABLE dim_suppression_action
;
-- 
-- TABLE: dim_suppression_action 
--

CREATE TABLE dim_suppression_action(
    suppression_action_key    int2           DEFAULT -1 NOT NULL,
    suppression_action        varchar(60)    NOT NULL,
    create_date               date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_suppression_action.suppression_action_key IS 'Only allowed for Record Type 01; Error and reject record if invalid value.; Cross  Validation: If Report Suppression Code is non-blank then Report Suppression Action must be non-blank, else error. Record field that can be updated and re-imported ; 1 - 01 = No student report and not aggregated;2 - 02 = Student report with scores, but not aggregated;3 - 03 = Student report with no scores, but not aggregated; 4 - 04 = No student report, but is aggregated; -1 - every other Scenario'
;
COMMENT ON COLUMN dim_suppression_action.suppression_action IS 'No student report and not aggregated; Student report with scores, but not aggregated; Student report with no scores, but not aggregated; No student report, but is aggregated; NA'
;

-- 
-- TABLE: dim_suppression_action 
--

ALTER TABLE dim_suppression_action ADD 
    CONSTRAINT dim_suppression_action_pk PRIMARY KEY (suppression_action_key)
;

