--
-- ER/Studio Data Architect 10.0 SQL Code Generation
-- Project :      Parcc_OLAP_Consort.dm1
--
-- Date Created : Tuesday, May 05, 2015 13:46:50
-- Target DBMS : PostgreSQL 9.x
--

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk10
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk3
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk4
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk5
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk6
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk9
;

DROP TABLE dim_poy
;
DROP TABLE dim_report_roster
;
DROP TABLE dim_report_suppression
;
DROP TABLE dim_student
;
DROP TABLE dim_suppression_action
;
DROP TABLE dim_test
;
DROP TABLE fact_sum
;
-- 
-- TABLE: dim_poy 
--

CREATE TABLE dim_poy(
    poy_key        numeric(1, 0)    DEFAULT -1 NOT NULL,
    poy            varchar(20)      NOT NULL,
    end_year       numeric(4, 0)    NOT NULL,
    school_year    char(9)          NOT NULL,
    create_date    date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_poy.poy_key IS 'YEAR || 01 OR YEAR || 02'
;
COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring'
;
COMMENT ON COLUMN dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014'
;

-- 
-- TABLE: dim_report_roster 
--

CREATE TABLE dim_report_roster(
    reported_roster_key     int2          DEFAULT -1 NOT NULL,
    reported_roster_flag    varchar(2)    DEFAULT 'NA' NOT NULL,
    create_date             date          DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_report_roster.reported_roster_key IS '1 - y; 0 - N, -1 = null'
;
COMMENT ON COLUMN dim_report_roster.reported_roster_flag IS 'Y, N, NA'
;

-- 
-- TABLE: dim_report_suppression 
--

CREATE TABLE dim_report_suppression(
    report_suppresssion_key    int2       DEFAULT -1 NOT NULL,
    report_suppression_code    char(2),
    create_date                date       DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_report_suppression.report_suppresssion_key IS 'Only allowed for Record Type 01. Error and reject record if invalid value. Cross  Validation: If Report Suppression Action is non-blank then Report Suppression Code  must be non-blank, else error. 1 - 01 = Report Suppression Code reason 1; 2 - 02 = Report Suppression Code reason 2; 3 - 03 = Report Suppression Code reason 3; 4 - 04 = Report Suppression Code reason 4; 5 - 05 = Report Suppression Code reason 5; 6 - 06 = Report Suppression Code reason 6; 7 - 07 = Report Suppression Code reason 7; 8 - 08 = Report Suppression Code reason 8; 9 - 09 = Report Suppression Code reason 9; 10 - 10 = Report Suppression Code reason 10; -1 - Blank for all other cases'
;

-- 
-- TABLE: dim_student 
--

CREATE TABLE dim_student(
    rec_key               int8           NOT NULL,
    student_sex           char(1),
    ethnicity             varchar(20)    DEFAULT 'UNKNOWN' NOT NULL,
    ell                   char(1),
    lep_status            char(1),
    econo_disadvantage    char(1),
    disabil_student       char(1),
    create_date           date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).'
;
COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783'
;
COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741'
;
COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569 '
;

-- 
-- TABLE: dim_suppression_action 
--

CREATE TABLE dim_suppression_action(
    suppression_action_key    int2           DEFAULT -1 NOT NULL,
    suppression_action        varchar(60)    NOT NULL,
    create_date               date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_suppression_action.suppression_action_key IS 'Only allowed for Record Type 01; Error and reject record if invalid value.; Cross  Validation: If Report Suppression Code is non-blank then Report Suppression Action must be non-blank, else error. Record field that can be updated and re-imported ; 1 - 01 = No student report and not aggregated;2 - 02 = Student report with scores, but not aggregated;3 - 03 = Student report with no scores, but not aggregated; 4 - 04 = No student report, but is aggregated; -1 - every other Scenario'
;
COMMENT ON COLUMN dim_suppression_action.suppression_action IS 'No student report and not aggregated; Student report with scores, but not aggregated; Student report with no scores, but not aggregated; No student report, but is aggregated; NA'
;

-- 
-- TABLE: dim_test 
--

CREATE TABLE dim_test(
    test_form_key    int4           NOT NULL,
    form_id          varchar(14)    DEFAULT 'NA' NOT NULL,
    test_category    varchar(2)     DEFAULT 'NA' NOT NULL,
    test_subject     varchar(35)    DEFAULT 'UNKNOWN' NOT NULL,
    test_code        varchar(20)    NOT NULL,
    create_date      date           DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN dim_test.test_form_key IS 'NA for summative different for different test_codes'
;
COMMENT ON COLUMN dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative'
;
COMMENT ON COLUMN dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component '
;
COMMENT ON COLUMN dim_test.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON TABLE dim_test IS 'removed course, course_key fields'
;

-- 
-- TABLE: fact_sum 
--

CREATE TABLE fact_sum(
    rec_id                     int8             NOT NULL,
    rec_key                    int8             NOT NULL,
    poy_key                    numeric(1, 0)    DEFAULT -1 NOT NULL,
    report_suppresssion_key    int2             DEFAULT -1 NOT NULL,
    suppression_action_key     int2             DEFAULT -1 NOT NULL,
    reported_roster_key        int2             DEFAULT -1 NOT NULL,
    test_form_key              int4             NOT NULL,
    student_grade              varchar(12)      NOT NULL,
    total_items                numeric(3, 0),
    total_items_attempt        numeric(3, 0),
    total_items_unit1          numeric(2, 0),
    unit1_items_attempt        numeric(2, 0),
    total_items_unit2          numeric(2, 0),
    unit2_items_attempt        numeric(2, 0),
    total_items_unit3          numeric(2, 0),
    unit3_items_attempt        numeric(2, 0),
    total_items_unit4          numeric(2, 0),
    unit4_items_attempt        numeric(2, 0),
    total_items_unit5          numeric(2, 0),
    unit5_items_attempt        numeric(2, 0),
    not_tested_reason          numeric(2, 0),
    void_reason                numeric(2, 0),
    sum_scale_score            int4,
    sum_csem                   int4,
    sum_perf_lvl               int2,
    sum_read_scale_score       int4,
    sum_read_csem              int4,
    sum_write_scale_score      int4,
    sum_write_csem             int4,
    subclaim1_category         int2,
    subclaim2_category         int2,
    subclaim3_category         int2,
    subclaim4_category         int2,
    subclaim5_category         int2,
    subclaim6_category         int2,
    state_growth_percent       numeric(5, 2),
    district_growth_percent    numeric(5, 2),
    parcc_growth_percent       numeric(5, 2),
    attempt_flag               char(1)          DEFAULT -1 NOT NULL,
    multirecord_flag           char(1)          DEFAULT -1 NOT NULL,
    test_uuid                  varchar(36)      NOT NULL,
    result_type_key            int2             DEFAULT -1 NOT NULL,
    result_type                varchar(10)      NOT NULL,
    record_type_key            int2             DEFAULT 1 NOT NULL,
    record_type                varchar(20)      NOT NULL,
    reported_score_flag_key    int2             DEFAULT -1 NOT NULL,
    reported_score_flag        char(1)          NOT NULL,
    create_date                date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN fact_sum.poy_key IS 'YEAR || 01 OR YEAR || 02'
;
COMMENT ON COLUMN fact_sum.report_suppresssion_key IS 'Only allowed for Record Type 01. Error and reject record if invalid value. Cross  Validation: If Report Suppression Action is non-blank then Report Suppression Code  must be non-blank, else error. 1 - 01 = Report Suppression Code reason 1; 2 - 02 = Report Suppression Code reason 2; 3 - 03 = Report Suppression Code reason 3; 4 - 04 = Report Suppression Code reason 4; 5 - 05 = Report Suppression Code reason 5; 6 - 06 = Report Suppression Code reason 6; 7 - 07 = Report Suppression Code reason 7; 8 - 08 = Report Suppression Code reason 8; 9 - 09 = Report Suppression Code reason 9; 10 - 10 = Report Suppression Code reason 10; -1 - Blank for all other cases'
;
COMMENT ON COLUMN fact_sum.suppression_action_key IS 'Only allowed for Record Type 01; Error and reject record if invalid value.; Cross  Validation: If Report Suppression Code is non-blank then Report Suppression Action must be non-blank, else error. Record field that can be updated and re-imported ; 1 - 01 = No student report and not aggregated;2 - 02 = Student report with scores, but not aggregated;3 - 03 = Student report with no scores, but not aggregated; 4 - 04 = No student report, but is aggregated; -1 - every other Scenario'
;
COMMENT ON COLUMN fact_sum.reported_roster_key IS '1 - y; 0 - N, -1 = null'
;
COMMENT ON COLUMN fact_sum.test_form_key IS 'NA for summative different for different test_codes'
;
COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN fact_sum.total_items IS 'Total number of questions/items on the PBA, EOY test for that form'
;
COMMENT ON COLUMN fact_sum.total_items_attempt IS 'PBA Total Test Items Attempted'
;
COMMENT ON COLUMN fact_sum.total_items_unit1 IS 'PBA Unit 1 Total Number of Items'
;
COMMENT ON COLUMN fact_sum.total_items_unit2 IS 'PBA Unit 2 Total Number of Items. Total number of items on unit 2 of the test.'
;
COMMENT ON COLUMN fact_sum.unit2_items_attempt IS 'PBA Unit 2 Number of Attempted Items. Number of unit 2 items students attempted.'
;
COMMENT ON COLUMN fact_sum.total_items_unit3 IS 'PBA Unit 3 Total Number of Items'
;
COMMENT ON COLUMN fact_sum.void_reason IS 'PBA Void PBA/EOY Score Reason'
;
COMMENT ON COLUMN fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN fact_sum.sum_csem IS 'Summative CSEM'
;
COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level'
;
COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score'
;
COMMENT ON COLUMN fact_sum.sum_read_csem IS 'Summative Reading CSEM'
;
COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN fact_sum.sum_write_csem IS 'Summative Writing CSEM'
;
COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse'
;
COMMENT ON COLUMN fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District'
;
COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC'
;
COMMENT ON COLUMN fact_sum.attempt_flag IS '1 - Y, 0 - N, -1 = blank'
;
COMMENT ON COLUMN fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank'
;
COMMENT ON COLUMN fact_sum.test_uuid IS 'PBA, eoy or fake Student Test UUID.'
;
COMMENT ON COLUMN fact_sum.result_type_key IS '1 = Summative; 2 = PBA; 3 = EOY'
;
COMMENT ON COLUMN fact_sum.result_type IS 'Summative, PBA, EOY'
;
COMMENT ON COLUMN fact_sum.record_type_key IS '1 - Summative;2 - PBA; 3 - EOY'
;
COMMENT ON COLUMN fact_sum.record_type IS ' Summative Score, Single Component, No Component'
;
COMMENT ON COLUMN fact_sum.reported_score_flag_key IS '1 - Y, 0 - N, -1 = blank; Reported Summative Score Flag'
;
COMMENT ON COLUMN fact_sum.reported_score_flag IS 'Y,N,balnk'
;

-- 
-- INDEX: fact_sum_ix1 
--

DROP INDEX fact_sum_ix1
;

CREATE INDEX fact_sum_ix1 ON fact_sum(rec_key)
;
-- 
-- INDEX: fact_sum_ix2 
--

DROP INDEX fact_sum_ix2
;

CREATE INDEX fact_sum_ix2 ON fact_sum(poy_key)
;
-- 
-- INDEX: fact_sum_ix4 
--

DROP INDEX fact_sum_ix4
;

CREATE INDEX fact_sum_ix4 ON fact_sum(report_suppresssion_key)
;
-- 
-- INDEX: fact_sum_ix3 
--

DROP INDEX fact_sum_ix3
;

CREATE INDEX fact_sum_ix3 ON fact_sum(suppression_action_key)
;
-- 
-- INDEX: fact_sum_ix5 
--

DROP INDEX fact_sum_ix5
;

CREATE INDEX fact_sum_ix5 ON fact_sum(reported_roster_key)
;
-- 
-- INDEX: fact_sum_ix6 
--

DROP INDEX fact_sum_ix6
;

CREATE INDEX fact_sum_ix6 ON fact_sum(test_form_key)
;
-- 
-- INDEX: fact_sum_fk6 
--

DROP INDEX fact_sum_fk6
;

CREATE INDEX fact_sum_fk6 ON fact_sum(poy_key)
;
-- 
-- INDEX: fact_sum_fk3 
--

DROP INDEX fact_sum_fk3
;

CREATE INDEX fact_sum_fk3 ON fact_sum(report_suppresssion_key)
;
-- 
-- INDEX: fact_sum_fk10 
--

DROP INDEX fact_sum_fk10
;

CREATE INDEX fact_sum_fk10 ON fact_sum(suppression_action_key)
;
-- 
-- INDEX: fact_sum_fk4 
--

DROP INDEX fact_sum_fk4
;

CREATE INDEX fact_sum_fk4 ON fact_sum(rec_key)
;
-- 
-- INDEX: fact_sum_fk5 
--

DROP INDEX fact_sum_fk5
;

CREATE INDEX fact_sum_fk5 ON fact_sum(test_form_key)
;
-- 
-- INDEX: fact_sum_fk9 
--

DROP INDEX fact_sum_fk9
;

CREATE INDEX fact_sum_fk9 ON fact_sum(reported_roster_key)
;
-- 
-- TABLE: dim_poy 
--

ALTER TABLE dim_poy ADD 
    CONSTRAINT dim_poy_pk PRIMARY KEY (poy_key)
;

-- 
-- TABLE: dim_report_roster 
--

ALTER TABLE dim_report_roster ADD 
    CONSTRAINT dim_report_roster_pk PRIMARY KEY (reported_roster_key)
;

-- 
-- TABLE: dim_report_suppression 
--

ALTER TABLE dim_report_suppression ADD 
    CONSTRAINT dim_report_suppression_pk PRIMARY KEY (report_suppresssion_key)
;

-- 
-- TABLE: dim_student 
--

ALTER TABLE dim_student ADD 
    CONSTRAINT dim_student_pk PRIMARY KEY (rec_key)
;

-- 
-- TABLE: dim_suppression_action 
--

ALTER TABLE dim_suppression_action ADD 
    CONSTRAINT dim_suppression_action_pk PRIMARY KEY (suppression_action_key)
;

-- 
-- TABLE: dim_test 
--

ALTER TABLE dim_test ADD 
    CONSTRAINT dim_test_pk PRIMARY KEY (test_form_key)
;

-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD 
    CONSTRAINT fact_sum_pk PRIMARY KEY (rec_id)
;

-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk10 
    FOREIGN KEY (suppression_action_key)
    REFERENCES dim_suppression_action(suppression_action_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk3 
    FOREIGN KEY (report_suppresssion_key)
    REFERENCES dim_report_suppression(report_suppresssion_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk4 
    FOREIGN KEY (rec_key)
    REFERENCES dim_student(rec_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk5 
    FOREIGN KEY (test_form_key)
    REFERENCES dim_test(test_form_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk6 
    FOREIGN KEY (poy_key)
    REFERENCES dim_poy(poy_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk9 
    FOREIGN KEY (reported_roster_key)
    REFERENCES dim_report_roster(reported_roster_key)
;


