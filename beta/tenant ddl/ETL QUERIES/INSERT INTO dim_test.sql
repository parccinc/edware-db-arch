﻿-- ****************************
-- New version 4/27/15
-- ****************************
/*INSERT INTO dim_test(
  test_form_key
, form_id
, test_category
, test_subject
, test_code
)*/
(
-- math pba
	SELECT 
	 lf.test_form_key
	, st.pba_form_id AS form_id
	, CASE WHEN st.pba_category IS NULL THEN 'NA' ELSE st.pba_category END AS test_category
	, CASE WHEN st.test_code  = 'ALG01' THEN 'Algebra I'
		WHEN st.test_code = 'ALG02' THEN 'Algebra II'
		WHEN st.test_code = 'GEO01' THEN 'Geometry'
		WHEN st.test_code = 'MAT1I' THEN 'Integrated Mathematics I'
		WHEN st.test_code = 'MAT2I' THEN 'Integrated Mathematics II'
		WHEN st.test_code = 'MAT3I' THEN 'Integrated Mathematics III'
		WHEN st.test_code LIKE 'MAT0%' THEN 'Mathematics'
	   ELSE 'UNKNOWN' END AS test_subject
	, st.test_code
	FROM lkp_form_xref lf JOIN ( SELECT * FROM rpt_math_sum 
		WHERE pba_test_uuid IS NOT NULL
		AND rec_status = 'C'
		) st  ON (lf.form_id = st.pba_form_id AND lf.test_code = st.test_code )
	UNION ALL
-- math eoy
	SELECT 
	lf.test_form_key
	, st.eoy_form_id AS form_id
	, CASE WHEN st.eoy_category IS NULL THEN 'NA' ELSE st.eoy_category END AS test_category
	, CASE WHEN st.test_code  = 'ALG01' THEN 'Algebra I'
		WHEN st.test_code = 'ALG02' THEN 'Algebra II'
		WHEN st.test_code = 'GEO01' THEN 'Geometry'
		WHEN st.test_code = 'MAT1I' THEN 'Integrated Mathematics I'
		WHEN st.test_code = 'MAT2I' THEN 'Integrated Mathematics II'
		WHEN st.test_code = 'MAT3I' THEN 'Integrated Mathematics III'
		WHEN st.test_code LIKE 'MAT0%' THEN 'Mathematics'
	   ELSE 'UNKNOWN' END AS test_subject
	, st.test_code
	FROM lkp_form_xref lf JOIN ( SELECT * FROM rpt_math_sum 
		WHERE eoy_test_uuid IS NOT NULL
		AND rec_status = 'C'
		) st ON (lf.form_id = st.eoy_form_id AND lf.test_code = st.test_code)
	UNION ALL
-- math summ
	SELECT 
	lf.test_form_key
	, 'NA' AS form_id
	, CASE WHEN st.eoy_category IS NULL THEN 'NA' ELSE st.eoy_category END AS test_category 
	, CASE WHEN st.test_code  = 'ALG01' THEN 'Algebra I'
		WHEN st.test_code = 'ALG02' THEN 'Algebra II'
		WHEN st.test_code = 'GEO01' THEN 'Geometry'
		WHEN st.test_code = 'MAT1I' THEN 'Integrated Mathematics I'
		WHEN st.test_code = 'MAT2I' THEN 'Integrated Mathematics II'
		WHEN st.test_code = 'MAT3I' THEN 'Integrated Mathematics III'
		WHEN st.test_code LIKE 'MAT0%' THEN 'Mathematics'
	     ELSE 'UNKNOWN' END AS test_subject
	, st.test_code
	FROM ( SELECT * FROM lkp_form_xref WHERE form_id = 'NA') lf JOIN 
	     ( SELECT * FROM rpt_math_sum 
		WHERE sum_score_rec_uuid IS NOT NULL
		AND rec_status = 'C'
		) st ON ( lf.test_code = st.test_code)
-- ELA pba --*****	
	UNION ALL
	SELECT 
	lf.test_form_key
	, st.pba_form_id AS form_id
	, CASE WHEN st.pba_category IS NULL THEN 'NA' ELSE st.pba_category END AS test_category
	, 'English Language Arts/Literarcy' AS test_subject
	, st.test_code
	FROM lkp_form_xref lf JOIN ( SELECT * FROM rpt_ela_sum 
		WHERE pba_test_uuid IS NOT NULL
		AND rec_status = 'C'
		) st ON (lf.form_id = st.pba_form_id AND lf.test_code = st.test_code)
-- ELA eoy --*****
	UNION ALL
	SELECT 
	lf.test_form_key
	, st.eoy_form_id AS form_id
	, CASE WHEN st.pba_category IS NULL THEN 'NA' ELSE st.pba_category END AS test_category
	, 'English Language Arts/Literarcy' AS test_subject
	, st.test_code
	FROM lkp_form_xref lf JOIN ( SELECT * FROM rpt_ela_sum 
		WHERE eoy_test_uuid IS NOT NULL
		AND rec_status = 'C'
		) st ON (lf.form_id = st.eoy_form_id AND lf.test_code = st.test_code) 
-- ELA summative --*****
	UNION 
	SELECT
	lf.test_form_key
	, 'NA' AS form_id
	, CASE WHEN st.pba_category IS NULL THEN 'NA' ELSE st.pba_category END AS test_category
	, 'English Language Arts/Literarcy' AS test_subject
	, st.test_code
	FROM ( SELECT * FROM lkp_form_xref WHERE form_id = 'NA') lf JOIN 
	      ( SELECT * FROM rpt_ela_sum 
		WHERE eoy_test_uuid IS NOT NULL
		AND rec_status = 'C' ) st ON ( lf.test_code = st.test_code)
);