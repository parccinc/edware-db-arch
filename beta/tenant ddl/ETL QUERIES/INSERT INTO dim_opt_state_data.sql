﻿INSERT INTO dim_opt_state_data ( 
rec_key
, opt_state_data1
, opt_state_data2
, opt_state_data3
, opt_state_data4
, opt_state_data5
, opt_state_data6
, opt_state_data7
, opt_state_data8 )
	-- NOTE from mjacob: these keys need to be associated with all relevant entries in the fact tables, not just '111' and '222'
	(SELECT
	  CAST( '555' || st.rec_id AS bigint) AS rec_id
	, st.opt_state_data1
	, st.opt_state_data2
	, st.opt_state_data3
	, st.opt_state_data4
	, st.opt_state_data5
	, st.opt_state_data6
	, st.opt_state_data7
	, st.opt_state_data8
	FROM ( SELECT * FROM rpt_math_sum WHERE rec_status = 'C') st
	UNION ALL
	SELECT
	  CAST( '444' || st.rec_id AS bigint) AS rec_id
	, st.opt_state_data1
	, st.opt_state_data2
	, st.opt_state_data3
	, st.opt_state_data4
	, st.opt_state_data5
	, st.opt_state_data6
	, st.opt_state_data7
	, st.opt_state_data8
	FROM ( SELECT * FROM rpt_ela_sum WHERE rec_status = 'C') st
	);
