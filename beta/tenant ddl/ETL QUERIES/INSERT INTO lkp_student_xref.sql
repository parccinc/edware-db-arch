-- *************************************
-- 4/30/2015
-- *************************************
INSERT INTO lkp_student_xref(
, student_parcc_id
, ell
, lep_status
, gift_talent
, migrant_status
, econo_disadvantage
, disabil_student
, primary_disabil_type
)
SELECT student_key
, student_parcc_id
, ell
, lep_status
, gift_talent
, migrant_status
, econo_disadvantage
, disabil_student
, primary_disabil_type
FROM vw_src_student_xref;
