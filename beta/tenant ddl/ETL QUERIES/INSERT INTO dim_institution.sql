INSERT INTO dim_institution(
  school_key
,dist_name
,dist_id
,school_name
,school_id
 )
(
--empty record
	SELECT -1 AS school_key
	, 'NA' AS dist_name
	, 'NA' AS dist_id
	, 'NA' AS school_name
	, 'NA' AS school_id
	UNION ALL
-- MATH eoy test institution
	SELECT 
	, ls.school_key
	, CASE WHEN st.eoy_test_dist_name IS NULL THEN 'NA' ELSE st.eoy_test_school_id END AS dist_name
	, CASE WHEN st.eoy_test_dist_id IS NULL THEN 'NA' ELSE st.eoy_test_dist_id END AS dist_id
	, st.eoy_test_school_name AS school_name
	, CASE WHEN st.eoy_test_school_id IS NULL THEN 'NA' ELSE st.eoy_test_school_id END AS school_id
	FROM lkp_school_xref ls JOIN (SELECT * FROM rpt_math_sum WHERE st.rec_status = 'C') st
	ON (ls.school_id = st.eoy_test_school_id)
	UNION ALL
-- ELA eoy test institution
	SELECT 
	, ls.school_key
	, CASE WHEN st.eoy_test_dist_name IS NULL THEN 'NA' ELSE st.eoy_test_school_id END AS dist_name
	, CASE WHEN st.eoy_test_dist_id IS NULL THEN 'NA' ELSE st.eoy_test_dist_id END AS dist_id
	, CASE WHEN st.eoy_test_school_name IS NULL THEN 'NA' ELSE st.eoy_test_school_name END AS school_name
	, CASE WHEN st.eoy_test_school_id IS NULL THEN 'NA' ELSE st.eoy_test_school_id END AS school_id
	FROM lkp_school_xref ls JOIN (SELECT * FROM rpt_ela_sum WHERE st.rec_status = 'C') st
	ON (ls.school_id = st.eoy_test_school_id)
	UNION ALL
-- MATH pba test institution
	SELECT 
	, ls.school_key
	, CASE WHEN st.pba_test_dist_name IS NULL THEN 'NA' ELSE st.pba_test_dist_name END AS dist_name
	, CASE WHEN st.pba_test_dist_id IS NULL THEN 'NA' ELSE st.pba_test_dist_id END AS dist_id
	, CASE WHEN st.pba_test_school_name IS NULL THEN 'NA' ELSE st.pba_test_school_name END AS school_name
	, CASE WHEN st.pba_test_school_id IS NULL THEN 'NA' ELSE st.pba_test_school_id END AS school_id
	FROM lkp_school_xref ls JOIN (SELECT * FROM rpt_math_sum WHERE st.rec_status = 'C') st
	ON (ls.school_id = st.pba_test_school_id)
	UNION ALL
-- ELA pba test institution
	SELECT 
	, ls.school_key
	, CASE WHEN st.pba_test_dist_name IS NULL THEN 'NA' ELSE st.pba_test_dist_name END AS dist_name
	, CASE WHEN st.pba_test_dist_id IS NULL THEN 'NA' ELSE st.pba_test_dist_id END AS dist_id
	, CASE WHEN st.pba_test_school_name IS NULL THEN 'NA' ELSE st.pba_test_school_name END AS school_name
	, CASE WHEN st.pba_test_school_id IS NULL THEN 'NA' ELSE st.pba_test_school_id END AS school_id
	FROM lkp_school_xref ls JOIN (SELECT * FROM rpt_ela_sum WHERE st.rec_status = 'C') st
	ON (ls.school_id = st.pba_test_school_id)
	UNION ALL
-- MATH resp institution
	SELECT 
	, ls.school_key
	, CASE WHEN st.resp_dist_name IS NULL THEN 'NA' ELSE st.resp_dist_name END AS dist_name
	, CASE WHEN st.resp_dist_id IS NULL THEN 'NA' ELSE st.resp_dist_id END AS dist_id
	, CASE WHEN st.resp_school_name IS NULL THEN 'NA' ELSE st.resp_school_name END AS school_name
	, CASE WHEN st.resp_school_id IS NULL THEN 'NA' ELSE st.resp_school_id END AS school_id
	FROM lkp_school_xref ls JOIN (SELECT * FROM rpt_math_sum WHERE st.rec_status = 'C') st
	ON (ls.school_id = st.resp_school_id)
	UNION
-- ELA resp institution
	SELECT 
	, ls.school_key
	, CASE WHEN st.resp_dist_name IS NULL THEN 'NA' ELSE st.resp_dist_name END AS dist_name
	, CASE WHEN st.resp_dist_id IS NULL THEN 'NA' ELSE st.resp_dist_id END AS dist_id
	, CASE WHEN st.resp_school_name IS NULL THEN 'NA' ELSE st.resp_school_name END AS school_name
	, CASE WHEN st.resp_school_id IS NULL THEN 'NA' ELSE st.resp_school_id END AS school_id
	FROM lkp_school_xref ls JOIN (SELECT * FROM rpt_ela_sum WHERE st.rec_status = 'C') st
	ON (ls.school_id = st.resp_school_id)
);

