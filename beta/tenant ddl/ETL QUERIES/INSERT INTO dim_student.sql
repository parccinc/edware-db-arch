﻿-- if UNION is slow might need to do merge from each source seperately
-- ********************
-- 4/27/2015 new version
-- ********************
INSERT INTO dim_student (
 student_key
, par_student_key
, student_sex
, ethnicity
, ethnicity_key
, ethn_hisp_latino
, ethn_indian_alaska
, ethn_asian
, ethn_black
, ethn_hawai
, ethn_white
, ethn_filler
, ell
, lep_status
, gift_talent
, migrant_status
, econo_disadvantage
, disabil_student
, primary_disabil_type
, student_parcc_id
, student_state_id
, student_local_id
, student_name
, student_dob
)
(
SELECT ls.student_key
, ls.par_student_key
, st.student_sex
, CASE WHEN st.student_sex ='M' THEN 1 WHEN st.student_sex ='F' THEN 2 ELSE -1 END AS student_sex_key
, st.ethnicity
, CASE WHEN st.ethnicity = '01' THEN 'American Indian or Alaska Native'
       WHEN st.ethnicity = '02' THEN 'Asian'
       WHEN st.ethnicity = '03' THEN 'Black or African American'
       WHEN st.ethnicity = '04' THEN 'Hispanic or Latino'
       WHEN st.ethnicity = '05' THEN 'White'
       WHEN st.ethnicity = '06' THEN 'Native Hawaiian or other Pacific Islander'
       WHEN st.ethnicity = '07' THEN 'Two or more races'
  ELSE 'NA' END AS ethnicity
, CASE WHEN st.ethnicity = '01' THEN 	1
       WHEN st.ethnicity = '02' THEN 	2
       WHEN st.ethnicity = '03' THEN 	3
       WHEN st.ethnicity = '04' THEN 	4
       WHEN st.ethnicity = '05' THEN 	5
       WHEN st.ethnicity = '06' THEN 	6
       WHEN st.ethnicity = '07' THEN 	7
   ELSE -1 END AS ethnicity_key
, st.ethn_hisp_latino
, st.ethn_indian_alaska
, st.ethn_asian
, st.ethn_black
, st.ethn_hawai
, st.ethn_white
, st.ethn_filler
, st.ell
, st.lep_status
, st.gift_talent
, st.migrant_status
, st.econo_disadvantage
, st.disabil_student
, st.primary_disabil_type
, st.student_parcc_id
, st.student_state_id
, st.student_local_id
, st.student_last_name ||', '|| st.student_first_name
|| (CASE WHEN st.student_middle_name IS NOT NULL THEN ', '|| st.student_middle_name ELSE '' END ) AS student_name
, st.student_dob
FROM lkp_student_xref ls JOIN (SELECT * FROM rpt_math_sum WHERE rec_status = 'C') st
ON (ls.student_parcc_id = st.student_parcc_id AND
  ls.ell = st.ell AND
  ls.lep_status = st.lep_status AND
  ls.gift_talent = st.gift_talent AND
  ls.migrant_status = st.migrant_status AND
  ls.econo_disadvantage = st.econo_disadvantage AND
  ls.disabil_student = st.disabil_student AND
  ls.primary_disabil_type = st.primary_disabil_type)
UNION
SELECT ls.student_key
, ls.par_student_key
, st.student_sex
, CASE WHEN st.student_sex ='M' THEN 1 WHEN st.student_sex ='F' THEN 2 ELSE -1 END AS student_sex_key
, st.ethnicity
, CASE WHEN st.ethnicity = '01' THEN 'American Indian or Alaska Native'
       WHEN st.ethnicity = '02' THEN 'Asian'
       WHEN st.ethnicity = '03' THEN 'Black or African American'
       WHEN st.ethnicity = '04' THEN 'Hispanic or Latino'
       WHEN st.ethnicity = '05' THEN 'White'
       WHEN st.ethnicity = '06' THEN 'Native Hawaiian or other Pacific Islander'
       WHEN st.ethnicity = '07' THEN 'Two or more races'
  ELSE 'NA' END AS ethnicity
, CASE WHEN st.ethnicity = '01' THEN 	1
       WHEN st.ethnicity = '02' THEN 	2
       WHEN st.ethnicity = '03' THEN 	3
       WHEN st.ethnicity = '04' THEN 	4
       WHEN st.ethnicity = '05' THEN 	5
       WHEN st.ethnicity = '06' THEN 	6
       WHEN st.ethnicity = '07' THEN 	7
   ELSE -1 END AS ethnicity_key
, st.ethn_hisp_latino
, st.ethn_indian_alaska
, st.ethn_asian
, st.ethn_black
, st.ethn_hawai
, st.ethn_white
, st.ethn_filler
, st.ell
, st.lep_status
, st.gift_talent
, st.migrant_status
, st.econo_disadvantage
, st.disabil_student
, st.primary_disabil_type
, st.student_parcc_id
, st.student_state_id
, st.student_local_id
, st.student_last_name ||', '|| st.student_first_name
|| (CASE WHEN st.student_middle_name IS NOT NULL THEN ', '|| st.student_middle_name ELSE '' END ) AS student_name
, st.student_dob
FROM lkp_student_xref ls JOIN (SELECT * FROM rpt_ela_sum WHERE rec_status = 'C') st
ON (ls.student_parcc_id = st.student_parcc_id AND
   ls.ell = st.ell AND
   ls.lep_status = st.lep_status AND
   ls.gift_talent = st.gift_talent AND
   ls.migrant_status = st.migrant_status AND
   ls.econo_disadvantage = st.econo_disadvantage AND
   ls.disabil_student = st.disabil_student AND
   ls.primary_disabil_type = st.primary_disabil_type)
);