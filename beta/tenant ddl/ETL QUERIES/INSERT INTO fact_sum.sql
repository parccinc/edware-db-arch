-- POPULATE fact_sum
-- ***************************
INSERT INTO fact_sum(
 rec_id
, rec_key
, poy_key
, report_suppresssion_key
, suppression_action_key
, reported_roster_key
, test_form_key
, test_school_key
, resp_school_key
, student_key
, grade_key
, student_grade
, total_items
, total_items_attempt
, total_items_unit1
, unit1_items_attempt
, total_items_unit2
, unit2_items_attempt
, total_items_unit3
, unit3_items_attempt
, total_items_unit4
, unit4_items_attempt
, total_items_unit5
, unit5_items_attempt
, not_tested_reason
, void_reason
, raw_score
, sum_scale_score
, sum_csem
, sum_perf_lvl
, sum_read_scale_score
, sum_read_csem
, sum_write_scale_score
, sum_write_csem
, subclaim1_category
, subclaim2_category
, subclaim3_category
, subclaim4_category
, subclaim5_category
, subclaim6_category
, state_growth_percent
, district_growth_percent
, parcc_growth_percent
, attempt_flag
, multirecord_flag
, pba_test_uuid
, result_type_key
, result_type
, record_type_key
, record_type
, reported_score_flag_key
, reported_score_flag 
)
-- ELA PBA
(
SELECT
CAST( '232' || st.rec_id AS bigint) AS rec_id
, CAST( '444' || st.rec_id AS bigint) AS rec_key
, CASE WHEN UPPER(st.poy) = 'FALL'   THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '01' AS int4)
       WHEN UPPER(st.poy) = 'SPRING' THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '02' AS int4)
    ELSE -1 END AS poy_key
, CASE WHEN st.report_suppression_code IS NULL THEN -1
    ELSE CAST(st.report_suppression_code AS int2) END AS report_suppression_key
, CASE WHEN st.report_suppression_action IS NULL THEN -1
    ELSE CAST(st.report_suppression_action AS int2) END AS suppression_action_key
, CASE WHEN st.is_roster_reported = 'Y' THEN 1 
 WHEN st.is_roster_reported = 'N' THEN 0  ELSE -1 END AS reported_roster_key 
, fx.test_form_key
, srx.school_key AS resp_school_key
, stx.school_key AS test_school_key
, sx.student_key
, CASE WHEN st.student_grade IS NULL THEN -1
      ELSE CAST ( st.student_grade AS int2) END AS grade_key
, st.student_grade
, st.pba_total_items AS total_items
, st.pba_total_items_attempt AS total_items_attempt
, st.pba_total_items_unit1 AS total_items_unit1
, st.pba_unit1_items_attempt AS unit1_items_attempt
, st.pba_total_items_unit2 AS total_items_unit2
, st.pba_unit2_items_attempt AS unit2_items_attempt
, st.pba_total_items_unit3 AS total_items_unit3
, st.pba_unit3_items_attempt AS unit3_items_attempt
, st.pba_total_items_unit4 AS total_items_unit4
, st.pba_unit4_items_attempt AS unit4_items_attempt
, st.pba_total_items_unit5 AS total_items_unit5
, st.pba_unit5_items_attempt AS unit5_items_attempt
, CASE WHEN st.pba_not_tested_reason IS NULL THEN -1
      ELSE CAST(st.pba_not_tested_reason AS int2) END AS not_tested_reason
, CASE WHEN st.pba_void_reason IS NULL THEN -1
      ELSE CAST(st.pba_void_reason AS int2) END AS void_reason
, CAST(NULL AS int2) AS sum_scale_score
, CAST(NULL AS int2) AS sum_csem
, CAST(NULL AS int2) AS sum_perf_lvl
, CAST(NULL AS int2) AS sum_read_scale_score
, CAST(NULL AS int2) AS sum_read_csem
, CAST(NULL AS int2) AS sum_write_scale_score
, CAST(NULL AS int2) AS sum_write_csem
, CAST(NULL AS int2) AS subclaim1_category
, CAST(NULL AS int2) AS subclaim2_category
, CAST(NULL AS int2) AS subclaim3_category
, CAST(NULL AS int2) AS subclaim4_category
, CAST(NULL AS int2) AS subclaim5_category
, CAST(NULL AS int2) AS subclaim6_category
, CAST(NULL AS int2) AS state_growth_percent
, CAST(NULL AS int2) AS district_growth_percent
, CAST(NULL AS int2) AS parcc_growth_percent
, st.pba_attempt_flag AS attempt_flag
, st.is_multi_rec AS multirecord_flag
, st.pba_test_uuid AS test_uuid
, 2 AS result_type_key
, 'PBA' AS result_type
, CAST(st.record_type AS int2) AS rec_type_key
, CASE WHEN st.record_type = '01' THEN 'Summative Score'
 WHEN st.record_type = '02' THEN 'Single Component'
 ELSE 'No Component' END AS record_type
, CASE WHEN st.is_reported_summative = 'Y' THEN 1
 WHEN st.is_reported_summative = 'N' THEN 0
  ELSE -1 END AS reported_score_flag_key
, st.is_reported_summative AS reported_score_flag
FROM lkp_form_xref fx JOIN ( SELECT * FROM rpt_ela_sum WHERE pba_test_uuid IS NOT NULL AND rec_status = 'C') st on ( fx.form_id = st.pba_form_id)
JOIN lkp_school_xref stx ON (stx.school_id = st.pba_test_school_id)
JOIN lkp_school_xref srx ON (srx.school_id = st.resp_school_id)
JOIN lkp_student_xref sx ON (sx.student_parcc_id = st.student_parcc_id AND
			    sx.ell = COALESCE ( st.ell, '-') AND
			    sx.lep_status = COALESCE ( st.lep_status, '-') AND
			    sx.gift_talent = COALESCE ( st.gift_talent, '-') AND
			    sx.migrant_status = COALESCE ( st.migrant_status, '-') AND
			    sx.econo_disadvantage = COALESCE ( st.econo_disadvantage, '-') AND
			    sx.disabil_student = COALESCE ( st.disabil_student, '-') AND
			    sx.primary_disabil_type = COALESCE ( st.primary_disabil_type, '-'))
UNION ALL
-- ELA EOY
SELECT
CAST( '222' || st.rec_id AS bigint) AS rec_id
, CAST( '444' || st.rec_id AS bigint) AS rec_key
, CASE WHEN UPPER(st.poy) = 'FALL'   THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '01' AS int4)
       WHEN UPPER(st.poy) = 'SPRING' THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '02' AS int4)
    ELSE -1 END AS poy_key
, CASE WHEN st.report_suppression_code IS NULL THEN -1
    ELSE CAST(st.report_suppression_code AS int2) END AS report_suppression_key
, CASE WHEN st.report_suppression_action IS NULL THEN -1
    ELSE CAST(st.report_suppression_action AS int2) END AS suppression_action_key
, CASE WHEN st.is_roster_reported = 'Y' THEN 1 
 WHEN st.is_roster_reported = 'N' THEN 0  ELSE -1 END AS reported_roster_key 
, fx.test_form_key
, srx.school_key AS resp_school_key
, stx.school_key AS test_school_key
, sx.student_key
, CASE WHEN st.student_grade IS NULL THEN -1
      ELSE CAST ( st.student_grade AS int2) END AS grade_key
, st.student_grade
, st.eoy_total_items AS total_items
, st.eoy_total_items_attempt AS total_items_attempt
, st.eoy_total_items_unit1 AS total_items_unit1
, st.eoy_unit1_items_attempt AS unit1_items_attempt
, st.eoy_total_items_unit2 AS total_items_unit2
, st.eoy_unit2_items_attempt AS unit2_items_attempt
, st.eoy_total_items_unit3 AS total_items_unit3
, st.eoy_unit3_items_attempt AS unit3_items_attempt
, st.eoy_total_items_unit4 AS total_items_unit4
, st.eoy_unit4_items_attempt AS unit4_items_attempt
, st.eoy_total_items_unit5 AS total_items_unit5
, st.eoy_unit5_items_attempt AS unit5_items_attempt
, CASE WHEN st.eoy_not_tested_reason IS NULL THEN -1
      ELSE CAST(st.eoy_not_tested_reason AS int2) END AS not_tested_reason
, CASE WHEN st.eoy_void_reason IS NULL THEN -1
      ELSE CAST(st.eoy_void_reason AS int2) END AS void_reason
, CAST(NULL AS smallint) AS sum_scale_score
, CAST(NULL AS int2) AS sum_csem
, CAST(NULL AS int2) AS sum_perf_lvl
, CAST(NULL AS int2) AS sum_read_scale_score
, CAST(NULL AS int2) AS sum_read_csem
, CAST(NULL AS int2) AS sum_write_scale_score
, CAST(NULL AS int2) AS sum_write_csem
, CAST(NULL AS int2) AS subclaim1_category
, CAST(NULL AS int2) AS subclaim2_category
, CAST(NULL AS int2) AS subclaim3_category
, CAST(NULL AS int2) AS subclaim4_category
, CAST(NULL AS int2) AS subclaim5_category
, CAST(NULL AS int2) AS subclaim6_category
, CAST(NULL AS int2) AS state_growth_percent
, CAST(NULL AS int2) AS district_growth_percent
, CAST(NULL AS int2) AS parcc_growth_percent
, st.eoy_attempt_flag AS attempt_flag
, st.is_multi_rec AS multirecord_flag
, st.eoy_test_uuid AS test_uuid
, 3 AS result_type_key
, 'EOY' AS result_type
, CAST(st.record_type AS int2) AS rec_type_key
, CASE WHEN st.record_type = '01' THEN 'Summative Score'
 WHEN st.record_type = '02' THEN 'Single Component'
 ELSE 'No Component' END AS record_type
, CASE WHEN st.is_reported_summative = 'Y' THEN 1
 WHEN st.is_reported_summative = 'N' THEN 0
  ELSE -1 END AS reported_score_flag_key
, st.is_reported_summative AS reported_score_flag
FROM lkp_form_xref fx JOIN ( SELECT * FROM rpt_ela_sum WHERE eoy_test_uuid IS NOT NULL AND rec_status = 'C') st on ( fx.form_id = st.eoy_form_id)
JOIN lkp_school_xref stx ON (stx.school_id = st.eoy_test_school_id)
JOIN lkp_school_xref srx ON (srx.school_id = st.resp_school_id)
JOIN lkp_student_xref sx ON (sx.student_parcc_id = st.student_parcc_id AND
			    sx.ell = COALESCE ( st.ell, '-') AND
			    sx.lep_status = COALESCE ( st.lep_status, '-') AND
			    sx.gift_talent = COALESCE ( st.gift_talent, '-') AND
			    sx.migrant_status = COALESCE ( st.migrant_status, '-') AND
			    sx.econo_disadvantage = COALESCE ( st.econo_disadvantage, '-') AND
			    sx.disabil_student = COALESCE ( st.disabil_student, '-') AND
			    sx.primary_disabil_type = COALESCE ( st.primary_disabil_type, '-'))
UNION ALL 
-- EOY Summative
SELECT
CAST( '212' || st.rec_id AS bigint) AS rec_id
, CAST( '444' || st.rec_id AS bigint) AS rec_key
, CASE WHEN UPPER(st.poy) = 'FALL'   THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '01' AS int4)
       WHEN UPPER(st.poy) = 'SPRING' THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '02' AS int4)
    ELSE -1 END AS poy_key
, CASE WHEN st.report_suppression_code IS NULL THEN -1
    ELSE CAST(st.report_suppression_code AS int2) END AS report_suppression_key
, CASE WHEN st.report_suppression_action IS NULL THEN -1
    ELSE CAST(st.report_suppression_action AS int2) END AS suppression_action_key
, CASE WHEN st.is_roster_reported = 'Y' THEN 1 
 WHEN st.is_roster_reported = 'N' THEN 0  ELSE -1 END AS reported_roster_key 
, fx.test_form_key
, srx.school_key AS resp_school_key
, -1 AS test_school_key
, sx.student_key
, CASE WHEN st.student_grade IS NULL THEN -1
      ELSE CAST ( st.student_grade AS int2) END AS grade_key
, st.student_grade
, CAST(NULL AS int2) AS total_items
, CAST(NULL AS int2) AS total_items_attempt
, CAST(NULL AS int2) AS total_items_unit1
, CAST(NULL AS int2) AS unit1_items_attempt
, CAST(NULL AS int2) AS total_items_unit2
, CAST(NULL AS int2) AS unit2_items_attempt
, CAST(NULL AS int2) AS total_items_unit3
, CAST(NULL AS int2) AS unit3_items_attempt
, CAST(NULL AS int2) AS total_items_unit4
, CAST(NULL AS int2) AS unit4_items_attempt
, CAST(NULL AS int2) AS total_items_unit5
, CAST(NULL AS int2) AS unit5_items_attempt
, -1 AS not_tested_reason
, -1 AS void_reason
, st.sum_scale_score AS sum_scale_score
, st.sum_csem AS sum_csem
, st.sum_perf_lvl AS sum_perf_lvl
, st.sum_read_scale_score AS sum_read_scale_score
, st.sum_read_csem AS sum_read_csem
, st.sum_write_scale_score AS sum_write_scale_score
, st.sum_write_csem AS sum_write_csem
, st.subclaim1_category AS subclaim1_category
, st.subclaim2_category AS subclaim2_category
, st.subclaim3_category AS subclaim3_category
, st.subclaim4_category AS subclaim4_category
, st.subclaim5_category AS subclaim5_category
, st.subclaim6_category AS subclaim6_category
, st.state_growth_percent AS state_growth_percent
, st.district_growth_percent AS district_growth_percent
, st.parcc_growth_percent AS parcc_growth_percent
, st.pba_attempt_flag AS attempt_flag
, st.is_multi_rec AS multirecord_flag
, st.sum_score_rec_uuid AS test_uuid
, 1 AS result_type_key
, 'Summative' AS result_type
, CAST(st.record_type AS int2) AS rec_type_key
, CASE WHEN st.record_type = '01' THEN 'Summative Score'
 WHEN st.record_type = '02' THEN 'Single Component'
 ELSE 'No Component' END AS record_type
, CASE WHEN st.is_reported_summative = 'Y' THEN 1
 WHEN st.is_reported_summative = 'N' THEN 0
  ELSE -1 END AS reported_score_flag_key
, st.is_reported_summative AS reported_score_flag
FROM ( SELECT * FROM lkp_form_xref WHERE form_id = 'NA') fx JOIN ( SELECT * FROM rpt_ela_sum WHERE sum_score_rec_uuid IS NOT NULL AND rec_status = 'C') st 
ON ( fx.test_code = st.test_code )
JOIN lkp_school_xref stx ON (stx.school_id = st.pba_test_school_id)
JOIN lkp_school_xref srx ON (srx.school_id = st.resp_school_id)
JOIN lkp_student_xref sx ON (sx.student_parcc_id = st.student_parcc_id AND
 			    sx.ell = COALESCE ( st.ell, '-') AND
 			    sx.lep_status = COALESCE ( st.lep_status, '-') AND
 			    sx.gift_talent = COALESCE ( st.gift_talent, '-') AND
 			    sx.migrant_status = COALESCE ( st.migrant_status, '-') AND
 			    sx.econo_disadvantage = COALESCE ( st.econo_disadvantage, '-') AND
 			    sx.disabil_student = COALESCE ( st.disabil_student, '-') AND
			    sx.primary_disabil_type = COALESCE ( st.primary_disabil_type, '-'))
UNION ALL
-- MATH PBA
SELECT
CAST( '131' || st.rec_id AS bigint) AS rec_id
, CAST( '555' || st.rec_id AS bigint) AS rec_key
, CASE WHEN UPPER(st.poy) = 'FALL'   THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '01' AS int4)
       WHEN UPPER(st.poy) = 'SPRING' THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '02' AS int4)
    ELSE -1 END AS poy_key
, CASE WHEN st.report_suppression_code IS NULL THEN -1
    ELSE CAST(st.report_suppression_code AS int2) END AS report_suppression_key
, CASE WHEN st.report_suppression_action IS NULL THEN -1
    ELSE CAST(st.report_suppression_action AS int2) END AS suppression_action_key
, CASE WHEN st.is_roster_reported = 'Y' THEN 1 
 WHEN st.is_roster_reported = 'N' THEN 0  ELSE -1 END AS reported_roster_key 
, fx.test_form_key
, srx.school_key AS resp_school_key
, stx.school_key AS test_school_key
, sx.student_key
, CASE WHEN st.student_grade IS NULL THEN -1
      ELSE CAST ( st.student_grade AS int2) END AS grade_key
, st.student_grade
, st.pba_total_items AS total_items
, st.pba_total_items_attempt AS total_items_attempt
, st.pba_total_items_unit1 AS total_items_unit1
, st.pba_unit1_items_attempt AS unit1_items_attempt
, st.pba_total_items_unit2 AS total_items_unit2
, st.pba_unit2_items_attempt AS unit2_items_attempt
, st.pba_total_items_unit3 AS total_items_unit3
, st.pba_unit3_items_attempt AS unit3_items_attempt
, st.pba_total_items_unit4 AS total_items_unit4
, st.pba_unit4_items_attempt AS unit4_items_attempt
, st.pba_total_items_unit5 AS total_items_unit5
, st.pba_unit5_items_attempt AS unit5_items_attempt
, CASE WHEN st.pba_not_tested_reason IS NULL THEN -1
      ELSE CAST(st.pba_not_tested_reason AS int2) END AS not_tested_reason
, CASE WHEN st.pba_void_reason IS NULL THEN -1
      ELSE CAST(st.pba_void_reason AS int2) END AS void_reason
, CAST(NULL AS int2) AS sum_scale_score
, CAST(NULL AS int2) AS sum_csem
, CAST(NULL AS int2) AS sum_perf_lvl
, CAST(NULL AS int2) AS sum_read_scale_score
, CAST(NULL AS int2) AS sum_read_csem
, CAST(NULL AS int2) AS sum_write_scale_score
, CAST(NULL AS int2) AS sum_write_csem
, CAST(NULL AS int2) AS subclaim1_category
, CAST(NULL AS int2) AS subclaim2_category
, CAST(NULL AS int2) AS subclaim3_category
, CAST(NULL AS int2) AS subclaim4_category
, CAST(NULL AS int2) AS subclaim5_category
, CAST(NULL AS int2) AS subclaim6_category
, CAST(NULL AS int2) AS state_growth_percent
, CAST(NULL AS int2) AS district_growth_percent
, CAST(NULL AS int2) AS parcc_growth_percent
, st.pba_attempt_flag AS attempt_flag
, st.is_multi_rec AS multirecord_flag
, st.pba_test_uuid AS test_uuid
, 2 AS result_type_key
, 'PBA' AS result_type
, CAST(st.record_type AS int2) AS rec_type_key
, CASE WHEN st.record_type = '01' THEN 'Summative Score'
 WHEN st.record_type = '02' THEN 'Single Component'
 ELSE 'No Component' END AS record_type
, CASE WHEN st.is_reported_summative = 'Y' THEN 1
 WHEN st.is_reported_summative = 'N' THEN 0
  ELSE -1 END AS reported_score_flag_key
, st.is_reported_summative AS reported_score_flag
FROM lkp_form_xref fx JOIN ( SELECT * FROM rpt_math_sum WHERE pba_test_uuid IS NOT NULL AND rec_status = 'C') st on ( fx.form_id = st.pba_form_id)
JOIN lkp_school_xref stx ON (stx.school_id = st.pba_test_school_id)
JOIN lkp_school_xref srx ON (srx.school_id = st.resp_school_id)
JOIN lkp_student_xref sx ON (sx.student_parcc_id = st.student_parcc_id AND
			    sx.ell = COALESCE ( st.ell, '-') AND
			    sx.lep_status = COALESCE ( st.lep_status, '-') AND
			    sx.gift_talent = COALESCE ( st.gift_talent, '-') AND
			    sx.migrant_status = COALESCE ( st.migrant_status, '-') AND
			    sx.econo_disadvantage = COALESCE ( st.econo_disadvantage, '-') AND
			    sx.disabil_student = COALESCE ( st.disabil_student, '-') AND
			    sx.primary_disabil_type = COALESCE ( st.primary_disabil_type, '-'))
UNION ALL
-- MATH EOY
SELECT
CAST( '121' || st.rec_id AS bigint) AS rec_id
, CAST( '555' || st.rec_id AS bigint) AS rec_key
, CASE WHEN UPPER(st.poy) = 'FALL'   THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '01' AS int4)
       WHEN UPPER(st.poy) = 'SPRING' THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '02' AS int4)
    ELSE -1 END AS poy_key
, CASE WHEN st.report_suppression_code IS NULL THEN -1
    ELSE CAST(st.report_suppression_code AS int2) END AS report_suppression_key
, CASE WHEN st.report_suppression_action IS NULL THEN -1
    ELSE CAST(st.report_suppression_action AS int2) END AS suppression_action_key
, CASE WHEN st.is_roster_reported = 'Y' THEN 1 
 WHEN st.is_roster_reported = 'N' THEN 0  ELSE -1 END AS reported_roster_key 
, fx.test_form_key
, srx.school_key AS resp_school_key
, stx.school_key AS test_school_key
, sx.student_key
, CASE WHEN st.student_grade IS NULL THEN -1
      ELSE CAST ( st.student_grade AS int2) END AS grade_key
, st.student_grade
, st.eoy_total_items AS total_items
, st.eoy_total_items_attempt AS total_items_attempt
, st.eoy_total_items_unit1 AS total_items_unit1
, st.eoy_unit1_items_attempt AS unit1_items_attempt
, st.eoy_total_items_unit2 AS total_items_unit2
, st.eoy_unit2_items_attempt AS unit2_items_attempt
, st.eoy_total_items_unit3 AS total_items_unit3
, st.eoy_unit3_items_attempt AS unit3_items_attempt
, st.eoy_total_items_unit4 AS total_items_unit4
, st.eoy_unit4_items_attempt AS unit4_items_attempt
, st.eoy_total_items_unit5 AS total_items_unit5
, st.eoy_unit5_items_attempt AS unit5_items_attempt
, CASE WHEN st.eoy_not_tested_reason IS NULL THEN -1
      ELSE CAST(st.eoy_not_tested_reason AS int2) END AS not_tested_reason
, CASE WHEN st.eoy_void_reason IS NULL THEN -1
      ELSE CAST(st.eoy_void_reason AS int2) END AS void_reason
, CAST(NULL AS smallint) AS sum_scale_score
, CAST(NULL AS int2) AS sum_csem
, CAST(NULL AS int2) AS sum_perf_lvl
, CAST(NULL AS int2) AS sum_read_scale_score
, CAST(NULL AS int2) AS sum_read_csem
, CAST(NULL AS int2) AS sum_write_scale_score
, CAST(NULL AS int2) AS sum_write_csem
, CAST(NULL AS int2) AS subclaim1_category
, CAST(NULL AS int2) AS subclaim2_category
, CAST(NULL AS int2) AS subclaim3_category
, CAST(NULL AS int2) AS subclaim4_category
, CAST(NULL AS int2) AS subclaim5_category
, CAST(NULL AS int2) AS subclaim6_category
, CAST(NULL AS int2) AS state_growth_percent
, CAST(NULL AS int2) AS district_growth_percent
, CAST(NULL AS int2) AS parcc_growth_percent
, st.eoy_attempt_flag AS attempt_flag
, st.is_multi_rec AS multirecord_flag
, st.eoy_test_uuid AS test_uuid
, 3 AS result_type_key
, 'EOY' AS result_type
, CAST(st.record_type AS int2) AS rec_type_key
, CASE WHEN st.record_type = '01' THEN 'Summative Score'
 WHEN st.record_type = '02' THEN 'Single Component'
 ELSE 'No Component' END AS record_type
, CASE WHEN st.is_reported_summative = 'Y' THEN 1
 WHEN st.is_reported_summative = 'N' THEN 0
  ELSE -1 END AS reported_score_flag_key
, st.is_reported_summative AS reported_score_flag
FROM lkp_form_xref fx JOIN ( SELECT * FROM rpt_math_sum WHERE eoy_test_uuid IS NOT NULL AND rec_status = 'C') st ON( fx.form_id = st.eoy_form_id)
JOIN lkp_school_xref stx ON (stx.school_id = st.eoy_test_school_id)
JOIN lkp_school_xref srx ON (srx.school_id = st.resp_school_id)
JOIN lkp_student_xref sx ON (sx.student_parcc_id = st.student_parcc_id AND
			    sx.ell = COALESCE ( st.ell, '-') AND
			    sx.lep_status = COALESCE ( st.lep_status, '-') AND
			    sx.gift_talent = COALESCE ( st.gift_talent, '-') AND
			    sx.migrant_status = COALESCE ( st.migrant_status, '-') AND
			    sx.econo_disadvantage = COALESCE ( st.econo_disadvantage, '-') AND
			    sx.disabil_student = COALESCE ( st.disabil_student, '-') AND
			    sx.primary_disabil_type = COALESCE ( st.primary_disabil_type, '-'))
UNION ALL 
-- MATH Summative
SELECT
CAST( '111' || st.rec_id AS bigint) AS rec_id
, CAST( '555' || st.rec_id AS bigint) AS rec_key
, CASE WHEN UPPER(st.poy) = 'FALL'   THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '01' AS int4)
       WHEN UPPER(st.poy) = 'SPRING' THEN CAST(SUBSTRING(st.year FROM 6 FOR 4) || '02' AS int4)
    ELSE -1 END AS poy_key
, CASE WHEN st.report_suppression_code IS NULL THEN -1
    ELSE CAST(st.report_suppression_code AS int2) END AS report_suppression_key
, CASE WHEN st.report_suppression_action IS NULL THEN -1
    ELSE CAST(st.report_suppression_action AS int2) END AS suppression_action_key
, CASE WHEN st.is_roster_reported = 'Y' THEN 1 
 WHEN st.is_roster_reported = 'N' THEN 0  ELSE -1 END AS reported_roster_key 
, fx.test_form_key
, srx.school_key AS resp_school_key
, -1 AS test_school_key
, sx.student_key
, CASE WHEN st.student_grade IS NULL THEN -1
      ELSE CAST ( st.student_grade AS int2) END AS grade_key
, st.student_grade
, CAST(NULL AS int2) AS total_items
, CAST(NULL AS int2) AS total_items_attempt
, CAST(NULL AS int2) AS total_items_unit1
, CAST(NULL AS int2) AS unit1_items_attempt
, CAST(NULL AS int2) AS total_items_unit2
, CAST(NULL AS int2) AS unit2_items_attempt
, CAST(NULL AS int2) AS total_items_unit3
, CAST(NULL AS int2) AS unit3_items_attempt
, CAST(NULL AS int2) AS total_items_unit4
, CAST(NULL AS int2) AS unit4_items_attempt
, CAST(NULL AS int2) AS total_items_unit5
, CAST(NULL AS int2) AS unit5_items_attempt
, -1 AS not_tested_reason
, -1 AS void_reason
, st.sum_scale_score AS sum_scale_score
, st.sum_csem AS sum_csem
, st.sum_perf_lvl AS sum_perf_lvl
, st.sum_read_scale_score AS sum_read_scale_score
, st.sum_read_csem AS sum_read_csem
, st.sum_write_scale_score AS sum_write_scale_score
, st.sum_write_csem AS sum_write_csem
, st.subclaim1_category AS subclaim1_category
, st.subclaim2_category AS subclaim2_category
, st.subclaim3_category AS subclaim3_category
, st.subclaim4_category AS subclaim4_category
, st.subclaim5_category AS subclaim5_category
, st.subclaim6_category AS subclaim6_category
, st.state_growth_percent AS state_growth_percent
, st.district_growth_percent AS district_growth_percent
, st.parcc_growth_percent AS parcc_growth_percent
, st.pba_attempt_flag AS attempt_flag
, st.is_multi_rec AS multirecord_flag
, st.sum_score_rec_uuid AS test_uuid
, 1 AS result_type_key
, 'Summative' AS result_type
, CAST(st.record_type AS int2) AS rec_type_key
, CASE WHEN st.record_type = '01' THEN 'Summative Score'
 WHEN st.record_type = '02' THEN 'Single Component'
 ELSE 'No Component' END AS record_type
, CASE WHEN st.is_reported_summative = 'Y' THEN 1
 WHEN st.is_reported_summative = 'N' THEN 0
  ELSE -1 END AS reported_score_flag_key
, st.is_reported_summative AS reported_score_flag
FROM  ( SELECT * FROM lkp_form_xref WHERE form_id = 'NA') fx JOIN ( SELECT * FROM rpt_math_sum WHERE sum_score_rec_uuid IS NOT NULL AND rec_status = 'C') st 
ON ( fx.test_code = st.test_code)
JOIN lkp_school_xref stx ON (stx.school_id = st.pba_test_school_id)
JOIN lkp_school_xref srx ON (srx.school_id = st.resp_school_id)
JOIN lkp_student_xref sx ON (sx.student_parcc_id = st.student_parcc_id AND
			    sx.ell = COALESCE ( st.ell, '-') AND
			    sx.lep_status = COALESCE ( st.lep_status, '-') AND
			    sx.gift_talent = COALESCE ( st.gift_talent, '-') AND
			    sx.migrant_status = COALESCE ( st.migrant_status, '-') AND
			    sx.econo_disadvantage = COALESCE ( st.econo_disadvantage, '-') AND
			    sx.disabil_student = COALESCE ( st.disabil_student, '-') AND
			    sx.primary_disabil_type = COALESCE ( st.primary_disabil_type, '-'))
);