﻿-- **************************
-- -1 for math summative and -2 for ela summative
-- is generated in fact_sum and dim_test Inserts
-- **************************
INSERT INTO lkp_form_xref (
 form_id
 , test_code
)
(
SELECT 
  st.pba_form_id AS form_id
  , st.test_code
FROM vw_src_form_xref st
);