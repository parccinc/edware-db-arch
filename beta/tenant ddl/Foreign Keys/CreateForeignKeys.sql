-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk1 
    FOREIGN KEY (test_school_key)
    REFERENCES dim_institution(school_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk12 
    FOREIGN KEY (rec_key)
    REFERENCES dim_accomod(rec_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk4 
    FOREIGN KEY (student_key)
    REFERENCES dim_student(student_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk5 
    FOREIGN KEY (test_form_key)
    REFERENCES dim_test(test_form_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk6 
    FOREIGN KEY (poy_key)
    REFERENCES dim_poy(poy_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk7 
    FOREIGN KEY (rec_key)
    REFERENCES dim_opt_state_data(rec_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk8 
    FOREIGN KEY (resp_school_key)
    REFERENCES dim_institution(school_key)
;


