--
-- ER/Studio Data Architect 10.0 SQL Code Generation
-- Company :      Amplify, Inc.
-- Project :      Parcc_OLAP LDM
-- Author :       Vadim Nirenberg
--
-- Date Created : Tuesday, May 26, 2015 16:25:41
-- Target DBMS : PostgreSQL 9.x
--

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk1
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk12
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk4
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk5
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk6
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk7
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk8
;

DROP TABLE dim_accomod
;
DROP TABLE dim_institution
;
DROP TABLE dim_opt_state_data
;
DROP TABLE dim_poy
;
DROP TABLE dim_student
;
DROP TABLE dim_test
;
DROP TABLE fact_sum
;
-- 
-- TABLE: dim_accomod 
--

CREATE TABLE dim_accomod(
    rec_key                           int8           NOT NULL,
    accomod_ell                       char(1),
    accomod_504                       char(1),
    accomod_ind_ed                    char(1),
    accomod_freq_breacks              char(1),
    accomod_alt_location              char(1),
    accomod_small_group               char(1),
    accomod_special_equip             char(1),
    accomod_spec_area                 char(1),
    accomod_time_day                  char(1),
    accomod_answer_mask               char(1),
    accomod_color_contrast            varchar(19),
    accomod_asl_video                 char(1),
    accomod_screen_reader             char(1),
    accomod_close_capt_ela            char(1),
    accomod_read_math_ela             varchar(14),
    accomod_braille_ela               char(1),
    accomod_tactile_graph             char(1),
    accomod_text_2_speech_math_ela    char(1),
    accomod_answer_rec                char(1),
    accomod_braille_resp              varchar(16),
    accomod_construct_resp_ela        varchar(20),
    accomod_select_resp_ela           varchar(20),
    accomod_monitor_test_resp         char(1),
    accomod_word_predict              char(1),
    accomod_native_lang               char(1),
    accomod_loud_native_lang          varchar(40),
    accomod_w_2_w_dict                char(1),
    accomod_extend_time               varchar(6),
    accomod_alt_paper_test            char(1),
    accomod_human_read_sign           varchar(14),
    accomod_large_print               char(1),
    accomod_braille_tactile           char(1),
    accomod_math_resp_el              varchar(20),
    accomod_calculator                char(1),
    accomod_math_trans_online         char(3),
    accomod_paper_trans_math          char(3),
    accomod_math_resp                 varchar(20),
    last_insert_date                  date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from                        date           NOT NULL,
    valid_to                          date           NOT NULL,
    rec_version                       int2           NOT NULL
)
;



COMMENT ON COLUMN dim_accomod.accomod_ell IS 'Assessment Accommodation:  English learner (EL)'
;
COMMENT ON COLUMN dim_accomod.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)'
;
COMMENT ON COLUMN dim_accomod.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location'
;
COMMENT ON COLUMN dim_accomod.accomod_small_group IS 'Small Testing Group'
;
COMMENT ON COLUMN dim_accomod.accomod_special_equip IS 'Specialized Equipment or Furniture'
;
COMMENT ON COLUMN dim_accomod.accomod_spec_area IS 'Specified Area or Setting'
;
COMMENT ON COLUMN dim_accomod.accomod_time_day IS 'Time Of Day'
;
COMMENT ON COLUMN dim_accomod.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.'
;
COMMENT ON COLUMN dim_accomod.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_read_math_ela IS 'General Administration Directions Read Aloud and Repeated as Needed in Student''s Native Language(by test administrator).OralScriptReadbyTestAdministratorARA; OralScriptReadbyTestAdministratorCHI ; OralScriptReadbyTestAdministratorHAT; OralScriptReadbyTestAdministratorMAH; OralScriptReadbyTestAdministratorNAV; OralScriptReadbyTestAdministratorPOL; OralScriptReadbyTestAdministratorPOR;'' OralScriptReadbyTestAdministratorSOM; OralScriptReadbyTestAdministratorSPA; OralScriptReadbyTestAdministratorVIE; HumanTranslator; blank;'
;
COMMENT ON COLUMN dim_accomod.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_text_2_speech_math_ela IS 'Text-to-Speech for ELA/L. Y blank. Personal Needs Profile field. Test content will be read aloud to the student via Text-to-Speech. Embedded within TestNav8. Text-to-Speech is an accessibility feature for Math. Text-to-Speech is an accommodation for ELA/L. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.'
;
COMMENT ON COLUMN dim_accomod.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types'
;
COMMENT ON COLUMN dim_accomod.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_native_lang IS 'General Administration Directions Clarified in Studentís Native Language (by test administrator)'
;
COMMENT ON COLUMN dim_accomod.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Studentís Native Language
(by test administrator)'
;
COMMENT ON COLUMN dim_accomod.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN dim_accomod.accomod_extend_time IS 'Extended Time'
;
COMMENT ON COLUMN dim_accomod.accomod_alt_paper_test IS 'Alternate Representation - Paper Test'
;
COMMENT ON COLUMN dim_accomod.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN dim_accomod.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN dim_accomod.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;

-- 
-- TABLE: dim_institution 
--

CREATE TABLE dim_institution(
    school_key          serial         NOT NULL,
    dist_name           varchar(60)    DEFAULT 'UNKNOWN' NOT NULL,
    dist_id             varchar(15)    NOT NULL,
    school_name         varchar(60)    NOT NULL,
    school_id           varchar(15)    NOT NULL,
    last_insert_date    date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date           NOT NULL,
    valid_to            date           NOT NULL,
    rec_version         int2           NOT NULL
)
;



COMMENT ON COLUMN dim_institution.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN dim_institution.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN dim_institution.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN dim_institution.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;

-- 
-- TABLE: dim_opt_state_data 
--

CREATE TABLE dim_opt_state_data(
    rec_key             int8           NOT NULL,
    opt_state_data8     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data7     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data6     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data5     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data4     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data3     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data2     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data1     varchar(20)    DEFAULT 'NA' NOT NULL,
    last_insert_date    date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date           NOT NULL,
    valid_to            date           NOT NULL,
    rec_version         int2           NOT NULL
)
;




-- 
-- TABLE: dim_poy 
--

CREATE TABLE dim_poy(
    poy_key             numeric(1, 0)    DEFAULT -1 NOT NULL,
    poy                 varchar(20)      NOT NULL,
    end_year            numeric(4, 0)    NOT NULL,
    school_year         char(9)          NOT NULL,
    last_insert_date    date             DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date             NOT NULL,
    valid_to            date             NOT NULL,
    rec_version         int2             NOT NULL
)
;



COMMENT ON COLUMN dim_poy.poy_key IS 'YEAR || 01 OR YEAR || 02'
;
COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring'
;
COMMENT ON COLUMN dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014'
;

-- 
-- TABLE: dim_student 
--

CREATE TABLE dim_student(
    student_key             int4            NOT NULL,
    student_parcc_id        varchar(40)     NOT NULL,
    student_sex             char(1),
    ethnicity               varchar(20)     DEFAULT 'UNKNOWN' NOT NULL,
    ethn_hisp_latino        char(1),
    ethn_indian_alaska      char(1),
    ethn_asian              char(1),
    ethn_black              char(1),
    ethn_hawai              char(1),
    ethn_white              char(1),
    ethn_filler             char(1),
    ethn_2_more             char(1),
    ell                     char(1),
    lep_status              char(1),
    gift_talent             char(1),
    migrant_status          char(1),
    econo_disadvantage      char(1),
    disabil_student         char(1),
    primary_disabil_type    varchar(3),
    student_state_id        varchar(40)     DEFAULT 'UNKNOWN',
    student_local_id        varchar(40)     DEFAULT 'UNKNOWN' NOT NULL,
    student_name            varchar(105)    DEFAULT 'UNKNOWN' NOT NULL,
    student_dob             date            NOT NULL,
    last_insert_date        date            DEFAULT CURRENT_DATE NOT NULL,
    valid_from              date            NOT NULL,
    valid_to                date            NOT NULL,
    rec_version             int2            NOT NULL
)
;



COMMENT ON COLUMN dim_student.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.'
;
COMMENT ON COLUMN dim_student.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425'
;
COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).'
;
COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783'
;
COMMENT ON COLUMN dim_student.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.'
;
COMMENT ON COLUMN dim_student.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789'
;
COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741'
;
COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569†'
;
COMMENT ON COLUMN dim_student.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.'
;
COMMENT ON COLUMN dim_student.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN dim_student.student_name IS 'Last name||", "|| First name ||", "||Middle Name'
;
COMMENT ON COLUMN dim_student.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995'
;

-- 
-- TABLE: dim_test 
--

CREATE TABLE dim_test(
    test_form_key       int4           NOT NULL,
    form_id             varchar(14)    DEFAULT 'NA' NOT NULL,
    test_category       varchar(2)     DEFAULT 'NA' NOT NULL,
    test_subject        varchar(35)    DEFAULT 'UNKNOWN' NOT NULL,
    asmt_grade          varchar(12)    NOT NULL,
    test_code           varchar(20)    NOT NULL,
    last_insert_date    date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date           NOT NULL,
    valid_to            date           NOT NULL,
    rec_version         int2           NOT NULL
)
;



COMMENT ON COLUMN dim_test.test_form_key IS 'NA for summative different for different test_codes'
;
COMMENT ON COLUMN dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative'
;
COMMENT ON COLUMN dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component '
;
COMMENT ON COLUMN dim_test.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356'
;
COMMENT ON COLUMN dim_test.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade'
;
COMMENT ON TABLE dim_test IS 'removed course, course_key fields'
;

-- 
-- TABLE: fact_sum 
--

CREATE TABLE fact_sum(
    rec_id                       int8             NOT NULL,
    rec_key                      int8             NOT NULL,
    poy_key                      numeric(1, 0)    DEFAULT -1 NOT NULL,
    test_form_key                int4             NOT NULL,
    test_school_key              serial           NOT NULL,
    resp_school_key              serial           NOT NULL,
    student_key                  int4             NOT NULL,
    student_grade                varchar(12)      NOT NULL,
    total_items                  numeric(3, 0),
    total_items_attempt          numeric(3, 0),
    total_items_unit1            numeric(2, 0),
    unit1_items_attempt          numeric(2, 0),
    total_items_unit2            numeric(2, 0),
    unit2_items_attempt          numeric(2, 0),
    total_items_unit3            numeric(2, 0),
    unit3_items_attempt          numeric(2, 0),
    total_items_unit4            numeric(2, 0),
    unit4_items_attempt          numeric(2, 0),
    total_items_unit5            numeric(2, 0),
    unit5_items_attempt          numeric(2, 0),
    not_tested_reason            numeric(2, 0),
    void_reason                  numeric(2, 0),
    sum_scale_score              int4,
    sum_csem                     int4,
    sum_perf_lvl                 int2,
    sum_read_scale_score         int4,
    sum_read_csem                int4,
    sum_write_scale_score        int4,
    sum_write_csem               int4,
    subclaim1_category           int2,
    subclaim2_category           int2,
    subclaim3_category           int2,
    subclaim4_category           int2,
    subclaim5_category           int2,
    subclaim6_category           int2,
    state_growth_percent         numeric(5, 2),
    district_growth_percent      numeric(5, 2),
    parcc_growth_percent         numeric(5, 2),
    attempt_flag                 char(1)          DEFAULT -1 NOT NULL,
    multirecord_flag             char(1)          DEFAULT -1 NOT NULL,
    test_uuid                    varchar(36)      NOT NULL,
    result_type                  varchar(10)      NOT NULL,
    record_type                  varchar(20)      NOT NULL,
    reported_score_flag          char(1)          NOT NULL,
    report_suppression_code      char(2),
    report_suppression_action    int2,
    reported_roster_flag         varchar(2)       DEFAULT 'NA' NOT NULL,
    include_in_parcc             boolean          NOT NULL,
    include_in_state             boolean          NOT NULL,
    include_in_district          boolean          NOT NULL,
    include_in_school            boolean          NOT NULL,
    include_in_isr               boolean          NOT NULL,
    include_in_roster            char(1)          NOT NULL,
    create_date                  date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN fact_sum.poy_key IS 'YEAR || 01 OR YEAR || 02'
;
COMMENT ON COLUMN fact_sum.test_form_key IS 'NA for summative different for different test_codes'
;
COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN fact_sum.total_items IS 'Total number of questions/items on the PBA, EOY test for that form'
;
COMMENT ON COLUMN fact_sum.total_items_attempt IS 'PBA Total Test Items Attempted'
;
COMMENT ON COLUMN fact_sum.total_items_unit1 IS 'PBA Unit 1 Total Number of Items'
;
COMMENT ON COLUMN fact_sum.total_items_unit2 IS 'PBA Unit 2 Total Number of Items. Total number of items on unit 2 of the test.'
;
COMMENT ON COLUMN fact_sum.unit2_items_attempt IS 'PBA Unit 2 Number of Attempted Items. Number of unit 2 items students attempted.'
;
COMMENT ON COLUMN fact_sum.total_items_unit3 IS 'PBA Unit 3 Total Number of Items'
;
COMMENT ON COLUMN fact_sum.void_reason IS 'PBA Void PBA/EOY Score Reason'
;
COMMENT ON COLUMN fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN fact_sum.sum_csem IS 'Summative CSEM'
;
COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level'
;
COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score'
;
COMMENT ON COLUMN fact_sum.sum_read_csem IS 'Summative Reading CSEM'
;
COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN fact_sum.sum_write_csem IS 'Summative Writing CSEM'
;
COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse'
;
COMMENT ON COLUMN fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District'
;
COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC'
;
COMMENT ON COLUMN fact_sum.attempt_flag IS '1 - Y, 0 - N, -1 = blank'
;
COMMENT ON COLUMN fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank'
;
COMMENT ON COLUMN fact_sum.test_uuid IS 'PBA, eoy or fake Student Test UUID.'
;
COMMENT ON COLUMN fact_sum.result_type IS 'Summative, PBA, EOY'
;
COMMENT ON COLUMN fact_sum.record_type IS ' Summative Score, Single Component, No Component'
;
COMMENT ON COLUMN fact_sum.reported_score_flag IS 'Y,N,balnk'
;
COMMENT ON COLUMN fact_sum.reported_roster_flag IS 'Y, N, NA'
;

-- 
-- INDEX: fact_sum_ix1 
--

DROP INDEX fact_sum_ix1
;

CREATE INDEX fact_sum_ix1 ON fact_sum(rec_key)
;
-- 
-- INDEX: fact_sum_ix2 
--

DROP INDEX fact_sum_ix2
;

CREATE INDEX fact_sum_ix2 ON fact_sum(poy_key)
;
-- 
-- INDEX: fact_sum_ix6 
--

DROP INDEX fact_sum_ix6
;

CREATE INDEX fact_sum_ix6 ON fact_sum(test_form_key)
;
-- 
-- INDEX: fact_sum_ix7 
--

DROP INDEX fact_sum_ix7
;

CREATE INDEX fact_sum_ix7 ON fact_sum(test_school_key)
;
-- 
-- INDEX: fact_sum_ix8 
--

DROP INDEX fact_sum_ix8
;

CREATE INDEX fact_sum_ix8 ON fact_sum(resp_school_key)
;
-- 
-- INDEX: fact_sum_ix9 
--

DROP INDEX fact_sum_ix9
;

CREATE INDEX fact_sum_ix9 ON fact_sum(student_key)
;
-- 
-- TABLE: dim_accomod 
--

ALTER TABLE dim_accomod ADD 
    CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (rec_key)
;

-- 
-- TABLE: dim_institution 
--

ALTER TABLE dim_institution ADD 
    CONSTRAINT dim_institution_pk PRIMARY KEY (school_key)
;

-- 
-- TABLE: dim_opt_state_data 
--

ALTER TABLE dim_opt_state_data ADD 
    CONSTRAINT dim_opt_state_data_pk PRIMARY KEY (rec_key)
;

-- 
-- TABLE: dim_poy 
--

ALTER TABLE dim_poy ADD 
    CONSTRAINT dim_poy_pk PRIMARY KEY (poy_key)
;

-- 
-- TABLE: dim_student 
--

ALTER TABLE dim_student ADD 
    CONSTRAINT dim_student_pk PRIMARY KEY (student_key)
;

-- 
-- TABLE: dim_test 
--

ALTER TABLE dim_test ADD 
    CONSTRAINT dim_test_pk PRIMARY KEY (test_form_key)
;

-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD 
    CONSTRAINT fact_sum_pk PRIMARY KEY (rec_id)
;

-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk1 
    FOREIGN KEY (test_school_key)
    REFERENCES dim_institution(school_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk12 
    FOREIGN KEY (rec_key)
    REFERENCES dim_accomod(rec_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk4 
    FOREIGN KEY (student_key)
    REFERENCES dim_student(student_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk5 
    FOREIGN KEY (test_form_key)
    REFERENCES dim_test(test_form_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk6 
    FOREIGN KEY (poy_key)
    REFERENCES dim_poy(poy_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk7 
    FOREIGN KEY (rec_key)
    REFERENCES dim_opt_state_data(rec_key)
;

ALTER TABLE fact_sum ADD CONSTRAINT fact_sum_fk8 
    FOREIGN KEY (resp_school_key)
    REFERENCES dim_institution(school_key)
;


