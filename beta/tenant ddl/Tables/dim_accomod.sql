DROP TABLE dim_accomod
;
-- 
-- TABLE: dim_accomod 
--

CREATE TABLE dim_accomod(
    rec_key                           int8           NOT NULL,
    accomod_ell                       char(1),
    accomod_504                       char(1),
    accomod_ind_ed                    char(1),
    accomod_freq_breacks              char(1),
    accomod_alt_location              char(1),
    accomod_small_group               char(1),
    accomod_special_equip             char(1),
    accomod_spec_area                 char(1),
    accomod_time_day                  char(1),
    accomod_answer_mask               char(1),
    accomod_color_contrast            varchar(19),
    accomod_asl_video                 char(1),
    accomod_screen_reader             char(1),
    accomod_close_capt_ela            char(1),
    accomod_read_math_ela             varchar(14),
    accomod_braille_ela               char(1),
    accomod_tactile_graph             char(1),
    accomod_text_2_speech_math_ela    char(1),
    accomod_answer_rec                char(1),
    accomod_braille_resp              varchar(16),
    accomod_construct_resp_ela        varchar(20),
    accomod_select_resp_ela           varchar(20),
    accomod_monitor_test_resp         char(1),
    accomod_word_predict              char(1),
    accomod_native_lang               char(1),
    accomod_loud_native_lang          varchar(40),
    accomod_w_2_w_dict                char(1),
    accomod_extend_time               varchar(6),
    accomod_alt_paper_test            char(1),
    accomod_human_read_sign           varchar(14),
    accomod_large_print               char(1),
    accomod_braille_tactile           char(1),
    accomod_math_resp_el              varchar(20),
    accomod_calculator                char(1),
    accomod_math_trans_online         char(3),
    accomod_paper_trans_math          char(3),
    accomod_math_resp                 varchar(20),
    last_insert_date                  date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from                        date           NOT NULL,
    valid_to                          date           NOT NULL,
    rec_version                       int2           NOT NULL
)
;



COMMENT ON COLUMN dim_accomod.accomod_ell IS 'Assessment Accommodation:  English learner (EL)'
;
COMMENT ON COLUMN dim_accomod.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)'
;
COMMENT ON COLUMN dim_accomod.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location'
;
COMMENT ON COLUMN dim_accomod.accomod_small_group IS 'Small Testing Group'
;
COMMENT ON COLUMN dim_accomod.accomod_special_equip IS 'Specialized Equipment or Furniture'
;
COMMENT ON COLUMN dim_accomod.accomod_spec_area IS 'Specified Area or Setting'
;
COMMENT ON COLUMN dim_accomod.accomod_time_day IS 'Time Of Day'
;
COMMENT ON COLUMN dim_accomod.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.'
;
COMMENT ON COLUMN dim_accomod.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_read_math_ela IS 'General Administration Directions Read Aloud and Repeated as Needed in Student''s Native Language(by test administrator).OralScriptReadbyTestAdministratorARA; OralScriptReadbyTestAdministratorCHI ; OralScriptReadbyTestAdministratorHAT; OralScriptReadbyTestAdministratorMAH; OralScriptReadbyTestAdministratorNAV; OralScriptReadbyTestAdministratorPOL; OralScriptReadbyTestAdministratorPOR;'' OralScriptReadbyTestAdministratorSOM; OralScriptReadbyTestAdministratorSPA; OralScriptReadbyTestAdministratorVIE; HumanTranslator; blank;'
;
COMMENT ON COLUMN dim_accomod.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_text_2_speech_math_ela IS 'Text-to-Speech for ELA/L. Y blank. Personal Needs Profile field. Test content will be read aloud to the student via Text-to-Speech. Embedded within TestNav8. Text-to-Speech is an accessibility feature for Math. Text-to-Speech is an accommodation for ELA/L. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.'
;
COMMENT ON COLUMN dim_accomod.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types'
;
COMMENT ON COLUMN dim_accomod.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_native_lang IS 'General Administration Directions Clarified in Studentís Native Language (by test administrator)'
;
COMMENT ON COLUMN dim_accomod.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Studentís Native Language
(by test administrator)'
;
COMMENT ON COLUMN dim_accomod.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN dim_accomod.accomod_extend_time IS 'Extended Time'
;
COMMENT ON COLUMN dim_accomod.accomod_alt_paper_test IS 'Alternate Representation - Paper Test'
;
COMMENT ON COLUMN dim_accomod.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;
COMMENT ON COLUMN dim_accomod.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN dim_accomod.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.'
;
COMMENT ON COLUMN dim_accomod.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.'
;

-- 
-- TABLE: dim_accomod 
--

ALTER TABLE dim_accomod ADD 
    CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (rec_key)
;

