DROP TABLE fact_sum
;
-- 
-- TABLE: fact_sum 
--

CREATE TABLE fact_sum(
    rec_id                       int8             NOT NULL,
    rec_key                      int8             NOT NULL,
    poy_key                      numeric(1, 0)    DEFAULT -1 NOT NULL,
    test_form_key                int4             NOT NULL,
    test_school_key              serial           NOT NULL,
    resp_school_key              serial           NOT NULL,
    student_key                  int4             NOT NULL,
    student_grade                varchar(12)      NOT NULL,
    total_items                  numeric(3, 0),
    total_items_attempt          numeric(3, 0),
    total_items_unit1            numeric(2, 0),
    unit1_items_attempt          numeric(2, 0),
    total_items_unit2            numeric(2, 0),
    unit2_items_attempt          numeric(2, 0),
    total_items_unit3            numeric(2, 0),
    unit3_items_attempt          numeric(2, 0),
    total_items_unit4            numeric(2, 0),
    unit4_items_attempt          numeric(2, 0),
    total_items_unit5            numeric(2, 0),
    unit5_items_attempt          numeric(2, 0),
    not_tested_reason            numeric(2, 0),
    void_reason                  numeric(2, 0),
    sum_scale_score              int4,
    sum_csem                     int4,
    sum_perf_lvl                 int2,
    sum_read_scale_score         int4,
    sum_read_csem                int4,
    sum_write_scale_score        int4,
    sum_write_csem               int4,
    subclaim1_category           int2,
    subclaim2_category           int2,
    subclaim3_category           int2,
    subclaim4_category           int2,
    subclaim5_category           int2,
    subclaim6_category           int2,
    state_growth_percent         numeric(5, 2),
    district_growth_percent      numeric(5, 2),
    parcc_growth_percent         numeric(5, 2),
    attempt_flag                 char(1)          DEFAULT -1 NOT NULL,
    multirecord_flag             char(1)          DEFAULT -1 NOT NULL,
    test_uuid                    varchar(36)      NOT NULL,
    result_type                  varchar(10)      NOT NULL,
    record_type                  varchar(20)      NOT NULL,
    reported_score_flag          char(1)          NOT NULL,
    report_suppression_code      char(2),
    report_suppression_action    int2,
    reported_roster_flag         varchar(2)       DEFAULT 'NA' NOT NULL,
    include_in_parcc             boolean          NOT NULL,
    include_in_state             boolean          NOT NULL,
    include_in_district          boolean          NOT NULL,
    include_in_school            boolean          NOT NULL,
    include_in_isr               boolean          NOT NULL,
    include_in_roster            char(1)          NOT NULL,
    create_date                  date             DEFAULT CURRENT_DATE NOT NULL
)
;



COMMENT ON COLUMN fact_sum.poy_key IS 'YEAR || 01 OR YEAR || 02'
;
COMMENT ON COLUMN fact_sum.test_form_key IS 'NA for summative different for different test_codes'
;
COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362'
;
COMMENT ON COLUMN fact_sum.total_items IS 'Total number of questions/items on the PBA, EOY test for that form'
;
COMMENT ON COLUMN fact_sum.total_items_attempt IS 'PBA Total Test Items Attempted'
;
COMMENT ON COLUMN fact_sum.total_items_unit1 IS 'PBA Unit 1 Total Number of Items'
;
COMMENT ON COLUMN fact_sum.total_items_unit2 IS 'PBA Unit 2 Total Number of Items. Total number of items on unit 2 of the test.'
;
COMMENT ON COLUMN fact_sum.unit2_items_attempt IS 'PBA Unit 2 Number of Attempted Items. Number of unit 2 items students attempted.'
;
COMMENT ON COLUMN fact_sum.total_items_unit3 IS 'PBA Unit 3 Total Number of Items'
;
COMMENT ON COLUMN fact_sum.void_reason IS 'PBA Void PBA/EOY Score Reason'
;
COMMENT ON COLUMN fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN fact_sum.sum_csem IS 'Summative CSEM'
;
COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level'
;
COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score'
;
COMMENT ON COLUMN fact_sum.sum_read_csem IS 'Summative Reading CSEM'
;
COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score'
;
COMMENT ON COLUMN fact_sum.sum_write_csem IS 'Summative Writing CSEM'
;
COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored'
;
COMMENT ON COLUMN fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse'
;
COMMENT ON COLUMN fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District'
;
COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC'
;
COMMENT ON COLUMN fact_sum.attempt_flag IS '1 - Y, 0 - N, -1 = blank'
;
COMMENT ON COLUMN fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank'
;
COMMENT ON COLUMN fact_sum.test_uuid IS 'PBA, eoy or fake Student Test UUID.'
;
COMMENT ON COLUMN fact_sum.result_type IS 'Summative, PBA, EOY'
;
COMMENT ON COLUMN fact_sum.record_type IS ' Summative Score, Single Component, No Component'
;
COMMENT ON COLUMN fact_sum.reported_score_flag IS 'Y,N,balnk'
;
COMMENT ON COLUMN fact_sum.reported_roster_flag IS 'Y, N, NA'
;

-- 
-- INDEX: fact_sum_ix1 
--

DROP INDEX fact_sum_ix1
;

CREATE INDEX fact_sum_ix1 ON fact_sum(rec_key)
;
-- 
-- INDEX: fact_sum_ix2 
--

DROP INDEX fact_sum_ix2
;

CREATE INDEX fact_sum_ix2 ON fact_sum(poy_key)
;
-- 
-- INDEX: fact_sum_ix6 
--

DROP INDEX fact_sum_ix6
;

CREATE INDEX fact_sum_ix6 ON fact_sum(test_form_key)
;
-- 
-- INDEX: fact_sum_ix7 
--

DROP INDEX fact_sum_ix7
;

CREATE INDEX fact_sum_ix7 ON fact_sum(test_school_key)
;
-- 
-- INDEX: fact_sum_ix8 
--

DROP INDEX fact_sum_ix8
;

CREATE INDEX fact_sum_ix8 ON fact_sum(resp_school_key)
;
-- 
-- INDEX: fact_sum_ix9 
--

DROP INDEX fact_sum_ix9
;

CREATE INDEX fact_sum_ix9 ON fact_sum(student_key)
;
-- 
-- TABLE: fact_sum 
--

ALTER TABLE fact_sum ADD 
    CONSTRAINT fact_sum_pk PRIMARY KEY (rec_id)
;

