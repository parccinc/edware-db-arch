DROP TABLE dim_institution
;
-- 
-- TABLE: dim_institution 
--

CREATE TABLE dim_institution(
    school_key          serial         NOT NULL,
    dist_name           varchar(60)    DEFAULT 'UNKNOWN' NOT NULL,
    dist_id             varchar(15)    NOT NULL,
    school_name         varchar(60)    NOT NULL,
    school_id           varchar(15)    NOT NULL,
    last_insert_date    date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date           NOT NULL,
    valid_to            date           NOT NULL,
    rec_version         int2           NOT NULL
)
;



COMMENT ON COLUMN dim_institution.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN dim_institution.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN dim_institution.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;
COMMENT ON COLUMN dim_institution.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.'
;

-- 
-- TABLE: dim_institution 
--

ALTER TABLE dim_institution ADD 
    CONSTRAINT dim_institution_pk PRIMARY KEY (school_key)
;

