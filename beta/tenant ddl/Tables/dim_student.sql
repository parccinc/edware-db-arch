DROP TABLE dim_student
;
-- 
-- TABLE: dim_student 
--

CREATE TABLE dim_student(
    student_key             int4            NOT NULL,
    student_parcc_id        varchar(40)     NOT NULL,
    student_sex             char(1),
    ethnicity               varchar(20)     DEFAULT 'UNKNOWN' NOT NULL,
    ethn_hisp_latino        char(1),
    ethn_indian_alaska      char(1),
    ethn_asian              char(1),
    ethn_black              char(1),
    ethn_hawai              char(1),
    ethn_white              char(1),
    ethn_filler             char(1),
    ethn_2_more             char(1),
    ell                     char(1),
    lep_status              char(1),
    gift_talent             char(1),
    migrant_status          char(1),
    econo_disadvantage      char(1),
    disabil_student         char(1),
    primary_disabil_type    varchar(3),
    student_state_id        varchar(40)     DEFAULT 'UNKNOWN',
    student_local_id        varchar(40)     DEFAULT 'UNKNOWN' NOT NULL,
    student_name            varchar(105)    DEFAULT 'UNKNOWN' NOT NULL,
    student_dob             date            NOT NULL,
    last_insert_date        date            DEFAULT CURRENT_DATE NOT NULL,
    valid_from              date            NOT NULL,
    valid_to                date            NOT NULL,
    rec_version             int2            NOT NULL
)
;



COMMENT ON COLUMN dim_student.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.'
;
COMMENT ON COLUMN dim_student.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765'
;
COMMENT ON COLUMN dim_student.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425'
;
COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).'
;
COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783'
;
COMMENT ON COLUMN dim_student.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.'
;
COMMENT ON COLUMN dim_student.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789'
;
COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741'
;
COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569 '
;
COMMENT ON COLUMN dim_student.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.'
;
COMMENT ON COLUMN dim_student.student_local_id IS 'Parcc name: LocalStudentIdentifier. '
;
COMMENT ON COLUMN dim_student.student_name IS 'Last name||", "|| First name ||", "||Middle Name'
;
COMMENT ON COLUMN dim_student.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995'
;

-- 
-- TABLE: dim_student 
--

ALTER TABLE dim_student ADD 
    CONSTRAINT dim_student_pk PRIMARY KEY (student_key)
;

