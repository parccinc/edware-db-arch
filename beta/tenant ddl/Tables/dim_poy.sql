DROP TABLE dim_poy
;
-- 
-- TABLE: dim_poy 
--

CREATE TABLE dim_poy(
    poy_key             numeric(1, 0)    DEFAULT -1 NOT NULL,
    poy                 varchar(20)      NOT NULL,
    end_year            numeric(4, 0)    NOT NULL,
    school_year         char(9)          NOT NULL,
    last_insert_date    date             DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date             NOT NULL,
    valid_to            date             NOT NULL,
    rec_version         int2             NOT NULL
)
;



COMMENT ON COLUMN dim_poy.poy_key IS 'YEAR || 01 OR YEAR || 02'
;
COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring'
;
COMMENT ON COLUMN dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014'
;

-- 
-- TABLE: dim_poy 
--

ALTER TABLE dim_poy ADD 
    CONSTRAINT dim_poy_pk PRIMARY KEY (poy_key)
;

