DROP TABLE dim_opt_state_data
;
-- 
-- TABLE: dim_opt_state_data 
--

CREATE TABLE dim_opt_state_data(
    rec_key             int8           NOT NULL,
    opt_state_data8     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data7     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data6     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data5     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data4     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data3     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data2     varchar(20)    DEFAULT 'NA' NOT NULL,
    opt_state_data1     varchar(20)    DEFAULT 'NA' NOT NULL,
    last_insert_date    date           DEFAULT CURRENT_DATE NOT NULL,
    valid_from          date           NOT NULL,
    valid_to            date           NOT NULL,
    rec_version         int2           NOT NULL
)
;




-- 
-- TABLE: dim_opt_state_data 
--

ALTER TABLE dim_opt_state_data ADD 
    CONSTRAINT dim_opt_state_data_pk PRIMARY KEY (rec_key)
;

