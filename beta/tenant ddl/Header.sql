--
-- ER/Studio Data Architect 10.0 SQL Code Generation
-- Company :      Amplify, Inc.
-- Project :      Parcc_OLAP LDM
-- Author :       Vadim Nirenberg
--
-- Date Created : Tuesday, May 26, 2015 16:26:09
-- Target DBMS : PostgreSQL 9.x
--

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk1
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk12
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk4
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk5
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk6
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk7
;

ALTER TABLE fact_sum
DROP CONSTRAINT fact_sum_fk8
;

DROP TABLE dim_accomod
;
DROP TABLE dim_institution
;
DROP TABLE dim_opt_state_data
;
DROP TABLE dim_poy
;
DROP TABLE dim_student
;
DROP TABLE dim_test
;
DROP TABLE fact_sum
;
