
CREATE TABLE public.dim_test_1_1 (
                test_form_key NUMERIC(4) NOT NULL,
                form_id VARCHAR(14) DEFAULT 'NA'::character varying NOT NULL,
                test_category VARCHAR(2) DEFAULT 'NA'::character varying NOT NULL,
                test_subject VARCHAR(35) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                asmt_grade VARCHAR(12) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                test_code VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                pba_form_id VARCHAR(50),
                eoy_form_id VARCHAR(50),
                pba_category CHAR(1),
                eoy_category CHAR(1),
                CONSTRAINT dim_test_1_1_pk PRIMARY KEY (test_form_key)
);
COMMENT ON TABLE public.dim_test_1_1 IS 'removed course, course_key fields';
COMMENT ON COLUMN public.dim_test_1_1.test_form_key IS 'NA for summative different for different test_codes';
COMMENT ON COLUMN public.dim_test_1_1.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative';
COMMENT ON COLUMN public.dim_test_1_1.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component ';
COMMENT ON COLUMN public.dim_test_1_1.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356';
COMMENT ON COLUMN public.dim_test_1_1.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade';


CREATE INDEX idx_dim_test_lookup
 ON public.dim_test_1_1 USING BTREE
 ( test_code, test_category );

CREATE TABLE public.dim_student_1_1 (
                student_key NUMERIC(7) NOT NULL,
                student_parcc_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_sex CHAR(1),
                ethnicity VARCHAR(150) DEFAULT 'could not resolve'::character varying NOT NULL,
                ethn_hisp_latino CHAR(1),
                ethn_indian_alaska CHAR(1),
                ethn_asian CHAR(1),
                ethn_black CHAR(1),
                ethn_hawai CHAR(1),
                ethn_white CHAR(1),
                ethn_filler CHAR(1),
                ethn_2_more CHAR(1),
                ell CHAR(1),
                lep_status CHAR(1),
                gift_talent CHAR(1),
                migrant_status CHAR(1),
                econo_disadvantage CHAR(1),
                disabil_student CHAR(1),
                primary_disabil_type TEXT,
                student_state_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying,
                student_local_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_name VARCHAR(105) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_dob VARCHAR(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                valid_from TIMESTAMP DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
                valid_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
                CONSTRAINT dim_student_1_1_pk PRIMARY KEY (student_key)
);
COMMENT ON COLUMN public.dim_student_1_1.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.';
COMMENT ON COLUMN public.dim_student_1_1.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1_1.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425';
COMMENT ON COLUMN public.dim_student_1_1.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).';
COMMENT ON COLUMN public.dim_student_1_1.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';
COMMENT ON COLUMN public.dim_student_1_1.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.';
COMMENT ON COLUMN public.dim_student_1_1.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789';
COMMENT ON COLUMN public.dim_student_1_1.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';
COMMENT ON COLUMN public.dim_student_1_1.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569�';
COMMENT ON COLUMN public.dim_student_1_1.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.';
COMMENT ON COLUMN public.dim_student_1_1.student_local_id IS 'Parcc name: LocalStudentIdentifier. ';
COMMENT ON COLUMN public.dim_student_1_1.student_name IS 'Last name||", "|| First name ||", "||Middle Name';
COMMENT ON COLUMN public.dim_student_1_1.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995';


CREATE INDEX idx_dim_student_lookup
 ON public.dim_student_1_1 USING BTREE
 ( student_parcc_id );

CREATE TABLE public.dim_poy_1_1 (
                poy_key NUMERIC(4) DEFAULT (-1) NOT NULL,
                poy VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                end_year VARCHAR(9) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                school_year CHAR(9) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_poy_1_1_pk PRIMARY KEY (poy_key)
);
COMMENT ON COLUMN public.dim_poy_1_1.poy_key IS 'YEAR || 01 OR YEAR || 02';
COMMENT ON COLUMN public.dim_poy_1_1.poy IS 'Fall, Spring';
COMMENT ON COLUMN public.dim_poy_1_1.end_year IS 'Academioc year end year,example 2014 for 2013-2014';


CREATE INDEX idx_dim_poy_lookup
 ON public.dim_poy_1_1 USING BTREE
 ( poy, school_year );

CREATE TABLE public.dim_opt_state_data_1_1 (
                rec_key BIGINT NOT NULL,
                opt_state_data8 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data7 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data6 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data5 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data4 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data3 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data2 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data1 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE,
                valid_to DATE,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_opt_state_data_1_1_pk PRIMARY KEY (rec_key)
);


CREATE TABLE public.dim_institution_1_1 (
                school_key NUMERIC(4) DEFAULT nextval('dim_institution_school_key_seq'::regclass) NOT NULL,
                dist_name VARCHAR(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                dist_id VARCHAR(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                school_name VARCHAR(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                school_id VARCHAR(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                state_id CHAR(3) DEFAULT 'N/A'::bpchar,
                CONSTRAINT dim_institution_1_1_pk PRIMARY KEY (school_key)
);
COMMENT ON COLUMN public.dim_institution_1_1.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution_1_1.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution_1_1.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution_1_1.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


CREATE INDEX idx_dim_institution_lookup
 ON public.dim_institution_1_1 USING BTREE
 ( school_id );

CREATE TABLE public.fact_sum_1_1 (
                rec_id BIGINT NOT NULL,
                rec_key BIGINT NOT NULL,
                poy_key NUMERIC(4) DEFAULT (-1) NOT NULL,
                resp_school_key NUMERIC(4) DEFAULT nextval('fact_sum_resp_school_key_seq'::regclass) NOT NULL,
                student_key NUMERIC(7) NOT NULL,
                student_grade VARCHAR(12) NOT NULL,
                sum_scale_score INTEGER,
                sum_csem INTEGER,
                sum_perf_lvl SMALLINT,
                sum_read_scale_score INTEGER,
                sum_read_csem INTEGER,
                sum_write_scale_score INTEGER,
                sum_write_csem INTEGER,
                subclaim1_category SMALLINT,
                subclaim2_category SMALLINT,
                subclaim3_category SMALLINT,
                subclaim4_category SMALLINT,
                subclaim5_category SMALLINT,
                subclaim6_category SMALLINT,
                state_growth_percent NUMERIC(5,2),
                district_growth_percent NUMERIC(5,2),
                parcc_growth_percent NUMERIC(5,2),
                multirecord_flag CHAR(2) DEFAULT (-1) NOT NULL,
                result_type VARCHAR(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                record_type VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                reported_score_flag CHAR(1) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
                report_suppression_code CHAR(2),
                report_suppression_action SMALLINT,
                reported_roster_flag VARCHAR(2) DEFAULT 'NA'::character varying NOT NULL,
                include_in_parcc BOOLEAN DEFAULT false NOT NULL,
                include_in_state BOOLEAN DEFAULT false NOT NULL,
                include_in_district BOOLEAN DEFAULT false NOT NULL,
                include_in_school BOOLEAN DEFAULT false NOT NULL,
                include_in_isr BOOLEAN DEFAULT false NOT NULL,
                include_in_roster CHAR(1) DEFAULT 'N'::bpchar NOT NULL,
                create_date DATE DEFAULT ('now'::text)::date NOT NULL,
                me_flag VARCHAR(8),
                form_category VARCHAR(2),
                pba_test_uuid VARCHAR(36),
                eoy_test_uuid VARCHAR(36),
                sum_score_rec_uuid VARCHAR(36),
                eoy_test_school_key INTEGER,
                pba_test_school_key INTEGER,
                pba_total_items INTEGER,
                eoy_total_items INTEGER,
                pba_attempt_flag CHAR(1),
                eoy_attempt_flag CHAR(1),
                pba_total_items_attempt INTEGER,
                eoy_total_items_attempt INTEGER,
                pba_total_items_unit1 INTEGER,
                eoy_total_items_unit1 INTEGER,
                pba_total_items_unit2 INTEGER,
                eoy_total_items_unit2 INTEGER,
                pba_total_items_unit3 INTEGER,
                eoy_total_items_unit3 INTEGER,
                pba_total_items_unit4 INTEGER,
                eoy_total_items_unit4 INTEGER,
                pba_total_items_unit5 INTEGER,
                eoy_total_items_unit5 INTEGER,
                pba_unit1_items_attempt INTEGER,
                eoy_unit1_items_attempt INTEGER,
                pba_unit2_items_attempt INTEGER,
                eoy_unit2_items_attempt INTEGER,
                pba_unit3_items_attempt INTEGER,
                eoy_unit3_items_attempt INTEGER,
                pba_unit4_items_attempt INTEGER,
                eoy_unit4_items_attempt INTEGER,
                pba_unit5_items_attempt INTEGER,
                eoy_unit5_items_attempt INTEGER,
                pba_not_tested_reason VARCHAR(150),
                eoy_not_tested_reason VARCHAR(150),
                pba_void_reason VARCHAR(150),
                eoy_void_reason VARCHAR(150),
                pba_test_key INTEGER,
                eoy_test_key INTEGER,
                summative_population INTEGER DEFAULT 1,
                CONSTRAINT fact_sum_1_1_pk PRIMARY KEY (rec_id)
);
COMMENT ON COLUMN public.fact_sum_1_1.poy_key IS 'YEAR || 01 OR YEAR || 02';
COMMENT ON COLUMN public.fact_sum_1_1.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';
COMMENT ON COLUMN public.fact_sum_1_1.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';
COMMENT ON COLUMN public.fact_sum_1_1.sum_csem IS 'Summative CSEM';
COMMENT ON COLUMN public.fact_sum_1_1.sum_perf_lvl IS 'Summative Performance Level';
COMMENT ON COLUMN public.fact_sum_1_1.sum_read_scale_score IS 'Summative Reading Scale Score';
COMMENT ON COLUMN public.fact_sum_1_1.sum_read_csem IS 'Summative Reading CSEM';
COMMENT ON COLUMN public.fact_sum_1_1.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';
COMMENT ON COLUMN public.fact_sum_1_1.sum_write_csem IS 'Summative Writing CSEM';
COMMENT ON COLUMN public.fact_sum_1_1.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1_1.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1_1.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1_1.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1_1.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1_1.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1_1.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse';
COMMENT ON COLUMN public.fact_sum_1_1.district_growth_percent IS 'Student Growth Percentile Compared to District';
COMMENT ON COLUMN public.fact_sum_1_1.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';
COMMENT ON COLUMN public.fact_sum_1_1.multirecord_flag IS '1 - Y, 0 - N, -1 = blank';
COMMENT ON COLUMN public.fact_sum_1_1.result_type IS 'Summative, PBA, EOY';
COMMENT ON COLUMN public.fact_sum_1_1.record_type IS ' Summative Score, Single Component, No Component';
COMMENT ON COLUMN public.fact_sum_1_1.reported_score_flag IS 'Y,N,balnk';
COMMENT ON COLUMN public.fact_sum_1_1.reported_roster_flag IS 'Y, N, NA';


CREATE INDEX eoy_attempt_idx
 ON public.fact_sum_1_1 USING BTREE
 ( eoy_attempt_flag );

CREATE INDEX eoy_test_idx
 ON public.fact_sum_1_1 USING BTREE
 ( eoy_test_key );

CREATE INDEX eoy_test_school_idx
 ON public.fact_sum_1_1 USING BTREE
 ( eoy_test_school_key );

CREATE INDEX fact_sum_ix1
 ON public.fact_sum_1_1 USING BTREE
 ( rec_key );

CREATE INDEX fact_sum_ix2
 ON public.fact_sum_1_1 USING BTREE
 ( poy_key );

CREATE INDEX fact_sum_ix8
 ON public.fact_sum_1_1 USING BTREE
 ( resp_school_key );

CREATE INDEX fact_sum_ix9
 ON public.fact_sum_1_1 USING BTREE
 ( student_key );

CREATE INDEX me_idx
 ON public.fact_sum_1_1 USING BTREE
 ( me_flag );

CREATE INDEX multirecord_idx
 ON public.fact_sum_1_1 USING BTREE
 ( multirecord_flag );

CREATE INDEX pba_attempt_idx
 ON public.fact_sum_1_1 USING BTREE
 ( pba_attempt_flag );

CREATE INDEX pba_test_idx
 ON public.fact_sum_1_1 USING BTREE
 ( pba_test_key );

CREATE INDEX pba_test_school_idx
 ON public.fact_sum_1_1 USING BTREE
 ( pba_test_school_key );

CREATE INDEX sum_scale_score_idx
 ON public.fact_sum_1_1 USING BTREE
 ( sum_scale_score );

CREATE TABLE public.dim_accomod_1_1 (
                rec_key BIGINT NOT NULL,
                accomod_ell CHAR(1),
                accomod_504 CHAR(1),
                accomod_ind_ed CHAR(1),
                accomod_freq_breacks CHAR(1),
                accomod_alt_location CHAR(1),
                accomod_small_group CHAR(1),
                accomod_special_equip CHAR(1),
                accomod_spec_area CHAR(1),
                accomod_time_day CHAR(1),
                accomod_answer_mask CHAR(1),
                accomod_color_contrast VARCHAR(19),
                accomod_asl_video CHAR(1),
                accomod_screen_reader CHAR(1),
                accomod_close_capt_ela CHAR(1),
                accomod_braille_ela CHAR(1),
                accomod_tactile_graph CHAR(1),
                accomod_answer_rec CHAR(1),
                accomod_braille_resp VARCHAR(16),
                accomod_construct_resp_ela VARCHAR(20),
                accomod_select_resp_ela VARCHAR(20),
                accomod_monitor_test_resp CHAR(1),
                accomod_word_predict CHAR(1),
                accomod_native_lang CHAR(1),
                accomod_loud_native_lang VARCHAR(40),
                accomod_w_2_w_dict CHAR(1),
                accomod_extend_time VARCHAR(6),
                accomod_alt_paper_test CHAR(1),
                accomod_human_read_sign VARCHAR(14),
                accomod_large_print CHAR(1),
                accomod_braille_tactile CHAR(1),
                accomod_math_resp_el VARCHAR(20),
                accomod_calculator CHAR(1),
                accomod_math_trans_online CHAR(3),
                accomod_paper_trans_math CHAR(3),
                accomod_math_resp VARCHAR(20),
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT DEFAULT 0 NOT NULL,
                accomod_text_2_speech_ela CHAR(1),
                accomod_text_2_speech_math CHAR(1),
                accomod_read_ela VARCHAR(50),
                accomod_read_math VARCHAR(50),
                CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (rec_key)
);
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_ell IS 'Assessment Accommodation:  English learner (EL)';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_small_group IS 'Small Testing Group';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_special_equip IS 'Specialized Equipment or Furniture';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_spec_area IS 'Specified Area or Setting';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_time_day IS 'Time Of Day';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_native_lang IS 'General Administration Directions Clarified in Student�s Native Language (by test administrator)';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Student�s Native Language
(by test administrator)';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_extend_time IS 'Extended Time';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_alt_paper_test IS 'Alternate Representation - Paper Test';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod_1_1.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


CREATE TABLE public.dim_test (
                test_form_key NUMERIC(4) NOT NULL,
                form_id VARCHAR(14) DEFAULT 'NA'::character varying NOT NULL,
                test_category VARCHAR(2) DEFAULT 'NA'::character varying NOT NULL,
                test_subject VARCHAR(35) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                asmt_grade VARCHAR(12) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                test_code VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                pba_form_id VARCHAR(50),
                eoy_form_id VARCHAR(50),
                pba_category CHAR(1),
                eoy_category CHAR(1),
                CONSTRAINT dim_test_pk PRIMARY KEY (test_form_key)
);
COMMENT ON TABLE public.dim_test IS 'removed course, course_key fields';
COMMENT ON COLUMN public.dim_test.test_form_key IS 'NA for summative different for different test_codes';
COMMENT ON COLUMN public.dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative';
COMMENT ON COLUMN public.dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component ';
COMMENT ON COLUMN public.dim_test.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356';
COMMENT ON COLUMN public.dim_test.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade';


CREATE INDEX idx_dim_test_lookup
 ON public.dim_test USING BTREE
 ( test_code, test_category );

CREATE TABLE public.dim_student (
                student_key NUMERIC(7) NOT NULL,
                student_parcc_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_sex CHAR(1),
                ethnicity VARCHAR(150) DEFAULT 'could not resolve'::character varying NOT NULL,
                ethn_hisp_latino CHAR(1),
                ethn_indian_alaska CHAR(1),
                ethn_asian CHAR(1),
                ethn_black CHAR(1),
                ethn_hawai CHAR(1),
                ethn_white CHAR(1),
                ethn_filler CHAR(1),
                ethn_2_more CHAR(1),
                ell CHAR(1),
                lep_status CHAR(1),
                gift_talent CHAR(1),
                migrant_status CHAR(1),
                econo_disadvantage CHAR(1),
                disabil_student CHAR(1),
                primary_disabil_type TEXT,
                student_state_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying,
                student_local_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_name VARCHAR(105) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_dob VARCHAR(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                valid_from TIMESTAMP DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
                valid_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
                CONSTRAINT dim_student_pk PRIMARY KEY (student_key)
);
COMMENT ON COLUMN public.dim_student.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.';
COMMENT ON COLUMN public.dim_student.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425';
COMMENT ON COLUMN public.dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).';
COMMENT ON COLUMN public.dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';
COMMENT ON COLUMN public.dim_student.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.';
COMMENT ON COLUMN public.dim_student.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789';
COMMENT ON COLUMN public.dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';
COMMENT ON COLUMN public.dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569�';
COMMENT ON COLUMN public.dim_student.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.';
COMMENT ON COLUMN public.dim_student.student_local_id IS 'Parcc name: LocalStudentIdentifier. ';
COMMENT ON COLUMN public.dim_student.student_name IS 'Last name||", "|| First name ||", "||Middle Name';
COMMENT ON COLUMN public.dim_student.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995';


CREATE INDEX idx_dim_student_lookup
 ON public.dim_student USING BTREE
 ( student_parcc_id );

CREATE TABLE public.dim_poy (
                poy_key NUMERIC(4) DEFAULT (-1) NOT NULL,
                poy VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                end_year VARCHAR(9) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                school_year CHAR(9) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_poy_pk PRIMARY KEY (poy_key)
);
COMMENT ON COLUMN public.dim_poy.poy_key IS 'YEAR || 01 OR YEAR || 02';
COMMENT ON COLUMN public.dim_poy.poy IS 'Fall, Spring';
COMMENT ON COLUMN public.dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014';


CREATE INDEX idx_dim_poy_lookup
 ON public.dim_poy USING BTREE
 ( poy, school_year );

CREATE TABLE public.dim_opt_state_data (
                rec_key BIGINT NOT NULL,
                opt_state_data8 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data7 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data6 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data5 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data4 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data3 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data2 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data1 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE,
                valid_to DATE,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_opt_state_data_pk PRIMARY KEY (rec_key)
);


CREATE TABLE public.dim_institution (
                school_key NUMERIC(4) DEFAULT nextval('dim_institution_school_key_seq'::regclass) NOT NULL,
                dist_name VARCHAR(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                dist_id VARCHAR(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                school_name VARCHAR(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                school_id VARCHAR(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT NOT NULL,
                state_id CHAR(3) DEFAULT 'N/A'::bpchar,
                CONSTRAINT dim_institution_pk PRIMARY KEY (school_key)
);
COMMENT ON COLUMN public.dim_institution.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


CREATE INDEX idx_dim_institution_lookup
 ON public.dim_institution USING BTREE
 ( school_id );

CREATE TABLE public.fact_sum (
                rec_id BIGINT NOT NULL,
                rec_key BIGINT NOT NULL,
                poy_key NUMERIC(4) DEFAULT (-1) NOT NULL,
                resp_school_key NUMERIC(4) DEFAULT nextval('fact_sum_resp_school_key_seq'::regclass) NOT NULL,
                student_key NUMERIC(7) NOT NULL,
                student_grade VARCHAR(12) NOT NULL,
                sum_scale_score INTEGER,
                sum_csem INTEGER,
                sum_perf_lvl SMALLINT,
                sum_read_scale_score INTEGER,
                sum_read_csem INTEGER,
                sum_write_scale_score INTEGER,
                sum_write_csem INTEGER,
                subclaim1_category SMALLINT,
                subclaim2_category SMALLINT,
                subclaim3_category SMALLINT,
                subclaim4_category SMALLINT,
                subclaim5_category SMALLINT,
                subclaim6_category SMALLINT,
                state_growth_percent NUMERIC(5,2),
                district_growth_percent NUMERIC(5,2),
                parcc_growth_percent NUMERIC(5,2),
                multirecord_flag CHAR(2) DEFAULT (-1) NOT NULL,
                result_type VARCHAR(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                record_type VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                reported_score_flag CHAR(1) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
                report_suppression_code CHAR(2),
                report_suppression_action SMALLINT,
                reported_roster_flag VARCHAR(2) DEFAULT 'NA'::character varying NOT NULL,
                include_in_parcc BOOLEAN DEFAULT false NOT NULL,
                include_in_state BOOLEAN DEFAULT false NOT NULL,
                include_in_district BOOLEAN DEFAULT false NOT NULL,
                include_in_school BOOLEAN DEFAULT false NOT NULL,
                include_in_isr BOOLEAN DEFAULT false NOT NULL,
                include_in_roster CHAR(1) DEFAULT 'N'::bpchar NOT NULL,
                create_date DATE DEFAULT ('now'::text)::date NOT NULL,
                me_flag VARCHAR(8),
                form_category VARCHAR(2),
                pba_test_uuid VARCHAR(36),
                eoy_test_uuid VARCHAR(36),
                sum_score_rec_uuid VARCHAR(36),
                eoy_test_school_key INTEGER,
                pba_test_school_key INTEGER,
                pba_total_items INTEGER,
                eoy_total_items INTEGER,
                pba_attempt_flag CHAR(1),
                eoy_attempt_flag CHAR(1),
                pba_total_items_attempt INTEGER,
                eoy_total_items_attempt INTEGER,
                pba_total_items_unit1 INTEGER,
                eoy_total_items_unit1 INTEGER,
                pba_total_items_unit2 INTEGER,
                eoy_total_items_unit2 INTEGER,
                pba_total_items_unit3 INTEGER,
                eoy_total_items_unit3 INTEGER,
                pba_total_items_unit4 INTEGER,
                eoy_total_items_unit4 INTEGER,
                pba_total_items_unit5 INTEGER,
                eoy_total_items_unit5 INTEGER,
                pba_unit1_items_attempt INTEGER,
                eoy_unit1_items_attempt INTEGER,
                pba_unit2_items_attempt INTEGER,
                eoy_unit2_items_attempt INTEGER,
                pba_unit3_items_attempt INTEGER,
                eoy_unit3_items_attempt INTEGER,
                pba_unit4_items_attempt INTEGER,
                eoy_unit4_items_attempt INTEGER,
                pba_unit5_items_attempt INTEGER,
                eoy_unit5_items_attempt INTEGER,
                pba_not_tested_reason VARCHAR(150),
                eoy_not_tested_reason VARCHAR(150),
                pba_void_reason VARCHAR(150),
                eoy_void_reason VARCHAR(150),
                pba_test_key INTEGER,
                eoy_test_key INTEGER,
                summative_population INTEGER DEFAULT 1,
                CONSTRAINT fact_sum_pk PRIMARY KEY (rec_id)
);
COMMENT ON COLUMN public.fact_sum.poy_key IS 'YEAR || 01 OR YEAR || 02';
COMMENT ON COLUMN public.fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';
COMMENT ON COLUMN public.fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';
COMMENT ON COLUMN public.fact_sum.sum_csem IS 'Summative CSEM';
COMMENT ON COLUMN public.fact_sum.sum_perf_lvl IS 'Summative Performance Level';
COMMENT ON COLUMN public.fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score';
COMMENT ON COLUMN public.fact_sum.sum_read_csem IS 'Summative Reading CSEM';
COMMENT ON COLUMN public.fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';
COMMENT ON COLUMN public.fact_sum.sum_write_csem IS 'Summative Writing CSEM';
COMMENT ON COLUMN public.fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse';
COMMENT ON COLUMN public.fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District';
COMMENT ON COLUMN public.fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';
COMMENT ON COLUMN public.fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank';
COMMENT ON COLUMN public.fact_sum.result_type IS 'Summative, PBA, EOY';
COMMENT ON COLUMN public.fact_sum.record_type IS ' Summative Score, Single Component, No Component';
COMMENT ON COLUMN public.fact_sum.reported_score_flag IS 'Y,N,balnk';
COMMENT ON COLUMN public.fact_sum.reported_roster_flag IS 'Y, N, NA';


CREATE INDEX eoy_attempt_idx
 ON public.fact_sum USING BTREE
 ( eoy_attempt_flag );

CREATE INDEX eoy_test_idx
 ON public.fact_sum USING BTREE
 ( eoy_test_key );

CREATE INDEX eoy_test_school_idx
 ON public.fact_sum USING BTREE
 ( eoy_test_school_key );

CREATE INDEX fact_sum_ix1
 ON public.fact_sum USING BTREE
 ( rec_key );

CREATE INDEX fact_sum_ix2
 ON public.fact_sum USING BTREE
 ( poy_key );

CREATE INDEX fact_sum_ix8
 ON public.fact_sum USING BTREE
 ( resp_school_key );

CREATE INDEX fact_sum_ix9
 ON public.fact_sum USING BTREE
 ( student_key );

CREATE INDEX me_idx
 ON public.fact_sum USING BTREE
 ( me_flag );

CREATE INDEX multirecord_idx
 ON public.fact_sum USING BTREE
 ( multirecord_flag );

CREATE INDEX pba_attempt_idx
 ON public.fact_sum USING BTREE
 ( pba_attempt_flag );

CREATE INDEX pba_test_idx
 ON public.fact_sum USING BTREE
 ( pba_test_key );

CREATE INDEX pba_test_school_idx
 ON public.fact_sum USING BTREE
 ( pba_test_school_key );

CREATE INDEX sum_scale_score_idx
 ON public.fact_sum USING BTREE
 ( sum_scale_score );

CREATE TABLE public.dim_accomod (
                rec_key BIGINT NOT NULL,
                accomod_ell CHAR(1),
                accomod_504 CHAR(1),
                accomod_ind_ed CHAR(1),
                accomod_freq_breacks CHAR(1),
                accomod_alt_location CHAR(1),
                accomod_small_group CHAR(1),
                accomod_special_equip CHAR(1),
                accomod_spec_area CHAR(1),
                accomod_time_day CHAR(1),
                accomod_answer_mask CHAR(1),
                accomod_color_contrast VARCHAR(19),
                accomod_asl_video CHAR(1),
                accomod_screen_reader CHAR(1),
                accomod_close_capt_ela CHAR(1),
                accomod_braille_ela CHAR(1),
                accomod_tactile_graph CHAR(1),
                accomod_answer_rec CHAR(1),
                accomod_braille_resp VARCHAR(16),
                accomod_construct_resp_ela VARCHAR(20),
                accomod_select_resp_ela VARCHAR(20),
                accomod_monitor_test_resp CHAR(1),
                accomod_word_predict CHAR(1),
                accomod_native_lang CHAR(1),
                accomod_loud_native_lang VARCHAR(40),
                accomod_w_2_w_dict CHAR(1),
                accomod_extend_time VARCHAR(6),
                accomod_alt_paper_test CHAR(1),
                accomod_human_read_sign VARCHAR(14),
                accomod_large_print CHAR(1),
                accomod_braille_tactile CHAR(1),
                accomod_math_resp_el VARCHAR(20),
                accomod_calculator CHAR(1),
                accomod_math_trans_online CHAR(3),
                accomod_paper_trans_math CHAR(3),
                accomod_math_resp VARCHAR(20),
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE DEFAULT '1900-01-01'::date NOT NULL,
                valid_to DATE DEFAULT '2199-12-31'::date NOT NULL,
                rec_version SMALLINT DEFAULT 0 NOT NULL,
                accomod_text_2_speech_ela CHAR(1),
                accomod_text_2_speech_math CHAR(1),
                accomod_read_ela VARCHAR(50),
                accomod_read_math VARCHAR(50),
                CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (rec_key)
);
COMMENT ON COLUMN public.dim_accomod.accomod_ell IS 'Assessment Accommodation:  English learner (EL)';
COMMENT ON COLUMN public.dim_accomod.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)';
COMMENT ON COLUMN public.dim_accomod.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location';
COMMENT ON COLUMN public.dim_accomod.accomod_small_group IS 'Small Testing Group';
COMMENT ON COLUMN public.dim_accomod.accomod_special_equip IS 'Specialized Equipment or Furniture';
COMMENT ON COLUMN public.dim_accomod.accomod_spec_area IS 'Specified Area or Setting';
COMMENT ON COLUMN public.dim_accomod.accomod_time_day IS 'Time Of Day';
COMMENT ON COLUMN public.dim_accomod.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.';
COMMENT ON COLUMN public.dim_accomod.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.';
COMMENT ON COLUMN public.dim_accomod.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types';
COMMENT ON COLUMN public.dim_accomod.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_native_lang IS 'General Administration Directions Clarified in Student�s Native Language (by test administrator)';
COMMENT ON COLUMN public.dim_accomod.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Student�s Native Language
(by test administrator)';
COMMENT ON COLUMN public.dim_accomod.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod.accomod_extend_time IS 'Extended Time';
COMMENT ON COLUMN public.dim_accomod.accomod_alt_paper_test IS 'Alternate Representation - Paper Test';
COMMENT ON COLUMN public.dim_accomod.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


CREATE TABLE public.dim_test_1 (
                test_form_key INTEGER NOT NULL,
                form_id VARCHAR(14) DEFAULT 'NA'::character varying NOT NULL,
                test_category VARCHAR(2) DEFAULT 'NA'::character varying NOT NULL,
                test_subject VARCHAR(35) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                asmt_grade VARCHAR(12) NOT NULL,
                test_code VARCHAR(20) NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE NOT NULL,
                valid_to DATE NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_test_1_pk PRIMARY KEY (test_form_key)
);
COMMENT ON TABLE public.dim_test_1 IS 'removed course, course_key fields';
COMMENT ON COLUMN public.dim_test_1.test_form_key IS 'NA for summative different for different test_codes';
COMMENT ON COLUMN public.dim_test_1.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative';
COMMENT ON COLUMN public.dim_test_1.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component ';
COMMENT ON COLUMN public.dim_test_1.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356';
COMMENT ON COLUMN public.dim_test_1.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade';


CREATE TABLE public.dim_student_1 (
                student_key INTEGER NOT NULL,
                student_parcc_id VARCHAR(40) NOT NULL,
                student_sex CHAR(1),
                ethnicity VARCHAR(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                ethn_hisp_latino CHAR(1),
                ethn_indian_alaska CHAR(1),
                ethn_asian CHAR(1),
                ethn_black CHAR(1),
                ethn_hawai CHAR(1),
                ethn_white CHAR(1),
                ethn_filler CHAR(1),
                ethn_2_more CHAR(1),
                ell CHAR(1),
                lep_status CHAR(1),
                gift_talent CHAR(1),
                migrant_status CHAR(1),
                econo_disadvantage CHAR(1),
                disabil_student CHAR(1),
                primary_disabil_type VARCHAR(3),
                student_state_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying,
                student_local_id VARCHAR(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_name VARCHAR(105) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                student_dob_1 DATE NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE NOT NULL,
                valid_to DATE NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_student_1_pk PRIMARY KEY (student_key)
);
COMMENT ON COLUMN public.dim_student_1.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.';
COMMENT ON COLUMN public.dim_student_1.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';
COMMENT ON COLUMN public.dim_student_1.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425';
COMMENT ON COLUMN public.dim_student_1.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).';
COMMENT ON COLUMN public.dim_student_1.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';
COMMENT ON COLUMN public.dim_student_1.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.';
COMMENT ON COLUMN public.dim_student_1.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789';
COMMENT ON COLUMN public.dim_student_1.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';
COMMENT ON COLUMN public.dim_student_1.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.';
COMMENT ON COLUMN public.dim_student_1.student_local_id IS 'Parcc name: LocalStudentIdentifier. ';
COMMENT ON COLUMN public.dim_student_1.student_name IS 'Last name||", "|| First name ||", "||Middle Name';
COMMENT ON COLUMN public.dim_student_1.student_dob_1 IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995';


CREATE TABLE public.dim_poy_1 (
                poy_key NUMERIC(1) DEFAULT (-1) NOT NULL,
                poy VARCHAR(20) NOT NULL,
                end_year NUMERIC(4) NOT NULL,
                school_year CHAR(9) NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE NOT NULL,
                valid_to DATE NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_poy_1_pk PRIMARY KEY (poy_key)
);
COMMENT ON COLUMN public.dim_poy_1.poy_key IS 'YEAR || 01 OR YEAR || 02';
COMMENT ON COLUMN public.dim_poy_1.poy IS 'Fall, Spring';
COMMENT ON COLUMN public.dim_poy_1.end_year IS 'Academioc year end year,example 2014 for 2013-2014';


CREATE TABLE public.dim_opt_state_data_1 (
                rec_key BIGINT NOT NULL,
                opt_state_data8 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data7 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data6 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data5 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data4 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data3 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data2 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                opt_state_data1 VARCHAR(20) DEFAULT 'NA'::character varying NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE NOT NULL,
                valid_to DATE NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_opt_state_data_1_pk PRIMARY KEY (rec_key)
);


CREATE SEQUENCE public.dim_institution_school_key_seq_1;

CREATE TABLE public.dim_institution_1 (
                school_key INTEGER DEFAULT nextval('dim_institution_school_key_seq'::regclass) NOT NULL DEFAULT nextval('public.dim_institution_school_key_seq_1'),
                dist_name VARCHAR(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
                dist_id VARCHAR(15) NOT NULL,
                school_name VARCHAR(60) NOT NULL,
                school_id VARCHAR(15) NOT NULL,
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE NOT NULL,
                valid_to DATE NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_institution_1_pk PRIMARY KEY (school_key)
);
COMMENT ON COLUMN public.dim_institution_1.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution_1.dist_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution_1.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';
COMMENT ON COLUMN public.dim_institution_1.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


ALTER SEQUENCE public.dim_institution_school_key_seq_1 OWNED BY public.dim_institution_1.school_key;

CREATE TABLE public.dim_accomod_1 (
                rec_key BIGINT NOT NULL,
                accomod_ell CHAR(1),
                accomod_504 CHAR(1),
                accomod_ind_ed CHAR(1),
                accomod_freq_breacks CHAR(1),
                accomod_alt_location CHAR(1),
                accomod_small_group CHAR(1),
                accomod_special_equip CHAR(1),
                accomod_spec_area CHAR(1),
                accomod_time_day CHAR(1),
                accomod_answer_mask CHAR(1),
                accomod_color_contrast VARCHAR(19),
                accomod_asl_video CHAR(1),
                accomod_screen_reader CHAR(1),
                accomod_close_capt_ela CHAR(1),
                accomod_read_math_ela VARCHAR(14),
                accomod_braille_ela CHAR(1),
                accomod_tactile_graph CHAR(1),
                accomod_text_2_speech_math_ela CHAR(1),
                accomod_answer_rec CHAR(1),
                accomod_braille_resp VARCHAR(16),
                accomod_construct_resp_ela VARCHAR(20),
                accomod_select_resp_ela VARCHAR(20),
                accomod_monitor_test_resp CHAR(1),
                accomod_word_predict CHAR(1),
                accomod_native_lang CHAR(1),
                accomod_loud_native_lang VARCHAR(40),
                accomod_w_2_w_dict CHAR(1),
                accomod_extend_time VARCHAR(6),
                accomod_alt_paper_test CHAR(1),
                accomod_human_read_sign VARCHAR(14),
                accomod_large_print CHAR(1),
                accomod_braille_tactile CHAR(1),
                accomod_math_resp_el VARCHAR(20),
                accomod_calculator CHAR(1),
                accomod_math_trans_online CHAR(3),
                accomod_paper_trans_math CHAR(3),
                accomod_math_resp VARCHAR(20),
                last_insert_date DATE DEFAULT ('now'::text)::date NOT NULL,
                valid_from DATE NOT NULL,
                valid_to DATE NOT NULL,
                rec_version SMALLINT NOT NULL,
                CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (rec_key)
);
COMMENT ON COLUMN public.dim_accomod_1.accomod_ell IS 'Assessment Accommodation:  English learner (EL)';
COMMENT ON COLUMN public.dim_accomod_1.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)';
COMMENT ON COLUMN public.dim_accomod_1.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location';
COMMENT ON COLUMN public.dim_accomod_1.accomod_small_group IS 'Small Testing Group';
COMMENT ON COLUMN public.dim_accomod_1.accomod_special_equip IS 'Specialized Equipment or Furniture';
COMMENT ON COLUMN public.dim_accomod_1.accomod_spec_area IS 'Specified Area or Setting';
COMMENT ON COLUMN public.dim_accomod_1.accomod_time_day IS 'Time Of Day';
COMMENT ON COLUMN public.dim_accomod_1.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_read_math_ela IS 'General Administration Directions Read Aloud and Repeated as Needed in Student''s Native Language(by test administrator).OralScriptReadbyTestAdministratorARA; OralScriptReadbyTestAdministratorCHI ; OralScriptReadbyTestAdministratorHAT; OralScriptReadbyTestAdministratorMAH; OralScriptReadbyTestAdministratorNAV; OralScriptReadbyTestAdministratorPOL; OralScriptReadbyTestAdministratorPOR;'' OralScriptReadbyTestAdministratorSOM; OralScriptReadbyTestAdministratorSPA; OralScriptReadbyTestAdministratorVIE; HumanTranslator; blank;';
COMMENT ON COLUMN public.dim_accomod_1.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_text_2_speech_math_ela IS 'Text-to-Speech for ELA/L. Y blank. Personal Needs Profile field. Test content will be read aloud to the student via Text-to-Speech. Embedded within TestNav8. Text-to-Speech is an accessibility feature for Math. Text-to-Speech is an accommodation for ELA/L. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types';
COMMENT ON COLUMN public.dim_accomod_1.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_extend_time IS 'Extended Time';
COMMENT ON COLUMN public.dim_accomod_1.accomod_alt_paper_test IS 'Alternate Representation - Paper Test';
COMMENT ON COLUMN public.dim_accomod_1.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';
COMMENT ON COLUMN public.dim_accomod_1.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


CREATE TABLE public.fact_sum_1 (
                rec_id BIGINT NOT NULL,
                rec_key BIGINT NOT NULL,
                poy_key NUMERIC(1) DEFAULT (-1) NOT NULL,
                test_form_key INTEGER NOT NULL,
                test_school_key INTEGER DEFAULT nextval('fact_sum_test_school_key_seq'::regclass) NOT NULL,
                resp_school_key INTEGER DEFAULT nextval('fact_sum_resp_school_key_seq'::regclass) NOT NULL,
                student_key INTEGER NOT NULL,
                student_grade VARCHAR(12) NOT NULL,
                total_items NUMERIC(3),
                total_items_attempt NUMERIC(3),
                total_items_unit1 NUMERIC(2),
                unit1_items_attempt NUMERIC(2),
                total_items_unit2 NUMERIC(2),
                unit2_items_attempt NUMERIC(2),
                total_items_unit3 NUMERIC(2),
                unit3_items_attempt NUMERIC(2),
                total_items_unit4 NUMERIC(2),
                unit4_items_attempt NUMERIC(2),
                total_items_unit5 NUMERIC(2),
                unit5_items_attempt NUMERIC(2),
                not_tested_reason NUMERIC(2),
                void_reason NUMERIC(2),
                sum_scale_score INTEGER,
                sum_csem INTEGER,
                sum_perf_lvl SMALLINT,
                sum_read_scale_score INTEGER,
                sum_read_csem INTEGER,
                sum_write_scale_score INTEGER,
                sum_write_csem INTEGER,
                subclaim1_category SMALLINT,
                subclaim2_category SMALLINT,
                subclaim3_category SMALLINT,
                subclaim4_category SMALLINT,
                subclaim5_category SMALLINT,
                subclaim6_category SMALLINT,
                state_growth_percent NUMERIC(5,2),
                district_growth_percent NUMERIC(5,2),
                parcc_growth_percent NUMERIC(5,2),
                attempt_flag CHAR(1) DEFAULT (-1) NOT NULL,
                multirecord_flag CHAR(1) DEFAULT (-1) NOT NULL,
                test_uuid VARCHAR(36) NOT NULL,
                result_type VARCHAR(10) NOT NULL,
                record_type VARCHAR(20) NOT NULL,
                reported_score_flag CHAR(1) NOT NULL,
                report_suppression_code CHAR(2),
                report_suppression_action SMALLINT,
                reported_roster_flag VARCHAR(2) DEFAULT 'NA'::character varying NOT NULL,
                include_in_parcc BOOLEAN NOT NULL,
                include_in_state BOOLEAN NOT NULL,
                include_in_district BOOLEAN NOT NULL,
                include_in_school BOOLEAN NOT NULL,
                include_in_isr BOOLEAN NOT NULL,
                include_in_roster CHAR(1) NOT NULL,
                create_date DATE DEFAULT ('now'::text)::date NOT NULL,
                CONSTRAINT fact_sum_1_pk PRIMARY KEY (rec_id)
);
COMMENT ON COLUMN public.fact_sum_1.poy_key IS 'YEAR || 01 OR YEAR || 02';
COMMENT ON COLUMN public.fact_sum_1.test_form_key IS 'NA for summative different for different test_codes';
COMMENT ON COLUMN public.fact_sum_1.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';
COMMENT ON COLUMN public.fact_sum_1.total_items IS 'Total number of questions/items on the PBA, EOY test for that form';
COMMENT ON COLUMN public.fact_sum_1.total_items_attempt IS 'PBA Total Test Items Attempted';
COMMENT ON COLUMN public.fact_sum_1.total_items_unit1 IS 'PBA Unit 1 Total Number of Items';
COMMENT ON COLUMN public.fact_sum_1.total_items_unit2 IS 'PBA Unit 2 Total Number of Items. Total number of items on unit 2 of the test.';
COMMENT ON COLUMN public.fact_sum_1.unit2_items_attempt IS 'PBA Unit 2 Number of Attempted Items. Number of unit 2 items students attempted.';
COMMENT ON COLUMN public.fact_sum_1.total_items_unit3 IS 'PBA Unit 3 Total Number of Items';
COMMENT ON COLUMN public.fact_sum_1.void_reason IS 'PBA Void PBA/EOY Score Reason';
COMMENT ON COLUMN public.fact_sum_1.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';
COMMENT ON COLUMN public.fact_sum_1.sum_csem IS 'Summative CSEM';
COMMENT ON COLUMN public.fact_sum_1.sum_perf_lvl IS 'Summative Performance Level';
COMMENT ON COLUMN public.fact_sum_1.sum_read_scale_score IS 'Summative Reading Scale Score';
COMMENT ON COLUMN public.fact_sum_1.sum_read_csem IS 'Summative Reading CSEM';
COMMENT ON COLUMN public.fact_sum_1.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';
COMMENT ON COLUMN public.fact_sum_1.sum_write_csem IS 'Summative Writing CSEM';
COMMENT ON COLUMN public.fact_sum_1.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';
COMMENT ON COLUMN public.fact_sum_1.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse';
COMMENT ON COLUMN public.fact_sum_1.district_growth_percent IS 'Student Growth Percentile Compared to District';
COMMENT ON COLUMN public.fact_sum_1.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';
COMMENT ON COLUMN public.fact_sum_1.attempt_flag IS '1 - Y, 0 - N, -1 = blank';
COMMENT ON COLUMN public.fact_sum_1.multirecord_flag IS '1 - Y, 0 - N, -1 = blank';
COMMENT ON COLUMN public.fact_sum_1.test_uuid IS 'PBA, eoy or fake Student Test UUID.';
COMMENT ON COLUMN public.fact_sum_1.result_type IS 'Summative, PBA, EOY';
COMMENT ON COLUMN public.fact_sum_1.record_type IS ' Summative Score, Single Component, No Component';
COMMENT ON COLUMN public.fact_sum_1.reported_score_flag IS 'Y,N,balnk';
COMMENT ON COLUMN public.fact_sum_1.reported_roster_flag IS 'Y, N, NA';


CREATE INDEX fact_sum_ix1
 ON public.fact_sum_1 USING BTREE
 ( rec_key );

CREATE INDEX fact_sum_ix2
 ON public.fact_sum_1 USING BTREE
 ( poy_key );

CREATE INDEX fact_sum_ix6
 ON public.fact_sum_1 USING BTREE
 ( test_form_key );

CREATE INDEX fact_sum_ix7
 ON public.fact_sum_1 USING BTREE
 ( test_school_key );

CREATE INDEX fact_sum_ix8
 ON public.fact_sum_1 USING BTREE
 ( resp_school_key );

CREATE INDEX fact_sum_ix9
 ON public.fact_sum_1 USING BTREE
 ( student_key );

ALTER TABLE public.fact_sum_1_1 ADD CONSTRAINT fact_sum_fk4
FOREIGN KEY (student_key)
REFERENCES public.dim_student_1_1 (student_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1_1 ADD CONSTRAINT fact_sum_fk6
FOREIGN KEY (poy_key)
REFERENCES public.dim_poy_1_1 (poy_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1_1 ADD CONSTRAINT fact_sum_fk8
FOREIGN KEY (resp_school_key)
REFERENCES public.dim_institution_1_1 (school_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum ADD CONSTRAINT fact_sum_fk4
FOREIGN KEY (student_key)
REFERENCES public.dim_student (student_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum ADD CONSTRAINT fact_sum_fk6
FOREIGN KEY (poy_key)
REFERENCES public.dim_poy (poy_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum ADD CONSTRAINT fact_sum_fk8
FOREIGN KEY (resp_school_key)
REFERENCES public.dim_institution (school_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk5
FOREIGN KEY (test_form_key)
REFERENCES public.dim_test_1 (test_form_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk4
FOREIGN KEY (student_key)
REFERENCES public.dim_student_1 (student_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk6
FOREIGN KEY (poy_key)
REFERENCES public.dim_poy_1 (poy_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk7
FOREIGN KEY (rec_key)
REFERENCES public.dim_opt_state_data_1 (rec_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk1
FOREIGN KEY (test_school_key)
REFERENCES public.dim_institution_1 (school_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk8
FOREIGN KEY (resp_school_key)
REFERENCES public.dim_institution_1 (school_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fact_sum_1 ADD CONSTRAINT fact_sum_fk12
FOREIGN KEY (rec_key)
REFERENCES public.dim_accomod_1 (rec_key)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
